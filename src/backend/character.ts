import {
    Chain,
    Duration,
    Energy,
    EquipmentTag,
    exportJsonExcept,
    LastAction,
    Point,
    removeFromArray,
    Targets
} from "./common";
import {Behavior, BEHAVIORS} from "./behavior";
import {ABILITIES, Ability, IAbilityTemplate} from "./abilities/ability";
import {AddableResult, Dice} from "./dice";
import {Armor, EquippableItem, Item, NothingEquipment, Weapon} from "./items/item";
import {Condition} from "./effects/condition";
import {WEAPONS} from "./items/weapons";
import {GameMaster} from "./gamemaster";
import {ChainableZone, Faction, ICharacterTemplate, Stats, TileQueryable, XY} from "./interfaces";
import {Color} from "rot-js/lib/color";

export interface EquipmentSlots {
    head: EquippableItem,
    neck: EquippableItem,
    body: EquippableItem,
    hands: EquippableItem,
    waist: EquippableItem,
    feet: EquippableItem,
    ring1: EquippableItem,
    ring2: EquippableItem,
    mainHand: EquippableItem,
    offHand: EquippableItem,
    [slotName: string]: EquippableItem,
}

export type EquipmentSlotName = keyof EquipmentSlots;

export class Character implements ICharacterTemplate {
    // Template vars
    public char: string;
    public name: string;
    public displayName: string;
    public baseMaxHp: number;
    public hpPerLevel: number;
    public description: string;
    public fgcolor: Color;

    className: string
    classAbilityNames: string[];  // ability names
    proficiencies: EquipmentTag[];

    public faction: Faction;
    public stats: Stats;
    public baseAc: number;
    public sightDistance: number;
    public maxPoints: number;
    public level: number;
    public baseMovementSpeed: number;

    public energy: number;
    public loc: XY;
    public hp: number;
    public tempHp: number;

    // Things that aren't part of the template but get saved regardless
    public points: number;
    public broadcasts: boolean;
    public lastTurn: LastAction[];
    public refreshBossNextTurn: boolean;

    // Unrealized versions of saved data
    public abilityNames: string[];
    public defaultMeleeAbilityName: string;
    public defaultRangedAbilityName: string;
    public behaviorName: string;

    // Realized versions of saved data
    public defaultMeleeAbility: Ability;
    public defaultRangedAbility: Ability;
    protected _behavior: Behavior;

    // auto-realized
    public abilities: Ability[];
    public inventory: Item[];
    public equipment: EquipmentSlots;
    public conditions: Condition[];
    public passives: Condition[];
    public hotkeys: Array<Ability>;

    // Other
    public messages: string[];
    public map: TileQueryable;

    constructor(template: ICharacterTemplate) {
        this.energy = 0;
        this.maxPoints = 20;
        this.level = 1;
        this.baseMovementSpeed = Energy.MOVE;
        this.loc = null;

        this.classAbilityNames = [];
        this.proficiencies = [];

        this.stats = {
            str: 10,
            dex: 10,
            con: 10,
            int: 10,
            wis: 10,
            cha: 10,
        }
        this.faction = Faction.Enemy;
        this.baseAc = 10;
        this.sightDistance = 10;
        this.tempHp = 0;

        this.abilities = [];
        this.messages = [];
        this.inventory = [];
        this.lastTurn = [LastAction.Nothing];
        this.refreshBossNextTurn = false;
        this.hotkeys = [
            null,null,null,null,null,null,null,null,null,null,
        ];

        let nothing = new NothingEquipment();
        this.equipment = {
            head: nothing,
            neck: nothing,
            body: nothing,
            hands: nothing,
            waist: nothing,
            feet: nothing,
            ring1: nothing,
            ring2: nothing,
            mainHand: nothing,
            offHand: nothing,
        };

        this.conditions = [];
        this.passives = [];

        let populateDefaultPowers = (
            !template.abilityNames ||
            template.abilityNames.length !== 0
        )
        if(populateDefaultPowers) {
            this.abilityNames = ["BasicMeleeAttack", "BasicRangedAttack"];
            this.defaultMeleeAbilityName = "BasicMeleeAttack";
            this.defaultRangedAbilityName = "BasicRangedAttack";
        }

        // Object.assign does a shallow copy, meaning the arrays in the template
        // get passed by _value_.  Without this, adding e.g. abilities adds
        // them to the template too
        Object.assign(this, JSON.parse(JSON.stringify(template)));
        this.autoRealize();
        this.points = this.points ?? this.maxPoints;
        this.hp = this.hp ?? this.maxHp;

        if(populateDefaultPowers) {
            this.defaultMeleeAbility = this.abilityByName(this.defaultMeleeAbilityName);
            this.defaultRangedAbility = this.abilityByName(this.defaultRangedAbilityName);
        }
        if(this.behaviorName && !this._behavior) {
            this.setBehavior(BEHAVIORS.newFromName(this.behaviorName));
        } else if(!this._behavior) {
            this.setBehavior(BEHAVIORS.newFromName("StandThereBehavior"));
        }
    }

    public get behavior(): Behavior {
        return this._behavior;
    }

    public get lightRadius(): number {
        return this.sightDistance;
    }

    public get maxHp(): number {
        let conbonus = this.statBonus('con');

        let addable = new AddableResult(this.baseMaxHp + conbonus);
        this.chain(Chain.maxHp, addable);
        let base = addable.value;

        let extraLevels = this.level - 1;
        let hpPerLevel = this.hpPerLevel + conbonus;

        return base + (extraLevels * hpPerLevel);
    }

    public get isBloody(): boolean {
        return this.hp <= this.maxHp / 2;
    }

    public get isMaster(): boolean {
        return !!this.className;
    }

    public isEquipped(item: EquippableItem): boolean {
        for(let slot of item.slots) {
            if(this.equipment[slot] === item) return true;
        }
        return false;
    }

    public isProficientWith(item: EquippableItem): boolean {
        let slot = item.slots[0];
        if(slot !== 'body' && slot !== 'mainHand' && slot !== 'offHand') return true;
        for(let tag of this.proficiencies) {
            if(item.matchesEquipmentTag(tag)) return true;
        }

        return false;
    }

    public setBehavior(behavior: Behavior, gm?: GameMaster) {
        if(gm) behavior.gm = gm;
        this._behavior = behavior;
        this.behaviorName = behavior.constructor.name;
        behavior.adopted(this);
        return behavior;
    }

    public beginningOfTurn(gm: GameMaster): void {
        // Abilities cool down
        for(let ability of this.abilities) {
            ability.doCooldown();
            if(ability.currentCooldown === Duration.Boss && this.refreshBossNextTurn) {
                ability.resetCooldown();
            }
        }
        this.refreshBossNextTurn = false;
        // This is when conditions do their thing
        for(let condition of this.conditions) {
            condition.beginningOfTurn(this, gm);
        }
        // Clear the 'last turn' flags
        this.lastTurn = [LastAction.Nothing];

        // If any of that killed me, die
        if(!this.alive) {
            gm.endTurn(this, Energy.MINOR);
            gm.player.message(`${this.name} dies!`);
        }
    }

    public didLastTurn(last: LastAction): boolean {
        if(last === LastAction.Nothing) {
            if(this.lastTurn.length === 0) return true;
            if(this.lastTurn.length === 1 &&
                this.lastTurn[0] === LastAction.Nothing) return true;
            return false;
        }
        return this.lastTurn.includes(last);
    }

    public recordAction(last: LastAction) {
        this.lastTurn.push(last);
    }

    public endOfTurn(gm: GameMaster): void {
        for(let condition of this.conditions) {
            condition.endOfTurn(this, gm);
            if(condition.duration == 0) {
                let display = condition.displayName || condition.name;
                this.message(`${display} wears off`);
            }
        }
        this.pruneExpiredConditions();
    }

    public pruneExpiredConditions(): void {
        this.conditions = this.conditions.filter(cond => cond.duration != 0);
    }

    public removeConditionNamed(name: string): void {
        let cond;
        while(cond = this.hasConditionNamed(name)) {
            cond.duration = 0;
            this.pruneExpiredConditions();
        }
    }

    public statBonus(statName: string, value?: number): number {
        let result = this.stats[statName];
        if(value) result = value;

        if(result === undefined) {
            throw `Unknown stat ${statName}!`;
        }
        result = Math.floor((result - 10) / 2);

        let addable = new AddableResult(result);
        this.chain(Chain.statBonus, addable, {statName})

        return addable.value;
    }

    //region Point buy system
    public buyStat(statName: string, direction=1, force=false): boolean {
        if(!force && !this.canBuyStat(statName, direction)) return false;
        let cost = this.pointCostAt(this.stats[statName], direction);
        this.points -= cost;
        this.stats[statName] += direction;
        return true;
    }

    public canBuyStat(statName: string, direction = 1): boolean {
        let curStatValue = this.stats[statName];
        if(curStatValue === undefined) return false;
        let proposedStatValue = curStatValue + direction;
        if(proposedStatValue < 8 || proposedStatValue > 18) return false;
        let cost = this.pointCostAt(this.stats[statName], direction);
        if(cost > this.points) return false;
        return true;
    }

    public pointCostAt(curAmount: number, direction=1): number {
        if(direction < 0) {
            return -(this.pointCostAt(curAmount-1));
        }
        if(curAmount >= 8 && curAmount <= 12) return 1;
        if(curAmount >= 13 && curAmount <= 15) return 2;
        if(curAmount === 16) return 3;
        if(curAmount === 17) return 4;
        return 0;
    }

    public totalPointCost(totalAmount: number): number {
        if(totalAmount === 10) return 0;
        if(totalAmount < 10) {
            return totalAmount - 10;
        } else {
            return (
                this.pointCostAt(totalAmount - 1) +
                this.totalPointCost(totalAmount - 1)
            );
        }
    }

    public get pointsSpent(): number {
        return this.maxPoints - this.points;
    }
    //endregion

    public savingThrow(gm: GameMaster): boolean {
        let die = gm.roll();
        this.chain(Chain.saveModify, die);
        return die.value >= 10;
    }

    public canAct(): boolean {
        let result = new AddableResult();
        this.chain(Chain.canAct, result);
        return result.passes;
    }

    public canMove(): boolean {
        let result = new AddableResult();
        this.chain(Chain.canMove,result);
        return result.passes;
    }

    public canSee(): boolean {
        let result = new AddableResult();
        this.chain(Chain.canSee,result);
        return result.passes;
    }

    public levelUp(statGain = 0): void {
        this.level = this.level + 1;
        this.message(`You are now level ${this.level}!`);
        for(let stat of ["str", "dex", "con", "int", "wis", "cha"]) {
            this.stats[stat] += statGain;
        }

        this.hp = this.maxHp;
    }

    //region Combat
    public get alive(): boolean {
        return this.hp > 0;
    }

    public advantageStatus(): number {
        let result = Dice.zero;
        this.chain(Chain.advantage, result);
        return result.value;
    }

    public addToHitBonuses(roll: Dice, vs: Character): Dice {
        this.chain(Chain.toHit, roll, {fromWho: this, toWho: vs});
        return roll;
    }

    public addDamageBonuses(roll: Dice, vs: Character): Dice {
        this.chain(Chain.damageModify, roll, {fromWho: this, toWho: vs});
        return roll;
    }

    public weaponForAbility(what?: Ability): Weapon {
        const imp = new Weapon(WEAPONS.ImprovisedWeapon);
        if(this.equipment.mainHand.isNothing()) return imp;
        // Effects don't have a tie to the Ability that enacted them,
        // but it's possible they might have a damage string that
        // involves a weaponry.  Improvising isn't /right/, but it
        // at least won't crash.
        if(!what) {
            console.warn("weaponForAbility called on null ability");
            return imp;
        }
        const weapon = this.equipment.mainHand as Weapon;

        // PBAoE can use either
        if(what.targeting === Targets.PBAoE) return weapon;

        let melee = what.targeting == Targets.Melee;
        if(weapon.isMelee && melee) return weapon;
        if(weapon.isRanged && !melee) return weapon;
        return imp;
    }

    public weaponDice(what: Ability): Dice {
        let weapon = this.weaponForAbility(what);
        let result = new Dice(weapon.damage);
        return result;
    }

    public calcDefense(def: Chain, fromWho?: Character): AddableResult {
        let result: AddableResult;
        if(def === Chain.ac) {
            result = new AddableResult(this.baseAc);
        } else {
            result = new AddableResult(10);
        }

        let strBonus = this.statBonus('str');
        let dexBonus = this.statBonus('dex');
        let conBonus = this.statBonus('con');
        let intBonus = this.statBonus('int');
        let wisBonus = this.statBonus('wis');
        let chaBonus = this.statBonus('cha');
        switch(def) {
            case Chain.ac:
                let armor = this.equipment.body as Armor;
                if(armor.plusDex || armor.isNothing()) {
                    let bonus = Math.max(dexBonus, intBonus);
                    result.add(bonus, 'Stat bonus');
                }
                break;
            case Chain.fort:
                let bonus = Math.max(strBonus, conBonus);
                result.add(bonus, "Stat bonus");
                break;
            case Chain.ref:
                bonus = Math.max(dexBonus, intBonus);
                result.add(bonus, "Stat bonus");
                break;
            case Chain.will:
                bonus = Math.max(wisBonus, chaBonus);
                result.add(bonus, "Stat bonus");
                break;
        }

        this.chain(def, result, {fromWho, toWho: this});

        return result;
    }

    public takeDamage(amount: Dice, fromWho?: Character): Dice {
        amount.minimum = 0;  // So damage doesn't accidentally heal

        this.chain(Chain.incomingDamage, amount, {
            fromWho, toWho: this,
        });

        let dmg = amount.value;

        if(dmg > this.tempHp) {
            dmg -= this.tempHp;
            this.tempHp = 0;
        } else {
            this.tempHp -= dmg;
            return Dice.zero;
        }

        // Not capping hp at 0 for last-minute heals, rez, whatever.
        this.hp -= dmg;

        for(let condition of this.conditions) {
            condition.takeDamage(amount);
        }
        this.pruneExpiredConditions();
        return amount;
    }

    public grantTempHp(amount: Dice) {
        let amt = amount.value;
        if(this.tempHp > amt) return;
        this.tempHp = amt;
    }

    public youDied(gm: GameMaster) {
        for(let condition of this.conditions) {
            condition.onDeath(this, gm);
        }
    }

    public heal(amount: number): number {
        amount = Math.floor(amount);
        if(amount <= 0) return 0;
        let addable = new AddableResult(amount);
        this.chain(Chain.healModify, addable);
        amount = addable.value;
        let target = this.hp + amount;
        if(target > this.maxHp) {
            amount -= (target - this.maxHp);
        }
        this.hp += amount;
        return amount;
    }
    //endregion

    /* Items */
    public receiveItem(...items: Item[]): void {
        this.inventory.push(...items);
    }

    public removeItem(...items: Item[]): boolean {
        for(let item of items) {
            let removed = removeFromArray(item, this.inventory);
            if(!removed) return false;
        }
        return true;
    }

    public hasItem(needle: Item): boolean {
        for(let item of this.inventory) {
            if(needle === item) { return true; }
        }
        return false;
    }

    /* End Items */

    public isReady(): boolean {
        return this.energy >= 0;
    }

    public tick(numTurns = 1): boolean {
        this.energy += numTurns;
        return this.isReady();
    }

    public expend(amount: number): number {
        let result = new AddableResult(amount);
        this.chain(Chain.expendEnergy, result, {amount});

        // Apply movement speed
        if(amount === Energy.MOVE) {
            let bonus =  Energy.MOVE - this.baseMovementSpeed;

            result.add(bonus, "Movement speed");
        }

        result.minimum = Energy.MIN;
        this.energy -= result.value;
        return this.energy;
    }

    public message(...msgs: string[]): void {
        let gm = this.behavior?.gm;
        if(gm) {
            msgs = msgs.map(msg=>`${gm.tickNum}: ${msg}`);
        }
        this.messages.push(...msgs);
        //console.log(...msgs);
    }

    public broadcast(gm: GameMaster, ...msgs: string[]): void {
        if(!this.broadcasts) return;
        gm.broadcast(this, ...msgs);
    }

    /* Chain */

    public chain(topic: Chain, result: AddableResult, args?: any): void {
        // Bit of a hack to try to get the gm if we don't already have one
        // (e.g. tests often call 'calcDefense' directly)
        if(args && !args.gm) {
            if(this.behavior) {
                args.gm = this.behavior.gm;
            }
        }

        // chain through everything that could possibly matter

        // Starting with equipment
        for(let slot in this.equipment) {
            let equipped = this.equipment[slot];
            equipped.recvChain(topic, result, this, args);
        }

        // Conditions
        for(let cond of this.conditions) {
            cond.recvChain(topic, result, this, args);
        }

        // Passives
        for(let passive of this.passives) {
            passive.recvChain(topic, result, this, args);
        }

        // Zone
        let tile = this.map?.tileAt(this.loc);

        if(tile && tile.zone && tile.zone.faction != this.faction) {
            let zone = tile.zone as ChainableZone;
            zone.recvChain(topic, result, this, args);
        }
    }

    /* End Chain */

    public distanceTo(other: Character): number {
        if(!this.map) return 0;

        let here = Point.fromLoc(this.loc);
        return here.distance(other.loc);
    }

    public receiveCondition(condition: Condition): boolean {
        this.conditions.push(condition);
        return true;
    }

    public hasConditionNamed(conditionName: string): Condition {
        return this.conditions.find(cond => cond.name === conditionName);
    }

    public get conditionString(): string {
        let result: string[] = [];
        if(!this.alive) return "DEAD";
        for(let cond of this.conditions) {
            result.push(cond.displayName || cond.name);
        }
        if(result.length === 0) {
            return "You are in normal condition"
        }
        return result.join(", ")
    }

    public gainPassive(condition: Condition): boolean {
        this.passives.push(condition);
        return true;
    }

    public hasPassiveNamed(passiveName: string): Condition {
        return this.passives.find(cond => cond.name === passiveName);
    }

    //region Ability functions
    public gainAbility(abilityName: string): Ability {
        if(this.abilityNames.includes(abilityName)) return;
        this.abilityNames.push(abilityName);
        return this.realizeAbility(abilityName);
    }

    public gainAbilityFromTemplate(template: IAbilityTemplate): Ability {
        let ability = new Ability(template);
        if(this.abilityNames.includes(ability.name)) return;
        this.abilityNames.push(ability.name);
        this.abilities.push(ability)
        this.postProcessAbilityGain(ability);
        return ability;
    }

    public postProcessAbilityGain(ability: Ability) {
        // assign default melee and ranged abilities if not there already.
        if(ability.basic) {
            if(!this.defaultMeleeAbility && ability.targeting === Targets.Melee) {
                this.assignDefaultMeleeAbility(ability);
            }
            if(!this.defaultRangedAbility && ability.targeting === Targets.Ranged) {
                this.assignDefaultRangedAbility(ability);
            }
        } else {
            for(let i=0; i < this.hotkeys.length; i++) {
                let hotkey = this.hotkeys[i];
                if(!hotkey) {
                    this.hotkeys[i] = ability;
                    break;
                }
            }
        }
    }

    public abilityByName(abilityName: string) {
        for(let ability of this.abilities) {
            if(ability.name === abilityName) {
                return ability;
            }
        }
        return null;
    }

    // This assigns the given ability to be the default melee ability,
    // but only if we haven't already assigned something non-basic,
    // and if the ability is instant
    public assignBetterMelee(ability: Ability) {
        if(this.defaultMeleeAbility.basic && ability.cooldown === Duration.Instant) {
            this.assignDefaultMeleeAbility(ability);
        }
    }

    // This assigns the given ability to be the default ranged ability,
    // but only if we haven't already assigned something non-basic
    public assignBetterRanged(ability: Ability) {
        if(this.defaultRangedAbility.basic && ability.cooldown === Duration.Instant) {
            this.assignDefaultRangedAbility(ability);
        }
    }

    public assignDefaultMeleeAbility(ability: Ability) {
        this.defaultMeleeAbility = ability;
        this.defaultMeleeAbilityName = ability.name;
    }

    public assignDefaultRangedAbility(ability: Ability) {
        this.defaultRangedAbility = ability;
        this.defaultRangedAbilityName = ability.name;
    }

    protected realizeAbilities() {
        if(this.abilities.length > 0) {
            // If we already have abilities, then we're being loaded
            this.abilities = this.abilities.map((a) => new Ability(a));
        } else {
            for (let name of this.abilityNames) {
                this.realizeAbility(name);
            }
        }
    }

    protected realizeAbility(abilityName: string): Ability {
        let ability = ABILITIES.newFromName(abilityName);
        this.abilities.push(ability);
        this.postProcessAbilityGain(ability);
        return ability;
    }
    //endregion

    public defeatedBoss() {
        for(let ability of this.abilities) {
            if(ability.currentCooldown === Duration.Boss) {
                ability.resetCooldown();
            }
        }
        // See https://gitlab.com/rostrander/tyranny-of-the-labyrinth/-/issues/58
        this.refreshBossNextTurn = true;
        this.levelUp();
    }

    protected autoRealize() {
        this.realizeAbilities();
        let iid2item: {[key:string]: EquippableItem} = {};
        this.inventory = this.inventory.map((i) => {
            let result = Item.fromTemplate(i);
            if(result.equippable) iid2item[result.iid] = result as EquippableItem;
            return result;
        });
        for(let equipmentKey in this.equipment) {
            let template = this.equipment[equipmentKey];
            let actual = iid2item[template.iid];
            if(!actual) {
                actual = Item.fromTemplate(template) as EquippableItem;
                if(!actual.isNothing()) console.warn("Could not re-link, missing or mismatched IID", template);
            }
            this.equipment[equipmentKey] = actual;
        }
        this.conditions = this.conditions.map((c) => new Condition(c));
        this.passives = this.passives.map((p) => new Condition(p));
        // Load up the hotkeys while we're at it too
        this.hotkeys = this.hotkeys.map((a) => {
            if(a) return this.abilityByName(a.name);
            return null;
        });
    }

    public toJSON() {
        let result = exportJsonExcept(this,
            'defaultMeleeAbility',
            'defaultRangedAbility',
            'behavior', '_behavior',
            'map',
        )
        return result;
    }
};
