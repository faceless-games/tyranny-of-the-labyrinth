import {XY, XYWH} from "./interfaces";

export enum Energy {
    TURN = 13,
    STANDARD = 7,
    MOVE = 5,
    MINOR = 3,
    MIN = 1,
    ALL = -1,
}

export function removeFromArray<T>(item: T, array: Array<T>): T {
    for(let index=0; index < array.length; index++) {
        if(array[index] == item) {
            array.splice(index, 1);
            return item;
        }
    }
    return null;
}

export function exportJsonExcept(item: any, ...exclude: string[]): any {
    let result: any = {};
    let excluded = new Set(exclude);
    for(let key in item) {
        if(!excluded.has(key)) {
            result[key] = item[key];
        }
    }
    return result;
}

export function templateOverride(template: any, override?: any): any {
    let result = JSON.parse(JSON.stringify(template));
    if(override) {
        Object.assign(result, override);
    }
    return result;
}

export function defaults<TemplateType>(defaults: any, ...templates: TemplateType[]): TemplateType[] {
    let result = templates.map((t) => {
        let duped = JSON.parse(JSON.stringify(defaults));
        Object.assign(duped, t);
        return duped;
    });
    return result;
}

// Courtesy: https://stackoverflow.com/a/21963136
export function uuidv4() {
    let u='',i=0;
    while(i++<36) {
        let c='xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'[i-1],r=Math.random()*16|0,v=c=='x'?r:(r&0x3|0x8);
        u+=(c=='-'||c=='4')?c:v.toString(16)
    }
    return u;
}

export class Point implements XY {
    constructor(public x: number, public y: number) {

    }

    static fromLoc(loc: XY) {
        return new Point(loc.x, loc.y);
    }

    static add(origin: XY, offset: XY): Point {
        return new Point(
            origin.x + offset.x,
            origin.y + offset.y
        );
    }

    static subtract(dest: XY, src: XY): Point {
        return new Point(
            src.x - dest.x,
            src.y - dest.y,
        );
    }

    static distance(origin: XY, other: XY, topology = 8): number {
        let dx = Math.abs(origin.x - other.x);
        let dy = Math.abs(origin.y - other.y);
        if(topology === 8) {
            return Math.max(dx, dy);
        } else if(topology === 4) {
            return dx + dy;
        }
        // Everything else gets Euclidian distance
        return Math.sqrt(dx*dx + dy*dy);
    }

    static sameAs(origin: XY, other: XY): boolean {
        return origin.x === other.x && origin.y === other.y;
    }

    public sameAs(other: XY): boolean {
        return Point.sameAs(this, other);
    }

    public add(offset: XY): Point {
        return Point.add(this, offset);
    }

    public subtract(src: XY): Point {
        return Point.subtract(this, src);
    }

    public distance(other: XY, topology=8): number {
        return Point.distance(this, other, topology);
    }

    public fastNormalized(): Point {
        // Normalization how I'd expect to see in a roguelike
        // (i.e. no trig, just the direction that moves me closest)
        let dx = 0;
        let dy = 0;
        if(this.x < 0) dx = -1;
        if(this.x > 0) dx = 1;
        if(this.y < 0) dy = -1;
        if(this.y > 0) dy = 1;
        return new Point(dx, dy);
    }

    public asXY(): XY {
        return {
            x: this.x,
            y: this.y,
        }
    }
}

export class BoundingBox implements XYWH {

    public static centered(center: XYWH) {
        let halfWidth = Math.floor(center.w / 2);
        let x = center.x - halfWidth;
        let halfHeight = Math.floor(center.h / 2);
        let y = center.y - halfHeight;

        return new BoundingBox(x, y, center.w, center.h);
    }

    public static fromXYWH(xywh: XYWH) {
        return new BoundingBox(
            xywh.x,
            xywh.y,
            xywh.w,
            xywh.h,
        );
    }

    public constructor(
        public x: number,
        public y: number,
        public w: number,
        public h: number,
    ) {
    }

    public get center(): XY {
        return {
            x: this.x + Math.floor(this.w / 2),
            y: this.y + Math.floor(this.h / 2)
        };
    }

    public get bottom(): number {
        return this.y + this.h - 1;
    }

    public get right(): number {
        return this.x + this.w - 1;
    }

    public get upperLeft(): XY {
        return {
            x: this.x,
            y: this.y,
        };
    }

    public get upperRight(): XY {
        return {
            x: this.right,
            y: this.y,
        }
    }

    public get lowerLeft(): XY {
        return {
            x: this.x,
            y: this.bottom,
        }
    }

    public get lowerRight(): XY {
        return {
            x: this.right,
            y: this.bottom,
        }
    }

    public intersects(other: XYWH) {
        let bb = BoundingBox.fromXYWH(other);
        return !(
            this.right < bb.x  ||
            this.x > bb.right  ||
            this.bottom < bb.y ||
            this.y > bb.bottom
        );
    }

    public contains(point: XY) {
        return (
            point.x >= this.x &&
            point.x <= this.right &&
            point.y >= this.y &&
            point.y <= this.bottom
        )
    }

    public inset(amount: number): BoundingBox {
        return new BoundingBox(
            this.x + amount,
            this.y + amount,
            this.w - (amount * 2),
            this.h - (amount * 2),
        );
    }
}

export class InvokeResult {
    constructor(public shouldExpend=true, public shouldEndTurn=true) {
    }

    public expend() {
        this.shouldExpend = true;
    }

    public expendMaybe(expend: boolean) {
        this.shouldExpend = this.shouldExpend || expend;
    }
}

export enum Chain {
    None,
    ac,
    fort,
    ref,
    will,
    AllDefenses,
    expendEnergy,
    damageModify,
    canMove,
    canAct,
    dieRoll,
    toHit,
    maxHp,
    healModify,
    incomingDamage,
    saveModify,
    onHitEnemy,
    onWasHit,
    canSee,
    statBonus,
    advantage,
}

export enum Targets {
    None,
    Self,
    Melee,
    Ranged,
    PBAoE,
    TAoE,
    Move,
}

export enum ConditionType {
    unconditional,
    hp,
    lastTurn,
    enemyAdjacent,
    wallAdjacent,
    enemyAdjacentAllies,
}

export enum HpType {
    Current,
    Max,
    Bloody,
}

export enum Comparison {
    LessThan,
    GreaterThan,
    Equal,
}

export const AUTOHIT = "auto";
export const NOHIT = "none";
export const NODAMAGE = "no_damage";

export enum Duration {
    AlwaysOn = -1,
    EndOfTurn = -2,
    EndOfNextTurn = -3,
    SaveEnds = -4,

    Instant = 1,
    Encounter = 30,
    Boss = -5,
};

export function duration_to_color(d: Duration): string {
    let result = null;
    switch(d) {
        case Duration.Instant:
            result = "#c6f35f";  // Theme's text-success
            break;
        case Duration.Encounter:
            result = "#ef962f";  // Theme's text-warning
            break;
        case Duration.Boss:
            result = "#ea4b48";  // Theme's text-danger
    }
    return result;
}

export enum LastAction {
    Nothing,
    Moved,
}

export enum EquipmentTag {
    Everything,
    LightArmor,        // AC <= 1
    MediumArmor,       // Armor w/ dex bonus
    HeavyArmor,        // Armor w/o dex bonus
    AllArmor,
    LightWeapon,      // Weapons <= 1d6
    HeavyWeapon,      // All other weapons
    OneHandedWeapon,
    TwoHandedWeapon,
    LightShield,      // AC <= 1
    HeavyShield,     // All other shields
    AllShield,
}

export function displayEquipmentTag(tag: EquipmentTag): string {
    let result = EquipmentTag[tag];
    switch(tag) {
        case EquipmentTag.LightArmor:
            return 'Light Armor';
        case EquipmentTag.MediumArmor:
            return 'Medium Armor';
        case EquipmentTag.HeavyArmor:
            return 'Heavy Armor';
        case EquipmentTag.AllArmor:
            return 'All Armor';
        case EquipmentTag.LightWeapon:
            return 'Light Weapon';
        case EquipmentTag.HeavyWeapon:
            return 'Heavy Weapon';
        case EquipmentTag.OneHandedWeapon:
            return "One-handed Weapon";
        case EquipmentTag.TwoHandedWeapon:
            return 'Two-handed Weapon';
        case EquipmentTag.LightShield:
            return 'Light Shield'
        case EquipmentTag.HeavyShield:
            return 'Heavy Shield'
        case EquipmentTag.AllShield:
            return 'All Shields'
    }
    return result;
}