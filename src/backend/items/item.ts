import {Invokable} from "../abilities/ability";
import {Effect, IEffectTemplate, PassiveEffect} from "../effects/effect";
import {Character, EquipmentSlots} from "../character";
import {GameMaster} from "../gamemaster";
import {AddableResult, Dice, IDieRollTemplate} from "../dice";
import {Chain, Energy, EquipmentTag, exportJsonExcept, uuidv4} from "../common";
import {EFFECTS} from "../effects/active";
import {chainDispatch, CombatChainArgs, ExpendEnergyArgs, IChainReceiver} from "../chain";
import {Registry} from "../registry";
import {IItemTemplate} from "../interfaces";
import {PASSIVEEFFECTS} from "../effects/passive";

export const ITEMTYPES = new Registry<Item>();
const registerItemType = ITEMTYPES.registryDecorator();

@registerItemType
export class Item implements IItemTemplate, IChainReceiver {
    public name: string
    private _displayName: string;
    public char: string
    public desc: string
    public iid: string;

    // Need an easy way to hint to the inventory screen if this is something
    // that can be used or equipped
    public usable: boolean;
    public equippable: boolean;

    public readonly itemType: string;
    public onBehalfOf: Character;

    // Due to subclassing, just doing the usual `new Item(template)` may not be enough:
    static fromTemplate(template: IItemTemplate): Item {
        return ITEMTYPES.newFromName(template.itemType, template);
    }

    constructor(template: IItemTemplate) {
        this.usable = false;
        this.equippable = false;
        this.setupDefaults();
        this.itemType = this.getItemType();
        Object.assign(this, JSON.parse(JSON.stringify(template)));
        if(!this.iid) {
            this.iid = uuidv4();
        }
    }

    protected setupDefaults() {}

    public toJSON() {
        let result = exportJsonExcept(this,
            'onBehalfOf',
        )
        return result;
    }

    public description(): string {
        return this.desc;
    }

    public get displayName(): string {
        if(this._displayName) return this._displayName;
        return this.name;
    }

    public set displayName(name: string) {
        this._displayName = name;
    }

    public recvChain(topic: Chain, result: AddableResult, onBehalfOf: Character, args?: any): boolean {
        return true;
    }

    public getItemType(): string {
        return this.constructor.name;
    }

}

export interface IUsableTemplate extends IItemTemplate {
    effectTemplates: IEffectTemplate[];
}

@registerItemType
export class UsableItem extends Item implements IUsableTemplate, Invokable {

    effectTemplates: IEffectTemplate[];
    protected effects: Effect[];

    constructor(template: IUsableTemplate) {
        super(template);
        this.usable = true;

        if(this.effectTemplates === undefined) {
            this.effectTemplates = [];
        }
        this.effects = EFFECTS.mapped(this.effectTemplates);
    }

    invoke(fromWho: Character, toWho: Character, gm: GameMaster): boolean {
        let result = false;
        for(let effect of this.effects) {
            let thisResult = effect.invoke(fromWho, toWho, gm);
            // We only return false (i.e. don't expend energy or use
            // the consumable) if none of its effects work.
            result = result || thisResult;
        }
        return result;
    }
}

export interface IEquippableItemTemplate extends IItemTemplate {
    slots: Array<keyof EquipmentSlots>;
    passiveEffectTemplates?: IEffectTemplate[];
}

@registerItemType
export class EquippableItem extends Item implements IEquippableItemTemplate {
    public slots: Array<keyof EquipmentSlots>;
    public desc: string;
    public passiveEffectTemplates: IEffectTemplate[];

    public passiveEffects: PassiveEffect[];
    protected _equipmentTags: EquipmentTag[];

    constructor(template: IEquippableItemTemplate) {
        super(template);
        this.equippable = true;
        this.passiveEffects = PASSIVEEFFECTS.mapped(this.passiveEffectTemplates);
    }

    protected setupDefaults() {
        super.setupDefaults();
        this.passiveEffectTemplates = [];
    }

    public isUnequppable(): boolean {
        return !this.isNothing();
    }

    public isNothing(): boolean {
        return this.name === NOTHINGNAME;
    }

    public get equipmentTags(): EquipmentTag[] {
        if(!this._equipmentTags) {
            this._equipmentTags = this.calcEquipmentTags();
        }
        return this._equipmentTags;
    }

    protected calcEquipmentTags(): EquipmentTag[] {
        return [];
    }

    public matchesEquipmentTag(tag: EquipmentTag): boolean {
        if(tag === EquipmentTag.Everything) return true;
        return this.equipmentTags.includes(tag);
    }

    recvChain(topic: Chain, result: AddableResult, onBehalfOf: Character, args?: any): boolean {
        super.recvChain(topic, result, onBehalfOf, args);
        for(let effect of this.passiveEffects) {
            effect.recvChain(topic, result, onBehalfOf, args);
        }
        return true;
    }

}

export const NOTHINGNAME = "nothing";
export const NOTHINGID = "0b1ec8bd-0764-4b48-9205-50ecbc3e5f29";

@registerItemType
export class NothingEquipment extends EquippableItem {
    damage: Dice;

    constructor() {
        super({
            name: NOTHINGNAME,
            char: "X",
            slots: [
                'hand', 'neck', 'body', 'hands', 'waist', 'feet',
                'ring1', 'ring2', 'mainHand', 'offHand'
            ],
            desc: "You have nothing equipped here",
            iid: NOTHINGID,
        });
        this.damage = new Dice({d: 4});
    }

}

export interface IArmorTemplate extends IEquippableItemTemplate {
    ac: number,
    plusDex?: boolean,
}

@registerItemType
export class Armor extends EquippableItem implements IArmorTemplate, IChainReceiver {
    public ac: number;
    public plusDex: boolean;

    constructor(template: IArmorTemplate) {
        super(template);
    }

    public description(): string {
        let plusdex = "";
        if (this.plusDex) plusdex = "+DEX";
        let acline = `AC +${this.ac}${plusdex}`;
        return `${this.desc}\n${acline}`;
    }

    public recvChain(topic: Chain, result: AddableResult, onBehalfOf: Character, args?: CombatChainArgs): boolean {
        super.recvChain(topic, result, onBehalfOf, args);
        return chainDispatch(this, topic, result, onBehalfOf, args);
    }

    public chainAC(result: AddableResult, args?: CombatChainArgs): boolean {
        result.add(this.ac, this.displayName);
        return true;
    }

    public chainExpendEnergy(result: AddableResult, args?: ExpendEnergyArgs): boolean {
        if(args.amount !== Energy.MOVE) return true;
        if(!this.plusDex) {
            result.add(1, "Armor speed penalty");
        }
        return true;
    }

    protected calcEquipmentTags(): EquipmentTag[] {
        let result = [];
        let isShield = this.slots.includes("offHand");
        if(!isShield) {
            result.push(EquipmentTag.AllArmor);
            if(this.ac <= 1) {
                result.push(EquipmentTag.LightArmor);
            }
            if(this.plusDex) {
                result.push(EquipmentTag.MediumArmor);
            } else {
                result.push(EquipmentTag.HeavyArmor);
            }
        } else {
            result.push(EquipmentTag.AllShield);
            if(this.ac <= 1) {
                result.push(EquipmentTag.LightShield);
            } else {
                result.push(EquipmentTag.HeavyShield);
            }
        }
        return result;
    }
}

export interface IWeaponTemplate extends IEquippableItemTemplate {
    damage: IDieRollTemplate;
    isMelee?: boolean;  // default true
    isRanged?: boolean; // default false
    isTwoHanded?: boolean; // default false
}

@registerItemType
export class Weapon extends EquippableItem implements IWeaponTemplate {
    damage: Dice;
    public isMelee: boolean;
    public isRanged: boolean;
    public isTwoHanded: boolean;

    constructor(template: IWeaponTemplate) {
        super(template);
        this.damage = new Dice(template.damage);
    }

    protected setupDefaults() {
        super.setupDefaults();
        this.isMelee = true;
        this.isRanged = false;
        this.isTwoHanded = false;
    }

    public description(): string {
        let dmgline = `Does ${this.damage.description()} damage`;
        return `${this.desc}\n${dmgline}`;
    }

    protected calcEquipmentTags(): EquipmentTag[] {
        let result: EquipmentTag[] = [];

        if(this.damage.n <= 1 && this.damage.d <= 6) {
            result.push(EquipmentTag.LightWeapon);
        } else {
            result.push(EquipmentTag.HeavyWeapon);
        }

        if(this.isTwoHanded) {
            result.push(EquipmentTag.TwoHandedWeapon);
        } else {
            result.push(EquipmentTag.OneHandedWeapon);
        }

        return result;
    }

}