import * as ROT from 'rot-js';

import {Chain} from "../common";
import {Armor, Item, UsableItem, Weapon} from "./item";
import {POTIONS} from "./potions";
import {ARMOR} from "./armors";
import {WEAPONS} from "./weapons";
import {TreasureTarget, Weightable, XY} from "../interfaces";
import {Character} from "../character";
import {IEffectTemplate} from "../effects/effect";

//region loot tables
const armors = {
    // Light
    "ClothArmor": 100,   // +0
    "PaddedArmor": 100,  // +1
    "Shield": 100,       // +1
    "HeavyShield": 100,  // +2
    "LeatherArmor": 100, // +2
    "HideArmor": 100,    // +3
    // Heavy
    "Chainmail": 100,    // +6
    "ScaleArmor": 100,   // +7
    "FullPlate": 100,    // +8
};

const potions = {
    "MinorHealingPotion": 100,
    "BarkskinPotion": 30,
};

const weapons = {
    // 1d4 weapons
    "Dagger": 100,
    "Whip": 100,
    // 1d6 weapons
    "Mace": 100,
    "Handaxe": 100,
    "Shortsword": 100,
    "Sickle": 100,
    "Hammer": 100,
    "HandCrossbow": 100,
    "Sling": 100,
    // 1d8 weapons
    "Longsword": 100,
    "Scimitar": 100,
    "Rapier": 100,
    "Pick": 100,
    // 2h 1d8 weapons
    "Quarterstaff": 100,
    "Crossbow": 100,
    "Shortbow": 100,
    // 2h 2d4 weapons
    "Greatclub": 100,
    "Scythe": 100,
    // 1h 1d10 weapons
    "Battleaxe": 100,
    "Broadsword": 100,
    "Flail": 100,
    "Warhammer": 100,
    "BastardSword": 100,
    // 2h 1d10 weapons
    "Longbow": 100,
    "HeavyCrossbow": 100,
    "Morningstar": 100,
    "Greatsword": 100,
    // 2h 1d12 weapons
    "Greatbow": 100,
    "Greataxe": 100,
    // 2h 2d6 weapons
    "HeavyFlail": 100,
    "Maul": 100,
}

export const masterLootTableByTier = [
    // T1
    {
        "gloves": 5,
        "plusarmor": 5,
        "plusweapon": 5,
        "plus_frw": 10,
        "weapon": 20,
        "armor": 20,
        "potion": 50,
        "nothing": 100,
    },
    // T2
    {
        "ring": 5,
        "gloves": 10,
        "plusarmor": 10,
        "plusweapon": 10,
        "plus_frw": 20,
        "potion": 50,
        "nothing": 100,
    },
]

export const ALL_DROPS = {
    armors,
    potions,
    weapons,
}
//endregion

export const STAT_TO_NAME: {[key: string]: string} = {
    'str': 'Strength',
    'con': 'Constitution',
    'dex': 'Dexterity',
    'int': 'Intelligence',
    'wis': 'Wisdom',
    'cha': 'Charisma',
}

abstract class Affix {
    abstract description: string;
    abstract shortDesc: string;
    public enchant: number;

    constructor() {
        this.enchant = 1;
    }

    abstract passiveEffectTemplates: IEffectTemplate[];
    abstract dup(): Affix;
}

class DefenseAffix extends Affix {
    static CHAIN_TO_DESC: {[key: string]: string} = {
        will: "Protects your Will",
        ref: "Helps your Reflex",
        fort: "Increases your fortitude",
    }

    public description: string;

    constructor(public chain: Chain, public shortDesc: string) {
        super();
        this.description = DefenseAffix.CHAIN_TO_DESC[Chain[chain]];
    }

    public get passiveEffectTemplates(): IEffectTemplate[] {
        return [{
            name: 'DefenseEffect',
            amount: this.enchant,
            defense: this.chain,
        }];
    }

    public dup(): Affix {
        let result = new DefenseAffix(this.chain, this.shortDesc);
        result.enchant = this.enchant;
        return result;
    }
}

class StatAffix extends Affix {
    shortDesc: string;

    constructor(public statName: string) {
        super();
        this.shortDesc = STAT_TO_NAME[statName];
    }

    public get description(): string {
        return `Increases your ${this.statName} bonus by ${this.enchant}`
    }

    public get passiveEffectTemplates(): IEffectTemplate[] {
        return [{
            name: "StatBonusEffect",
            amount: this.enchant,
            stat: this.statName,
        }];
    }

    dup(): Affix {
        let result = new StatAffix(this.statName);
        result.enchant = this.enchant;
        return result;
    }

}

class SimpleModifierAffix extends Affix {
    protected templates: IEffectTemplate[];

    constructor(
        public shortDesc: string,
        public description: string,
        ...passiveEffectTemplates: IEffectTemplate[]
    ) {
        super();
        this.templates = passiveEffectTemplates;
    }

    public get passiveEffectTemplates(): IEffectTemplate[] {
        return this.templates.map((template: IEffectTemplate) => {
            let result = Object.assign({}, template);
            result.amount = this.enchant;
            return result;
        });
    }

    dup(): Affix {
        let result = new SimpleModifierAffix(
            this.shortDesc,
            this.description,
            ...this.templates,
        );
        result.enchant = this.enchant;
        return result;
    }

}

class MultiAffix extends Affix {

    constructor(public affixes: Affix[] = []) {
        super();
    }

    public get description(): string {
        let descs = this.affixes.map(aff => aff.description);
        return descs.join(",");
    }

    public get shortDesc(): string {
        let descs = this.affixes.map(aff => aff.shortDesc);
        return descs.join(",");
    }

    public get passiveEffectTemplates(): IEffectTemplate[] {
        let result = [];
        for(let aff of this.affixes) {
            result.push(...aff.passiveEffectTemplates);
        }
        return result;
    }

    public dup(): Affix {
        let dupes = this.affixes.map(f => f.dup());
        let result = new MultiAffix(dupes);
        result.enchant = this.enchant;
        return result;
    }
}

const AFFIXES: {[key: string]: Affix}= {
    Fortitude: new DefenseAffix(Chain.fort, "Fortitude"),
    Reflex: new DefenseAffix(Chain.ref, "Reflexes"),
    Will: new DefenseAffix(Chain.will, "Will"),
    Ac: new DefenseAffix(Chain.ac, "Armor"),
    ToHit: new SimpleModifierAffix("Accuracy",
        "You are more likely to hit",
        {
            name: "ToHitModifierEffect",
        }),
    Damage: new SimpleModifierAffix("Injury",
        "You do more damage with hits", {
            name: "DamageModifierEffect",
        }),
    str: new StatAffix('str'),
    dex: new StatAffix('dex'),
    con: new StatAffix('con'),
    int: new StatAffix('int'),
    wis: new StatAffix('wis'),
    cha: new StatAffix('cha'),
}

export class Treasurer {
    public lootTable: Weightable;

    constructor(protected target:TreasureTarget, public tierNum: number) {
        this.setLootTableToTier(tierNum);
    }

    public adoptNextTier(): boolean {
        let nextTierNum = this.tierNum + 1;
        this.setLootTableToTier(nextTierNum);
        this.tierNum = nextTierNum;
        return true;
    }

    protected setLootTableToTier(tierNum: number) {
        let nextLoot = masterLootTableByTier[tierNum-1];
        if(!nextLoot) {
            let highest = masterLootTableByTier.length - 1;
            nextLoot = masterLootTableByTier[highest];
        }
        this.lootTable = nextLoot;
        return nextLoot;
    }

    public creatureDied(who: Character) {
        // In the future, this is where we can do things like
        // e.g. handle boss' loot tables.
        // Obviously this first pass is not it:
        if(!who.isMaster) {
            this.dropLoot(who.loc);
        }
    }

    public dropLoot(where: XY): Item {
        return this.dropLootFromTable(where, this.lootTable);
    }

    public dropLootFromTable(where: XY, table: Weightable): Item {
        let loot = this.createLootFromTable(table);
        if(!loot) return null;

        let placed = this.target.placeItemAt(loot, where);
        if(!placed) return null;
        return loot;
    }

    public createLoot(): Item {
        return this.createLootFromTable(this.lootTable);
    }

    public createLootFromTable(table: Weightable): Item {
        let category = ROT.RNG.getWeightedValue(table);
        let funcName = `loot_${category}`;
        let func = this[funcName as keyof Treasurer] as ()=>Item;
        if (!func) {
            throw `No loot category named ${category}`;
        }
        func = func.bind(this);
        return func();
    }

    protected loot_nothing(): Item {
        return null;
    }

    protected loot_test(): Item {
        let i = ROT.RNG.getUniformInt(1, 10000);
        return new Item({name: `Thingie ${i}`, char: "%", desc: `A ${i} kinda thing`});
    }

    protected loot_potion(): Item {
        let name = ROT.RNG.getWeightedValue(potions);
        return new UsableItem(POTIONS[name]);
    }

    protected loot_armor(): Item {
        let name = ROT.RNG.getWeightedValue(armors);
        return new Armor(ARMOR[name]);
    }

    protected loot_plusarmor(): Item {
        let armor = this.loot_armor() as Armor;
        let enchant = this.tierAppropriateEnchantment();

        let affix = AFFIXES.Ac
        affix.enchant = enchant;
        armor.passiveEffectTemplates.push(...affix.passiveEffectTemplates);
        let name = armor.displayName || armor.name;
        armor.displayName = `+${enchant} ${name}`;
        // Since that won't actually take effect, we need to return a new one:
        return new Armor(armor);
    }

    protected loot_plus_frw(): Item {
        let names = {
            "Helm": 100,
            "Boots": 100,
            "Belt": 100,
            "Amulet": 33,
        };
        let names_to_slot:{[key: string]: string} = {
            "Helm": "head",
            "Boots": "feet",
            "Belt": "waist",
            "Amulet": "neck",
        };
        let names_to_affix:{[key:string]: Affix} = {
            "Helm": AFFIXES.Will,
            "Boots": AFFIXES.Reflex,
            "Belt": AFFIXES.Fortitude,
        };

        let name = ROT.RNG.getWeightedValue(names);
        let affix: Affix;
        let enchant = this.tierAppropriateEnchantment();
        if(name === 'Amulet') {
            let affixes = [
                AFFIXES.Will,
                AFFIXES.Reflex,
                AFFIXES.Fortitude,
            ];
            affixes = ROT.RNG.shuffle(affixes);
            let aff1 = affixes.pop().dup();
            aff1.enchant = enchant;
            let aff2 = affixes.pop().dup();
            aff2.enchant = enchant;
            affix = new MultiAffix([aff1, aff2]);
        } else {
            affix = names_to_affix[name];
        }

        affix.enchant = enchant

        let armor = new Armor({
            name: `+${affix.enchant} ${name} of ${affix.shortDesc}`,
            char: ']',
            ac: 0,
            slots: [names_to_slot[name]],
            desc: affix.description,
            passiveEffectTemplates: affix.passiveEffectTemplates,
        });

        return armor;
    }

    protected loot_ring(): Armor {

        let enchant = this.tierAppropriateEnchantment();
        let affixesToAdd = 1;
        while(affixesToAdd < enchant && ROT.RNG.getPercentage() <= 50) {
            affixesToAdd += 1;
        }

        let affixKeys = ROT.RNG.shuffle(Object.keys(AFFIXES));
        let affixes = [];
        for(let i=0; i < affixesToAdd; i++) {
            let affix = AFFIXES[affixKeys.pop()].dup();
            affix.enchant = enchant;
            affixes.push(affix);
        }
        let affix = affixes.length === 1 ?  affixes[0] : new MultiAffix(affixes);

        let armor = new Armor({
            name: `+${affix.enchant} Ring of ${affix.shortDesc}`,
            char: '=',
            ac: 0,
            slots: ['ring1', 'ring2'],
            desc: affix.description,
            passiveEffectTemplates: affix.passiveEffectTemplates,
        });

        return armor;
    }

    protected tierAppropriateEnchantment(): number {
        if(this.tierNum === 1) return 1;
        let val = this.tierNum - 1;
        let pct = ROT.RNG.getPercentage();
        return pct < 66 ? val : val + 1;
    }

    protected loot_weapon(): Weapon {
        let name = ROT.RNG.getWeightedValue(weapons);
        return new Weapon(WEAPONS[name]);
    }

    protected loot_plusweapon(): Weapon {
        let weapon = this.loot_weapon();
        let enchant = this.tierAppropriateEnchantment();

        weapon.passiveEffectTemplates.push({
            name: "ToHitModifierEffect",
            amount: enchant,
        },{
            name: 'DamageModifierEffect',
            amount: enchant,
        });
        let name = weapon.displayName || weapon.name;
        weapon.displayName = `+${enchant} ${name}`;
        return new Weapon(weapon);
    }

    protected loot_gloves(): Armor {
        let stat = ROT.RNG.getItem(['str', 'dex', 'con', 'int', 'wis', 'cha']);
        let enchant = this.tierAppropriateEnchantment();
        let affix = AFFIXES[stat].dup();
        affix.enchant = enchant;
        let armor = new Armor({
            name: `+${enchant} gloves of ${affix.shortDesc}`,
            char: '[',
            ac: 0,
            slots: ['hands'],
            desc: affix.description,
            passiveEffectTemplates: affix.passiveEffectTemplates,
        });

        return armor;
    }
}