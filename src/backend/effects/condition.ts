import {Effect, IEffectTemplate, PassiveEffect} from "./effect";
import {Chain, Duration, Energy, exportJsonExcept} from "../common";
import {AddableResult} from "../dice";
import {PASSIVEEFFECTS} from "./passive";
import {GameMaster} from "../gamemaster";
import {EFFECTS} from "./active";
import {Character} from "../character";
import {DieCategory, IChainReceiver} from "../chain";
import {Faction} from "../interfaces";

export interface IConditionTemplate {
    name: string;
    displayName?: string;
    immuneFaction?: Faction;
    description?: string;
    beginningOfTurnTemplates?: IEffectTemplate[];
    passiveEffectTemplates?: IEffectTemplate[];
    onDeathEffects?: IEffectTemplate[];
    duration?: Duration;
    oneOff?: boolean;
    brokenByDamage?: number;

}

export class Condition implements IConditionTemplate, IChainReceiver {
    duration: Duration;
    beginningOfTurnTemplates: IEffectTemplate[];
    name: string;
    displayName: string;
    passiveEffectTemplates: IEffectTemplate[];
    oneOff: boolean;
    brokenByDamage: number;
    immuneFaction: Faction;

    // Realized from templates
    passiveEffects: PassiveEffect[];
    beginningOfTurnEffects: Effect[];
    onDeathEffects: Effect[];

    onBehalfOf: Character;

    constructor(template: IConditionTemplate, effectOverrides?: Partial<IEffectTemplate>, conditionOverrides?: Partial<IConditionTemplate>) {
        if(template===undefined) {
            throw "Bad template passed to Condition!";
        }
        Object.assign(this, JSON.parse(JSON.stringify(template)));
        if(conditionOverrides !== undefined) {
            Object.assign(this, JSON.parse(JSON.stringify(conditionOverrides)));
        }
        this.passiveEffects = PASSIVEEFFECTS.mapped(this.passiveEffectTemplates);
        this.beginningOfTurnEffects = EFFECTS.mapped(this.beginningOfTurnTemplates);
        this.onDeathEffects = EFFECTS.mapped(this.onDeathEffects);
        this.override(effectOverrides);
    }

    public recvChain(topic: Chain, result: AddableResult, onBehalfOf: Character, args?: any): boolean {
        for(let effect of this.passiveEffects) {
            effect.immuneFaction = this.immuneFaction;
            effect.recvChain(topic, result, onBehalfOf, args);
        }
        return true;
    }

    public toJSON() {
        let result = exportJsonExcept(this,
            'onBehalfOf',
        )
        return result;
    }

    protected override(overrides?: Partial<IEffectTemplate>) {
        if(overrides === undefined) return;
        for(let effect of this.passiveEffects) {
            Object.assign(effect, JSON.parse(JSON.stringify(overrides)));
        }
        for(let effect of this.beginningOfTurnEffects) {
            Object.assign(effect, JSON.parse(JSON.stringify(overrides)));
        }
    }

    /**
     * Called at the end of turn to decrease the amount of time
     * on this effect.
     * @returns If duration is a positive number, the number of
     *          turns remaining.  The other special cases return
     *          as follows:
     *          AlwaysOn => AlwaysOn
     *          EndOfTurn => 0, as it is the end of the turn
     *          EndOfNextTurn => EndOfTurn, so it'll expire next turn
     *          SaveEnds => 0 if it made the save, SaveEnds otherwise
     */
    public countDown(who: Character, gm: GameMaster): number {
        if(this.duration > 0) {
            return this.duration - 1;
        }
        switch (this.duration) {
            case Duration.AlwaysOn:
                return Duration.AlwaysOn;
            case Duration.EndOfTurn:
                return 0;
            case Duration.EndOfNextTurn:
                return Duration.EndOfTurn;
            case Duration.SaveEnds:
                let result = who.savingThrow(gm);
                return result ? 0 : Duration.SaveEnds;
            default:
                return 0;
        }
    }

    public beginningOfTurn(who: Character, gm: GameMaster) {
        for(let effect of this.beginningOfTurnEffects) {
            effect.immuneFaction = this.immuneFaction;
            effect.invoke(who, who, gm);
        }
    }

    public endOfTurn(who: Character, gm: GameMaster): void {
        if(this.oneOff && this.anyTookEffect()) this.expire();
        this.duration = this.countDown(who, gm);
    }

    public expire(): void {
        this.duration = 0;
    }

    public onDeath(who: Character, gm: GameMaster): void {
        for(let effect of this.onDeathEffects) {
            effect.immuneFaction = this.immuneFaction;
            effect.invoke(who, who, gm);
        }
    }

    public takeDamage(amount: AddableResult): void {
        if(this.brokenByDamage && amount.value > 0) {
            this.brokenByDamage -= 1;
            if(this.brokenByDamage <= 0) {
                this.expire();
            }
        }
    }

    protected anyTookEffect(): boolean {
        let result = false;
        for(let effect of this.passiveEffects) {
            result = result || effect.tookEffect;
        }
        for(let effect of this.beginningOfTurnEffects) {
            result = result || effect.tookEffect;
        }
        return result;
    }


}

export const CONDITIONS: { [key: string]: IConditionTemplate} = {
    //region Buffs
    Barkskin: {
        name: 'Barkskin',
        duration: Duration.Encounter,
        passiveEffectTemplates: [
            {
                name: 'DefenseEffect',
                amount: 2,
                defense: Chain.ac,
            },
        ]
    },
    ExtraDamageRoll: {
        name: 'ExtraDamageRoll',
        displayName: "Extra Damage Roll",
        duration: Duration.Encounter,
        passiveEffectTemplates: [
            {
                name: 'ExtraRollEffect',
                amount: 1,
                dieCategory: DieCategory.WEAPON,
            }
        ]
    },
    Hasted: {
        name: "Hasted",
        duration: Duration.Encounter,
        passiveEffectTemplates: [
            {
                name: "ExpendEnergyEffect",
                amount: -0.5,
                amountIsMultiplier: true,
                energyCategory: Energy.ALL,
            }
        ]
    },
    PlusToHit: {
        name: "PlusToHit",
        displayName: "Plus to Hit",
        duration: Duration.EndOfNextTurn,
        passiveEffectTemplates: [
            {
                name: "ToHitModifierEffect",
                amount: 1,
            }
        ]
    },
    PlusDamage: {
        name: "PlusDamage",
        displayName: "Plus Damage",
        duration: Duration.EndOfNextTurn,
        passiveEffectTemplates: [{
            name: "DamageModifierEffect",
            amount: 1,
        }]
    },
    PlusDefense: {
        name: "PlusDefense",
        displayName: "Plus Defense",
        duration: Duration.EndOfNextTurn,
        passiveEffectTemplates: [
            {
                name: "DefenseEffect",
                amount: 1,
                defense: Chain.ac,
            }
        ]
    },
    Inflicting: {
        name: "Inflicting",
        duration: Duration.Encounter,
        passiveEffectTemplates: [{
            name: "InflictionEffect",
            // inflictedCondition to be set by override
        }]
    },
    Pushy: {
        name: "Pushy",
        duration: Duration.Encounter,
        passiveEffectTemplates: [{
            name: "PushyEffect",
            amount: 1,
        }]
    },
    Thorny: {
        name: "Thorny",
        duration: Duration.EndOfNextTurn,
        passiveEffectTemplates: [{
            name: "ThornsEffect",
            amount: 1,
        }]
    },
    // This is exactly the same as 'Vulnerable', it only exists so that if
    // you gain resistance, it's displayed as such in the conditions pane.
    Resistant: {
        name: "Resistant",
        duration: Duration.EndOfNextTurn,
        passiveEffectTemplates: [{
            name: "IncomingDamageEffect",
            amount: -1,
        }]
    },
    //endregion
    //region Debuffs
    Dazed: {
        name: 'Dazed',
        duration: Duration.EndOfNextTurn,
        passiveEffectTemplates: [
            {
                name: "ExpendEnergyEffect",
                amount: 1,
                amountIsMultiplier: true,
                energyCategory: Energy.STANDARD,
            },
        ]
    },

    Slow: {
        name: 'Slow',
        duration: Duration.SaveEnds,
        passiveEffectTemplates: [
            {
                name: "ExpendEnergyEffect",
                amount: 1,
                amountIsMultiplier: true,
                energyCategory: Energy.MOVE,
            },
        ]
    },

    Weakened: {
        name: 'Weakened',
        duration: Duration.SaveEnds,
        passiveEffectTemplates: [
            {
                name: "DamageModifierEffect",
                amount: -0.5,
                amountIsMultiplier: true,
            },
        ]
    },

    Prone: {
        name: 'Prone',
        duration: Duration.AlwaysOn,
        oneOff: true,
        passiveEffectTemplates: [
            {
                name: "CanMoveEffect",
            }
        ]
    },

    Immobilized: {
        name: 'Immobilized',
        duration: Duration.SaveEnds,
        passiveEffectTemplates: [
            {
                name: "CanMoveEffect",
            }
        ]
    },

    Ongoing: {
        name: 'Ongoing',
        duration: Duration.SaveEnds,
        beginningOfTurnTemplates: [
            {
                name: "RawDamageEffect",
                amount: 0, // to be overridden upon infliction
            }
        ]
    },

    Impaired: {
        name: 'Impaired',
        duration: Duration.SaveEnds,
        passiveEffectTemplates: [
            {
                name: "ToHitModifierEffect",
                amount: -2
            },
            {
                name: "DamageModifierEffect",
                amount: -2
            },
            {
                name: "DefenseEffect",
                amount: -2,
                defense: Chain.AllDefenses,
            }
        ],
    },
    // Identical to above; again, just to get a different name in the status
    Debilitated: {
        name: 'Debilitated',
        duration: Duration.SaveEnds,
        passiveEffectTemplates: [
            {
                name: "ToHitModifierEffect",
                amount: -5
            },
            {
                name: "DamageModifierEffect",
                amount: -5
            },
            {
                name: "DefenseEffect",
                amount: -5,
                defense: Chain.AllDefenses,
            }
        ],
    },

    Asleep: {
        name: "Asleep",
        duration: Duration.SaveEnds,
        brokenByDamage: 1,
        passiveEffectTemplates: [
            {
                name: "CanActEffect",
            }
        ]
    },

    Vulnerable: {
        name: "Vulnerable",
        duration: Duration.SaveEnds,
        passiveEffectTemplates: [{
            name: "IncomingDamageEffect",
            amount: 1,
        }]
    },

    Saveless: {
        name: "Saveless",
        duration: Duration.SaveEnds,
        passiveEffectTemplates: [{
            name: "SaveModifyEffect",
            amount: -1,
        }]
    },

    Blind: {
        name: "Blind",
        duration: Duration.SaveEnds,
        passiveEffectTemplates: [{
            name: "BlindEffect",
        }]
    },
    // The same as 'PlusDefense' but with a more accurate name for statuses
    MinusDefense: {
        name: "MinusDefense",
        displayName: "Minus Defense",
        duration: Duration.EndOfNextTurn,
        passiveEffectTemplates: [{
            name: "DefenseEffect",
            amount: -1,
            defense: Chain.ac,
        }]
    },
    Disadvantaged: {
        name: "Disadvantaged",
        duration: Duration.EndOfNextTurn,
        passiveEffectTemplates: [{
            name: "AdvantageEffect",
            amount: -1,
        }],
    },
    //endregion
    //region Ability-specific conditions
    ComparativeArmorBuff: {
        name: "ComparativeArmorBuff",
        displayName: "Comparatively Armored",
        duration: Duration.Encounter,
        passiveEffectTemplates: [
            {
                name: "DefenseEffect",
                amount: 2,
                defense: Chain.AllDefenses,
            },
            {
                name: "DamageModifierEffect",
                amount: 2,
            },
            {
                name: "ToHitModifierEffect",
                amount: 2,
            }
        ],
    },
    Contagious: {
        name: "Contagious",
        duration: Duration.SaveEnds,
        onDeathEffects: [
            {
                name: "ContagiousEffect",
                radius: 2,
            }
        ]
    },
    PainAura: {
        name: "PainAura",
        displayName: "Pain Aura",
        duration: Duration.SaveEnds,
        beginningOfTurnTemplates: [
            {
                name: "BurstDamageEffect",
                radius: 1,
                damage: "1d10",
                includeOrigin: false,
            }
        ]
    },
    FastForwarded: {
        name: "FastForwarded",
        displayName: "Fast Forwarded",
        duration: Duration.Encounter,
        passiveEffectTemplates: [{
            name: "ExpendEnergyEffect",
            amount: -(Energy.MOVE - 1),
            energyCategory: Energy.MOVE,
        }],
    },
    //endregion

    //region Monster-specific conditions
    Vampirism: {
        name: "Vampirism",
        duration: Duration.AlwaysOn,
        passiveEffectTemplates: [{
            name: "LifestealEffect",
            amount: 0.25,
            amountIsMultiplier: true,
        }],
    }
    //endregion
}
