import {IConditionTemplate} from "./condition";
import {ConditionalEffect, registerPassive, ToHitAndDamageEffect} from "./passive";
import {CombatChainArgs} from "../chain";
import {Weapon} from "../items/item";
import {numberAmount, PassiveEffect} from "./effect";
import {AddableResult} from "../dice";
import {Chain, Comparison, ConditionType, Energy, HpType, LastAction} from "../common";
import {Faction} from "../interfaces";


@registerPassive
export class EinhanderEffect extends ToHitAndDamageEffect {
    protected shouldAffect(args: CombatChainArgs): boolean {
        const weapon = args.fromWho.equipment.mainHand as Weapon;
        if(weapon.isTwoHanded) return false;
        if(weapon.isRanged) return false;
        if(weapon.isNothing()) return false;
        if(args.fromWho.distanceTo(args.toWho) > 1) return false;

        return true;
    }
}

@registerPassive
export class ZweihanderEffect extends ToHitAndDamageEffect {
    protected shouldAffect(args: CombatChainArgs): boolean {
        const weapon = args.fromWho.equipment.mainHand as Weapon;
        if(!weapon.isTwoHanded) return false;
        if(weapon.isRanged) return false;
        if(weapon.isNothing()) return false;
        if(args.fromWho.distanceTo(args.toWho) > 1) return false;

        return true;
    }
}

@registerPassive
export class PointBlankEffect extends ToHitAndDamageEffect {
    protected shouldAffect(args?: any): boolean {
        const weapon = args.fromWho.equipment.mainHand as Weapon;
        if(!weapon.isRanged) return false;
        if(weapon.isNothing()) return false;
        if(args.fromWho.distanceTo(args.toWho) > 1) return false;

        return true;
    }
}

@registerPassive
export class ToughnessEffect extends PassiveEffect {
    public chainMaxHp(result: AddableResult): boolean {
        let amount = numberAmount(this.amount, this.onBehalfOf);
        this.tookEffect = true;

        amount *= this.onBehalfOf.level;
        result.add(amount, "Toughness bonus");
        return true;
    }
}

@registerPassive
export class HpDamageEffect extends ConditionalEffect {
    public chainDamageModify(result: AddableResult, args: CombatChainArgs): boolean {
        return this.affect(result, args);
    }
}

export const PASSIVES: { [key: string]: IConditionTemplate } = {
    Einhander: {
        name: "Einhander",
        description: "+1 to hit and damage in melee with one-handed weapons",
        passiveEffectTemplates: [
            { name: "EinhanderEffect", amount: 1 }
        ]
    },
    Zweihander: {
        name: "Zweihander",
        description: "+1 to hit and damage in melee with two-handed weapons",
        passiveEffectTemplates: [
            { name: "ZweihanderEffect", amount: 1 }
        ]
    },
    Toughness: {
        name: "Toughness",
        description: "Gain 4 max HP per level",
        passiveEffectTemplates: [{
            name: "ToughnessEffect", amount: 4,
        }]
    },
    BloodRage: {
        name: "BloodRage",
        displayName: "Blood Rage",
        description: "+2 damage when under half health",
        passiveEffectTemplates: [{
            name: "HpDamageEffect",
            conditionType: ConditionType.hp,
            hpType: HpType.Bloody,
            comparison: Comparison.Equal,
            targetFaction: Faction.Player,
            amount: 2,
        }]
    },
    Bloodseeker: {
        name: "Bloodseeker",
        description: "+2 damage to enemies under half health",
        passiveEffectTemplates: [{
            name: "HpDamageEffect",
            conditionType: ConditionType.hp,
            hpType: HpType.Bloody,
            comparison: Comparison.Equal,
            targetFaction: Faction.Enemy,
            amount: 2,
        }]
    },
    PunchingDown: {
        name: "PunchingDown",
        displayName: "Punching Down",
        description: "+1 damage to enemies with less maxhp than you",
        passiveEffectTemplates: [{
            name: "HpDamageEffect",
            conditionType: ConditionType.hp,
            hpType: HpType.Max,
            comparison: Comparison.LessThan,
            targetFaction: Faction.Enemy,
            amount: 1,
        }]
    },
    DefiantRage: {
        name: "DefiantRage",
        displayName: "Defiant Rage",
        description: "+2 damage to enemies with more current hp than you",
        passiveEffectTemplates: [{
            name: "HpDamageEffect",
            conditionType: ConditionType.hp,
            hpType: HpType.Current,
            comparison: Comparison.GreaterThan,
            targetFaction: Faction.Enemy,
            amount: 2,
        }]
    },
    DefyTheStrong: {
        name: "DefyTheStrong",
        displayName: "Defy the Strong",
        description: "+1 AC vs enemies with more max hp than you",
        passiveEffectTemplates: [{
            name: "DefenseEffect",
            conditionType: ConditionType.hp,
            hpType: HpType.Max,
            comparison: Comparison.GreaterThan,
            targetFaction: Faction.Enemy,
            amount: 1,
            defense: Chain.ac,
        }]
    },
    DisdainTheWeak: {
        name: "DisdainTheWeak",
        displayName: "Disdain the Weak",
        description: "+1 AC vs enemies with less max hp than you",
        passiveEffectTemplates: [{
            name: "DefenseEffect",
            conditionType: ConditionType.hp,
            hpType: HpType.Max,
            comparison: Comparison.LessThan,
            targetFaction: Faction.Enemy,
            amount: 1,
            defense: Chain.ac,
        }]
    },
    Healer: {
        name: "Healer",
        description: "Gain 1 additional HP when healed",
        passiveEffectTemplates: [{
            name: "HealModifyEffect",
            amount: 1,
        }]
    },
    CautiousCadence: {
        name: "CautiousCadence",
        displayName: "Cautious Cadence",
        description: "+2 AC if you moved last turn",
        passiveEffectTemplates: [{
            name: "DefenseEffect",
            conditionType: ConditionType.lastTurn,
            lastTurn: LastAction.Moved,
            amount: 2,
            defense: Chain.ac,
        }]
    },
    Runner: {
        name: "Runner",
        description: "Effectively increase your movement speed by 1",
        passiveEffectTemplates: [{
            name: "ExpendEnergyEffect",
            amount: -1,
            energyCategory: Energy.MOVE,
        }]
    },
    CurseOfRecklessness: {
        name: "CurseOfRecklessness",
        displayName: "Curse of Recklessness",
        description: "You're less likely to hit but you'll hit harder when you do",
        passiveEffectTemplates: [{
            name: "ToHitAndDamageEffect",
            amount: -1,
            damageAmount: 1,
        }],
    },
    GreaterRecklessness: {
        name: "GreaterRecklessness",
        displayName: "Greater Recklessness",
        description: "Flail even more wildly for even more damage and even less accuracy",
        passiveEffectTemplates: [{
            name: "ToHitAndDamageEffect",
            amount: -3,
            damageAmount: 2,
        }],
    },
    CurseOfCaution: {
        name: "CurseOfCaution",
        displayName: "Curse of Caution",
        description: "You're more likely to hit but don't hit as hard",
        passiveEffectTemplates: [{
            name: "ToHitAndDamageEffect",
            amount: 1,
            damageAmount: -1,
        }],
    },
    GreaterCaution: {
        name: "GreaterCaution",
        displayName: "Greater Caution",
        description: "+2 to hit, but -3 to damage.",
        passiveEffectTemplates: [{
            name: "ToHitAndDamageEffect",
            amount: 2,
            damageAmount: -3,
        }],
    },
    DefyTheOdds: {
        name: "DefyTheOdds",
        displayName: "Defy the Odds",
        description: "+1 to all defenses if you are adjacent to two or more enemies",
        passiveEffectTemplates: [{
            name: "DefenseEffect",
            conditionType: ConditionType.enemyAdjacent,
            amount: 0,
            perAdjacent: 0.5,
            maxAdjacentBonus: 1,
            defense: Chain.AllDefenses,
        }],
    },
    WallShield: {
        name: "WallShield",
        displayName: "Wall Shield",
        description: "+1 AC per adjacent wall (max 4)",
        passiveEffectTemplates: [{
            name: "DefenseEffect",
            conditionType: ConditionType.wallAdjacent,
            amount: 0,
            perAdjacent: 1,
            maxAdjacentBonus: 4,
            defense: Chain.ac,
        }]
    },
    Cornered: {
        name: "Cornered",
        description: "+1 damage per adjacent wall (max 4)",
        passiveEffectTemplates: [{
            name: "HpDamageEffect",
            conditionType: ConditionType.wallAdjacent,
            amount: 0,
            perAdjacent: 1,
            maxAdjacentBonus: 4,
        }]
    },
    Isolate: {
        name: "Isolate",
        description: "+2 damage vs enemies with no adjacent allies",
        passiveEffectTemplates: [{
            name: "HpDamageEffect",
            conditionType: ConditionType.enemyAdjacentAllies,
            comparison: Comparison.Equal,
            targetAdjacentAllies: 0,
            amount: 2,
            perAdjacent: 1,
        }]
    },
    PointBlank: {
        name: "PointBlank",
        displayName: "Point Blank",
        description: "+1 to hit and damage with ranged weapons against adjacent enemies",
        passiveEffectTemplates: [
            { name: "PointBlankEffect", amount: 1 }
        ]
    },
}
