import * as ROT from "rot-js";

import {Character} from "./character";
import {GameMaster} from "./gamemaster";
import {Energy, Point} from "./common";
import {Registry} from "./registry";
import {Faction, Weightable, XY} from "./interfaces";
import {Ability} from "./abilities/ability";

interface IBehaviorTemplate {
    gm: GameMaster,
    character: Character,
}

export abstract class Behavior {
    public gm: GameMaster;
    public character: Character;

    public constructor(template?: IBehaviorTemplate) {
        this.gm = template?.gm;
        this.character = template?.character;
    }

    public adopted(who: Character) {
        this.character = who;
    }

    abstract act(): boolean;
}

export const BEHAVIORS = new Registry<Behavior>();
export const registerBehavior = BEHAVIORS.registryDecorator();

interface PathInfo {
    relative: XY;
    absolute: XY;
}

@registerBehavior
export class StandThereBehavior extends Behavior {

    public act() {
        this.gm.endTurn(this.character, Energy.MINOR);
        return true;
    }
}

abstract class MonsterBehavior extends Behavior {
    private _target: Character;
    protected lastSawTarget: XY;

    public get target(): Character {
        if(!this._target || !this._target.alive) {
            this._target = this.findTarget();
        }
        return this._target;
    }

    public findTarget(): Character {
        if(this.character.faction !== Faction.Player) {
            return this.gm.player;
        } else {
            return this.gm.closestEnemyTo(this.character);
        }
    }

    public canSeeTarget(): boolean {
        let tile = this.gm.map.tileAt(this.character.loc);
        if(!tile.lit) return false;

        let dist = Point.distance(this.target.loc, this.character.loc);
        if(!this.character.canSee()) {
            return dist <= 1;
        }
        return dist <= this.character.sightDistance
    }

    public canCenterOnTarget(what?: Ability): boolean {
        what = what || this.character.defaultRangedAbility;
        return this.gm.canCenterAbility(this.character, what, this.target.loc);
    }

    public distanceToTarget(): number {
        return Point.distance(this.target.loc, this.character.loc);
    }

    public bumpTarget(): boolean {
        let {x:cx, y:cy} = this.character.loc;
        let dx = this.target.loc.x - cx;
        let dy = this.target.loc.y - cy;
        return this.gm.bump(this.character,{x: dx, y: dy});
    }

    public nextStepToward(targetLoc: XY, ignoreChars: boolean): PathInfo {
        let {x:cx, y:cy} = this.character.loc;
        let astar = new ROT.Path.AStar(
            targetLoc.x, targetLoc.y,
            this.gm.passableCallback(this.character.loc, ignoreChars));
        let path: XY[] = [];
        astar.compute(cx, cy, (x, y) => {
            if(cx != x || cy != y) {
                path.push({x,y});
            }
        });

        if(path.length >= 1) {
            let {x: px, y: py} = path[0];
            let dx = px - cx;
            let dy = py - cy;
            return {
                relative: {x: dx, y: dy},
                absolute: {x: px, y: py},
            }
        }
        return null;
    }

    public moveTowardTarget(orLoc: XY = null): boolean {
        let targetLoc = orLoc || this.target.loc;
        let nextStep = this.nextStepToward(targetLoc, false);

        if(nextStep) {
            return this.gm.bump(this.character, nextStep.relative);
        }
        return false;

    }

    public fleeTarget(): boolean {
        let vector = Point.subtract(this.target.loc, this.character.loc).fastNormalized();
        return this.gm.bump(this.character, {x: vector.x, y: vector.y});
    }

    protected recordTargetLoc(): void {
        this.lastSawTarget = this.target.loc;
    }

    public sitThere(): boolean {
        this.gm.endTurn(this.character, Energy.MINOR);
        return true;
    }
}

@registerBehavior
export class MeleeCombatBehavior extends MonsterBehavior {

    public act() {
        let acted = false;
        if(!this.target) return this.sitThere();
        if(!this.canSeeTarget()) {
            if(this.lastSawTarget) {
                acted = this.moveTowardTarget(this.lastSawTarget)
            }
            if(!acted) this.gm.endTurn(this.character, Energy.MINOR);
            return true;
        }

        this.recordTargetLoc();
        if(this.distanceToTarget() <= 1) {
            acted = this.bumpTarget();
        } else {
            acted = this.moveTowardTarget();
        }
        if(!acted) this.gm.endTurn(this.character, Energy.MINOR);
        return true;
    }
}

@registerBehavior
export class DefaultRangedBehavior extends MonsterBehavior {
    act(): boolean {
        let acted = false;
        if(!this.target) return this.sitThere();
        if(!this.canSeeTarget() || !this.canCenterOnTarget()) {
            this.gm.endTurn(this.character, Energy.MINOR);
            return true;
        }
        // We don't record the location of the target for chasing purposes
        // because ranged attackers don't chase; they wait and ambush.
        if(this.distanceToTarget() <= 1) {
            // Run away!
            acted = this.fleeTarget();
        } else {
            acted = this.gm.attack(
                this.character,
                this.character.defaultRangedAbility,
                this.target.loc);
        }
        if(!acted) this.gm.endTurn(this.character, Energy.MINOR);
        return true;
    }
}

@registerBehavior
export class BossBehavior extends MonsterBehavior {
    public weightList: Weightable;

    act(): boolean {
        let acted = false;
        if(!this.target) return this.sitThere();
        let power = this.choosePower();
        if(power.isMelee() && this.canSeeTarget()) {
            this.recordTargetLoc();
            if(this.distanceToTarget() <= 1) {
                acted = this.gm.attack(
                    this.character,
                    power,
                    this.target.loc);
            } else {
                acted = this.moveTowardTarget();
            }
        }
        if(power.isMelee() && !this.canSeeTarget() && this.lastSawTarget) {
            acted = this.moveTowardTarget(this.lastSawTarget);
        }
        if(power.isRanged() && this.canSeeTarget() && this.canCenterOnTarget(power)) {
            acted = this.gm.attack(
                this.character,
                power,
                this.target.loc);
        }

        if(!acted) this.gm.endTurn(this.character, Energy.MINOR);
        return true;
    }



    public adopted(who: Character) {
        super.adopted(who);
        this.weightList = {};
        for(let name of who.abilityNames) {
            let ability = who.abilityByName(name);
            if(ability.basic) continue;
            this.weightList[name] = 100;
        }
    }

    public choosePower(): Ability {
        let name = ROT.RNG.getWeightedValue(this.weightList);
        let ability = this.character.abilityByName(name);
        if(!ability.isReady()) {
            ability = this.appropriateDefaultPower(ability);
        }
        return ability;
    }

    protected appropriateDefaultPower(sub: Ability): Ability {
        if(sub.isMelee()) {
            return this.character.defaultMeleeAbility;
        }
        return this.character.defaultRangedAbility;
    }
}