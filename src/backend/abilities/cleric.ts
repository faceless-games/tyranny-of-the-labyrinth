import {IAbilityTemplate} from "./ability";
import {Chain, Duration, NODAMAGE, NOHIT, Targets} from "../common";
import {CONDITIONS} from "../effects/condition";

export const CLERIC_ABILITIES: {[key:string]: IAbilityTemplate} = {
    "HolyLance": {
        // Ranged, Wis v Ref, 1d6+wis, +2 to hit until EoNT
        name: "HolyLance",
        displayName: "Holy Lance",
        description: "Shoot a holy ray at your target, improving your aim",
        targeting: Targets.Ranged,
        range: 10,
        tier: 1,
        toHit: "wis vs ref",
        damage: "1d6+wis",
        invokerEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.PlusToHit,
                effectOverrides: {amount: 2}
            }
        ],
        techExtra: "+2 tohit",
    },
    "RadiantBulwark": {
        //Radiant Bulwark: Ranged, Wis v Ref, 1d6+wis,  +CHA AC until EoNT
        name: "RadiantBulwark",
        displayName: "Radiant Bulwark",
        description: "Smite from afar and be protected at the same time",
        targeting: Targets.Ranged,
        range: 10,
        tier: 1,
        toHit: "wis vs ref",
        damage: "1d6+wis",
        invokerEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.PlusDefense,
                effectOverrides: {amount: 'cha'}
            }
        ],
        techExtra: "+cha AC",
    },
    "ShieldingSiege": {
        // Shielding Siege: Melee, 1W+str, CHA temp hp
        name: "ShieldingSiege",
        displayName: "Shielding Siege",
        description: "Hit and gain temporary health",
        targeting: Targets.Melee,
        tier: 1,
        invokerEffects: [
            {
                name: 'BestowTempHpEffect',
                amount: 'cha',
            }
        ],
        techExtra: "+cha temp hp",
    },
    "RedoublingSiege": {
        //Redoubling Siege: Melee, 1W+str, +CON tohit until EoNT
        name: "RedoublingSiege",
        displayName: "Redoubling Siege",
        description: "Hit and add your constitution to the next swing",
        targeting: Targets.Melee,
        tier: 1,
        invokerEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.PlusToHit,
                effectOverrides: {amount: 'con'}
            }
        ],
        techExtra: "+con tohit",
    },
    "InduceFear": {
        // Induce Fear: Wis vs Will, Knockback 10
        name: "InduceFear",
        displayName: "Induce Fear",
        description: "Force your target to flee in terror!",
        cooldown: Duration.Encounter,
        targeting: Targets.Ranged,
        range: 20,
        tier: 1,
        toHit: "wis vs will",
        damage: NODAMAGE,
        onHitEffects: [{
            name: "PushbackEffect",
            amount: 10,
        }],
        techExtra: "knockback 10",
    },
    "DivineBlast": {
        // Divine Blast: PBaoE 3, Wis vs Ref, 1d8+wis, +2 tohit EoNT
        name: "DivineBlast",
        displayName: "Divine Blast",
        description: "Blast everyone near you with holy fire, strengthening yourself",
        cooldown: Duration.Encounter,
        targeting: Targets.PBAoE,
        radius: 2,
        tier: 1,
        toHit: "wis vs ref",
        damage: "1d8+wis",
        invokerEffects: [
            {
                name: 'BestowConditionEffect',
                condition: CONDITIONS.PlusToHit,
                effectOverrides: {amount: 2}
            }
        ],
        techExtra: "+2 tohit",
    },
    "DazingSiege": {
        // Dazing Siege: 1W+str, dazed EoNT
        name: "DazingSiege",
        displayName: "Dazing Siege",
        description: "Hit so hard you daze your opponent",
        cooldown: Duration.Encounter,
        targeting: Targets.Melee,
        tier: 1,
        onHitEffects: [{
            name: 'BestowConditionEffect',
            condition: CONDITIONS.Dazed,
        }],
        techExtra: "daze",
    },
    "RecuperatingSiege": {
        name: "RecuperatingSiege",
        displayName: "Recuperating Siege",
        description: "Hurt them, heal you",
        cooldown: Duration.Encounter,
        targeting: Targets.Melee,
        tier: 1,
        invokerEffects: [
            {name: "HealingEffect", amount: 0.15, amountIsMultiplier: true}
        ],
        techExtra: "heal 15%",
    },
    "DivineRebuke": {
        name: "DivineRebuke",
        displayName: "Divine Rebuke",
        description: "Blast your enemy and make them vulnerable",
        cooldown: Duration.Boss,
        targeting: Targets.Ranged,
        range: 20,
        tier: 1,
        toHit: "wis vs will",
        damage: "2d8+wis",
        onHitEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.Vulnerable,
            effectOverrides: {amount: 5}
        }],
        techExtra: "vulnerability 5",
    },
    "FierySiege": {
        name: "FierySiege",
        displayName: "Fiery Siege",
        description: "Strike your enemy, set them aflame!",
        cooldown: Duration.Boss,
        targeting: Targets.Melee,
        damage: "2W+str",
        tier: 1,
        onHitEffects: [
            {
                name: "BestowConditionEffect",
                condition: CONDITIONS.Saveless,
            },
            {
                name: "BestowConditionEffect",
                condition: CONDITIONS.Ongoing,
                effectOverrides: {amount: 5},
            },
        ],
        techExtra: "ongoing 5, -1 to saves",
    },
    // T2
    "LightOfJudgement": {
        name: "LightOfJudgement",
        displayName: "Light of Judgement",
        description: "A zone that lowers enemy defenses",
        targeting: Targets.TAoE,
        radius: 0,
        tier: 2,
        range: 10,
        toHit: NOHIT,
        invokerEffects: [{
            name: "ZoneEffect",
            radius: 0,
            zonePassives: [{
                name: "DefenseEffect",
                amount: -1,
                defense: Chain.AllDefenses,
            }]
        }],
        techExtra: "zone: -1 all defenses",
    },
    "HolyMark": {
        name: "HolyMark",
        displayName: "Holy Mark",
        description: "Harm your enemy and make them vulnerable",
        targeting: Targets.Ranged,
        range: 10,
        tier: 2,
        toHit: "wis vs will",
        damage: "1d6+wis",
        onHitEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.Vulnerable,
            effectOverrides: {amount: 1}
        }],
        techExtra: "vulnerable",
    },
    "ScourgingSiege": {
        name: "ScourgingSiege",
        displayName: "Scouring Siege",
        description: "A strike that makes your next strike hit harder",
        targeting: Targets.Melee,
        tier: 2,
        invokerEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.PlusDamage,
        }],
        techExtra: "+1 damage",
    },
    "HealWounds": {
        name: "HealWounds",
        displayName: "Heal Wounds",
        description: "Heal yourself for 20% of your max hp",
        cooldown: Duration.Encounter,
        targeting: Targets.Self,
        tier: 2,
        invokerEffects: [{
            name: 'HealingEffect',
            amount: 0.2,
            amountIsMultiplier: true,
        }],
        techExtra: "heal 20%",
    },
    "ExposeCorruption": {
        name: "ExposeCorruption",
        displayName: "Expose Corruption",
        description: "Harm your enemy and make them even more vulnerable",
        cooldown: Duration.Encounter,
        targeting: Targets.Ranged,
        range: 10,
        tier: 2,
        toHit: "wis vs will",
        damage: "2d10+wis",
        onHitEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.Vulnerable,
            effectOverrides: {amount: 3},
        }],
        techExtra: "vulnerable 3",
    },
    "RebukingSiege": {
        name: "RebukingSiege",
        displayName: "Rebuking Siege",
        description: "Slam your weapon into your enemy, knocking them down and back",
        cooldown: Duration.Encounter,
        targeting: Targets.Melee,
        tier: 2,
        damage: "2W+str",
        onHitEffects: [{
            name: "PushbackEffect",
            amount: 2,
        }, {
            name: "BestowConditionEffect",
            condition: CONDITIONS.Prone,
        }],
        techExtra: "prone, knockback 2",
    },
    "EnforcedPacifism": {
        name: "EnforcedPacifism",
        displayName: "Enforced Pacifism",
        description: "Your enemy does nothing unless provoked",
        cooldown: Duration.Boss,
        targeting: Targets.Ranged,
        range: 10,
        tier: 2,
        damage: NODAMAGE,
        toHit: "wis vs will",
        onHitEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.Asleep
        }],
        techExtra: "sleep",
    },
    "HolyWeapon": {
        name: "HolyWeapon",
        displayName: "Holy Weapon",
        description: "Imbue your weapon with the power to shred defenses",
        cooldown: Duration.Boss,
        targeting: Targets.Self,
        tier: 2,
        invokerEffects: [{
            name: 'BestowConditionEffect',
            condition: CONDITIONS.Inflicting,
            effectOverrides: {
                inflictedCondition: CONDITIONS.PlusDefense,
                inflictedOverride: {amount: -2}
            }
        }],
        techExtra: "mode: -2 defenses on hit",
    },
    // T3
    "RootingSiege": {
        name: "RootingSiege",
        displayName: "Rooting Siege",
        description: "Hit your enemy and root them in place",
        targeting: Targets.Melee,
        tier: 3,
        onHitEffects: [{
            name: 'BestowConditionEffect',
            condition: CONDITIONS.Immobilized,
        }],
        techExtra: "immobilize",
    },
    "UnbalancingLight": {
        name: "UnbalancingLight",
        displayName: "Unbalancing Light",
        description: "Damage and disorient your foe to lower their defenses",
        targeting: Targets.Ranged,
        tier: 3,
        range: 10,
        toHit: "wis vs will",
        damage: "1d8+wis",
        onHitEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.MinusDefense,
            effectOverrides: {
                defense: Chain.AllDefenses,
            },
        }],
        techExtra: "-1 all defenses",
    },
    "KnockoutSiege": {
        name: "KnockoutSiege",
        displayName: "Knockout Siege",
        description: "Hit your enemy so hard it knocks them out",
        targeting: Targets.Melee,
        tier: 3,
        cooldown: Duration.Encounter,
        onHitEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.Asleep,
            conditionOverride: {brokenByDamage: 2}
        }],
        techExtra: "sleep",
    },
    "RepelAssault": {
        name: "RepelAssault",
        displayName: "Repel Assault",
        description: "Bunker down to increase your defenses temporarily",
        targeting: Targets.Self,
        tier: 3,
        cooldown: Duration.Encounter,
        invokerEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.PlusDefense,
            effectOverrides: {
                defense: Chain.AllDefenses,
                amount: 5,
            },
        }],
        techExtra: "+5 all defenses",
    },
    "Bless": {
        name: "Bless",
        description: "Gain +1 to hit and damage",
        targeting: Targets.Self,
        tier: 3,
        cooldown: Duration.Boss,
        invokerEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.PlusToHit,
            conditionOverride: {duration: Duration.Encounter},
        },{
            name: "BestowConditionEffect",
            condition: CONDITIONS.PlusDamage,
            conditionOverride: {duration: Duration.Encounter},
        }],
        // no techExtra because description is on point for once
    },
}
