import {CONDITIONS} from "../effects/condition";
import {IAbilityTemplate} from "./ability";
import {AUTOHIT, Chain, defaults, Duration, Energy, NODAMAGE, NOHIT, Targets} from "../common";
import {MONSTERS} from "../monsters/nonboss";

export const WIZARD_ABILITIES: IAbilityTemplate[] = [
    //region T1
    ...defaults({tier: 1},
    //region Instant
    {
        name: "UnerringBolt",
        displayName: "Unerring Bolt",
        description: "A bolt of magic that always hits",
        targeting: Targets.Ranged,
        range: 10,
        toHit: AUTOHIT,
        damage: 'int',
    },
    {
        name: "Fright",
        description: "Cause enemies to flee the area",
        targeting: Targets.PBAoE,
        radius: 1,
        toHit: "int vs will",
        damage: "int",
        onHitEffects: [
            {name: 'PushbackEffect', amount: 2}
        ],
        techExtra: "knockback 2",
    },
    {
        name: "ZoneOfError",
        displayName: "Zone of Error",
        description: "Enemies in the zone have -2 to hit",
        targeting: Targets.TAoE,
        radius: 1,
        range: 10,
        toHit: NOHIT,
        invokerEffects: [
            {
                name: "ZoneEffect",
                radius: 1,
                zonePassives: [
                    {
                        name: "ToHitModifierEffect",
                        amount: -2,
                    }
                ]
            }
        ],
        techExtra: "zone: -2 tohit"
    },
    {
        name: "ArcaneBolt",
        displayName: "Arcane Bolt",
        description: "A bolt of arcane energy",
        targeting: Targets.Ranged,
        range: 10,
        toHit: "int vs ref",
        damage: "1d8+int",
    },
    //endregion
    //region Encounter
    {
        name: "Strobe",
        description: "Daze enemies with a magical flashbang",
        cooldown: Duration.Encounter,
        targeting: Targets.TAoE,
        radius: 1,
        range: 10,
        toHit: "int vs will",
        damage: "1d8+int",
        onHitEffects: [
            {name: 'BestowConditionEffect', condition: CONDITIONS["Dazed"]}
        ],
        techExtra: "daze",
    },
    {
        name: "ArcaneBlast",
        displayName: "Arcane Blast",
        description: "A blast of pure arcane energy",
        cooldown: Duration.Encounter,
        targeting: Targets.TAoE,
        radius: 1,
        range: 2,
        toHit: "int vs will",
        damage: "2d6+int",
    },
    {
        name: "ProximityPunishment",
        displayName: "Proximity Punishment",
        description: "Knocks everything away from you, forcefully",
        cooldown: Duration.Encounter,
        targeting: Targets.PBAoE,
        radius: 2,
        toHit: "int vs will",
        damage: "1d10+int",
        onHitEffects: [{
            name: "PushbackEffect",
            amount: 2,
        }],
        techExtra: "knockback 2",
    },
    {
        name: "EchoingBlast",
        displayName: "Echoing Blast",
        description: "Damages your target and then everyone around them",
        cooldown: Duration.Encounter,
        targeting: Targets.Ranged,
        range: 10,
        toHit: "int vs will",
        damage: "1d8+int",
        onHitEffects: [
            {
                name: "BurstDamageEffect",
                radius: 1,
                damage: "1d10",
                includeOrigin: false,
            }
        ],
        techExtra: "1d10 damage to adjacent",
    },
    //endregion Encounter

    //region Boss
    {
        name: "EntropicBolt",
        displayName: "Entropic Bolt",
        description: "A bolt of pure entropy that damages and weakens your target",
        cooldown: Duration.Boss,
        targeting: Targets.Ranged,
        range: 10,
        toHit: "int vs ref",
        damage: "2d8+int",
        onHitEffects: [
            {
                name: "BestowConditionEffect",
                condition: CONDITIONS.Weakened,
            }
        ],
        techExtra: "weaken",
    },
    {
        name: "Sleep",
        description: "Put enemies in an area to sleep",
        cooldown: Duration.Boss,
        targeting: Targets.TAoE,
        range: 10,
        radius: 1,
        toHit: "int vs will",
        damage: NODAMAGE,
        onHitEffects: [
            {
                name: "BestowConditionEffect",
                condition: CONDITIONS.Asleep,
            }
        ],
        onMissEffects: [
            {
                name: "EnergyDamageEffect",
                amount: Energy.STANDARD,
            }
        ],
        techExtra: "sleep",
    },
    //endregion Boss
    //endregion
    ),
    //region T2
    ...defaults({tier: 2},
    //region instant
    //Beam of Impedance - 1d6+INT damage, slow
    {
        name: "BeamOfImpedance",
        displayName: "Beam of Impedance",
        description: "A beam of slow-time that damages and slows",
        targeting: Targets.Ranged,
        range: 10,
        toHit: "int vs will",
        damage: "1d6+int",
        onHitEffects: [
            {name: "BestowConditionEffect", condition: CONDITIONS.Slow}
        ],
        techExtra: "slow",
    },
    // Twin Bolt - 1d6+INT to target and one other random target
    {
        name: "TwinBolt",
        displayName: "Twin Bolt",
        description: "A bolt of energy hits one target, then another",
        targeting: Targets.Ranged,
        range: 10,
        toHit: "int vs will",
        damage: "1d6+int",
        onHitEffects: [
            {
                name: "BurstDamageEffect",
                radius: 10,
                damage: "1d6+int",
                numTargets: 1,
                includeOrigin: false,
            }
        ]
    },
    // Zone of Torpor - Ranged burst 1, Zone: enemies in zone are slowed
    {
        name: "ZoneOfTorpor",
        displayName: "Zone of Torpor",
        description: "A zone that slows enemies within it",
        targeting: Targets.TAoE,
        range: 10,
        radius: 1,
        toHit: NOHIT,
        invokerEffects: [{
            name: "ZoneEffect",
            radius: 1,
            zonePassives: [{
                name: "ExpendEnergyEffect",
                amount: 1,
                amountIsMultiplier: true,
                energyCategory: Energy.MOVE,
            }],
        }],
        techExtra: "zone: slow",
    },
    //endregion instant

    //region encounter
    {
        name: "ArcaneBrain",
        displayName: "Arcane Brain",
        description: "Shoot magic directly at someone's brain",
        targeting: Targets.Ranged,
        range: 10,
        toHit: "int vs will",
        damage: "2d6+int",
        cooldown: Duration.Encounter,
    },
    {
        name: "Contagion",
        description: "If an enemy dies while affected by this, they will " +
            "spread all their conditions to other nearby enemies",
        targeting: Targets.Ranged,
        range: 10,
        toHit: "int vs fort",
        damage: "1d8+int",
        cooldown: Duration.Encounter,
        onHitEffects: [
            {
                name: "BestowConditionEffect",
                condition: CONDITIONS.Contagious,
                bestowerImmune: true,
            },
            {
                name: "BestowConditionEffect",
                condition: CONDITIONS.Ongoing,
                effectOverrides: {amount: 5}
            }
        ],
        techExtra: "ongoing 5, contagious",
    },
    // Source of Pain - Ranged, autohit: Creature gains aura: deal 1d10 damage to its adjacent allies. SE.
    {
        name: "SourceOfPain",
        displayName: "Source of Pain",
        description: "Put an aura on an enemy to do damage to those nearby",
        targeting: Targets.Ranged,
        range: 10,
        toHit: "int vs will",
        damage: NODAMAGE,
        cooldown: Duration.Encounter,
        onHitEffects: [
            {
                name: "BestowConditionEffect",
                condition: CONDITIONS.PainAura,
                bestowerImmune: true,
            }
        ],
        techExtra: "enemy aura 1: 1d10 damage",
    },
    //endregion encounter

    //region boss
    {
        name: "SummonImp",
        displayName: "Summon Imp",
        description: "Summon a friendly but uncontrollable imp",
        targeting: Targets.None,
        toHit: NOHIT,
        cooldown: Duration.Boss,
        invokerEffects: [{
            name: "SpawnCreatureEffect",
            creatures: [MONSTERS.Imp],
            radius: 2,
        }],
    },
    {
        name: "Haste",
        description: "Do everything twice as fast",
        targeting: Targets.Self,
        cooldown: Duration.Boss,
        invokerEffects: [{
            name: "BestowConditionEffect",
            condition: CONDITIONS.Hasted,
        }],
    },
    //endregion boss
    ), //endregion T2
    // region T3
    ...defaults({tier: 3},
        // region Instant
        {
            // Arcane Link - 1d10+INT damage, -100 will
            name: "ArcaneLink",
            displayName: "Arcane Link",
            description: "An arcane trick to erode your enemy's will",
            targeting: Targets.Ranged,
            range: 10,
            toHit: "int vs will",
            damage: "1d10+int",
            onHitEffects: [{
                name: "BestowConditionEffect",
                condition: CONDITIONS.MinusDefense,
                effectOverrides: {
                    amount: -100,
                    defense: Chain.will,
                }
            }],
            techExtra: "-100 will",
        },
        {
            name: "ZoneOfDisfavor",
            displayName: "Zone of Disfavor",
            description: "Damage and create a zone disadvantaging enemies",
            targeting: Targets.TAoE,
            range: 10,
            radius: 1,
            toHit: "int vs will",
            damage: "int",
            invokerEffects: [{
                name: "ZoneEffect",
                radius: 1,
                zonePassives: [{
                    name: "AdvantageEffect",
                    amount: -1,
                }]
            }],
            techExtra: "zone: disadvantage",
        },
        // endregion Instant
        // region Encounter
        {
            name: "Hypnosis",
            description: "Put nearby enemies to sleep",
            cooldown: Duration.Encounter,
            targeting: Targets.PBAoE,
            radius: 1,
            toHit: "int vs will",
            damage: NODAMAGE,
            onHitEffects: [{
                name: "BestowConditionEffect",
                condition: CONDITIONS.Asleep,
            }],
            techExtra: "sleep",
        },
        {
            name: "Misfortune",
            description: "Inflict a wide variety of conditions on your enemy",
            cooldown: Duration.Encounter,
            targeting: Targets.Ranged,
            range: 10,
            toHit: "int vs will",
            damage: NODAMAGE,
            onHitEffects: [{
                name: "BestowConditionEffect",
                condition: CONDITIONS.Slow,
            },{
                name: "BestowConditionEffect",
                condition: CONDITIONS.Disadvantaged,
            },{
                name: "BestowConditionEffect",
                condition: CONDITIONS.Debilitated,
            },{
                name: "BestowConditionEffect",
                condition: CONDITIONS.Dazed,
            },{
                name: "BestowConditionEffect",
                condition: CONDITIONS.Weakened,
            },{
                name: "BestowConditionEffect",
                condition: CONDITIONS.Ongoing,
                effectOverrides: {amount: 3}
            }],
            techExtra: "slow, disadvantage, -5 tohit, -5 damage, -5 defense, daze, weak, ongoing 3",
        },
        //endregion Encounter
        //region Boss
        {
            name: "Begone",
            description: "Teleport your enemy elsewhere",
            cooldown: Duration.Boss,
            targeting: Targets.Ranged,
            range: 10,
            toHit: "int vs will",
            damage: NODAMAGE,
            onHitEffects: [{
                name: "TeleportOtherEffect",
            }]
        },
        //endregion
    ),
    // endregion T3
];
