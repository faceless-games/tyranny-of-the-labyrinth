import {RegistryMappable, TemplateMap} from "./interfaces";

interface SubclassConstructor<T> {
    new(template?: any): T;
}

interface TemplateHolder<T> {
    name: string;
    template: RegistryMappable;
    constructor: SubclassConstructor<T>;
}

export class Registry<T> {

    private storage: {[id: string]: SubclassConstructor<T>}

    private templates: {[id: string]: TemplateHolder<T>}

    constructor() {
        this.storage = {};
        this.templates = {};
    }

    public get all(): TemplateHolder<T>[] {
        return Object.values(this.templates);
    }

    /* Output */
    newFromName(name: string, template?: any): T {
        let constructor = this.storage[name];

        if (constructor !== undefined) {
            return new constructor(template);
        }

        let th = this.templateHolderFromName(name);
        return new th.constructor(th.template);
    }

    templateHolderFromName(name: string): TemplateHolder<T> {
        let storedTemplate = this.templates[name];
        if(storedTemplate === undefined) {
            throw(`Registry could not look up ${name}!`);
        }
        return storedTemplate;
    }

    mapped(templates: RegistryMappable[]): T[] {
        if(!templates) return [];
        return templates.map((t) => this.newFromName(t.name, t));
    }

    registryDecorator() {
        return (constructor: SubclassConstructor<T>) => {
            this.storage[constructor.name] = constructor;
        }
    }

    adoptFromTemplates(constructor: SubclassConstructor<T>, templates: RegistryMappable[]) {
        for(let template of templates) {
            this.templates[template.name] = {
                name: template.name,
                template,
                constructor
            }
        }
    }

    adoptFromTemplateMap(constructor: SubclassConstructor<T>, templates: TemplateMap) {
        let lst = [];
        for(let template of Object.values(templates)) {
            lst.push(template);
        }
        this.adoptFromTemplates(constructor, lst);
    }
}