
import {Character} from "../../character";
import {GameMaster} from "../../gamemaster";
import {StandThereBehavior} from "../../behavior";
import {PlayerController} from "../../control";
import {Dice} from "../../dice";
import {Chain, Duration, Point} from "../../common";
import {TestGameFactory} from "../testutils";
import {Ability} from "../../abilities/ability";

let tgf: TestGameFactory;
let gm: GameMaster;
let pc: PlayerController;
let player: Character;
let npc: Character;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player, gm} = tgf.testGameData());
    pc = new PlayerController(player, gm);
    npc = tgf.testMonster();
})

describe("T1", () => {
    describe("Instant", () => {
        test("Shield Bash", () => {
            let bash = player.gainAbility("ShieldBash");
            expect(bash.tier).toStrictEqual(1);
            player.assignDefaultMeleeAbility(bash);

            gm.fixRolls(19, 1);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();

            expect(npc.hp).toStrictEqual(9999);
            expect(npc.hasConditionNamed('Dazed')).toBeTruthy();
        });

        test("Blood Strike", () => {
            let bs = player.gainAbility("BloodStrike");
            player.assignDefaultMeleeAbility(bs);
            gm.fixRolls(19, 1);
            pc.bump({x: 1, y: 1});

            expect(npc.hp).toStrictEqual(9999);
            let ongoing = npc.hasConditionNamed("Ongoing");
            expect(ongoing).toBeTruthy();
            expect(ongoing.beginningOfTurnEffects[0]?.amount).toStrictEqual(1);
        });

        test("Cleave", () => {
            let npc2 = tgf.testMonster({
                loc: {x: 1,  y: 2},
                baseMaxHp: 5000,
            });

            player.stats.str = 14;
            let cleave = player.gainAbility("Cleave");
            player.assignDefaultMeleeAbility(cleave);

            gm.fixRolls(19, 1);
            pc.bump({x: 1, y: 1});

            expect(npc.hp).toStrictEqual(9997);
            expect(npc2.hp).toStrictEqual(4998);
        });

        test("Shield Push", () => {
            let push = player.gainAbility("ShieldPush");
            player.assignDefaultMeleeAbility(push);

            let originalLoc = Point.fromLoc(npc.loc);
            expect(gm.map.tileAt(npc.loc).character).toBe(npc);

            gm.fixRolls(19, 1);
            pc.bump({x: 1, y: 1});

            // First, the NPC should have been bumped in that direction
            let expectedLoc = {x: originalLoc.x + 1, y: originalLoc.y + 1};
            expect(expectedLoc).toEqual(npc.loc);
            expect(gm.map.tileAt(expectedLoc).character).toBe(npc);

            // Secondly, the player should be where the npc originally was
            expect(gm.map.tileAt(originalLoc).character).toBe(player);

            // Oh and damage or something I guess
            expect(npc.hp).toStrictEqual(9999);
        });
    });
    describe("Encounter", () => {

        test("Get Over Here", () => {
            let goh = player.gainAbility("GetOverHere");
            expect(goh.currentCooldown).toStrictEqual(0);

            gm.forceMoveCharacter(npc, {x: 3, y: 3});
            let originalLoc = Point.fromLoc(npc.loc);

            gm.fixRolls(19, 1);
            expect(pc.attack(goh, npc.loc)).toBeTruthy();

            let expectedLoc = {x: originalLoc.x - 1, y: originalLoc.y - 1};
            expect(expectedLoc).toEqual(npc.loc);
            expect(gm.map.tileAt(expectedLoc).character).toBe(npc);

            expect(npc.hp).toStrictEqual(9999);
            expect(goh.currentCooldown).toStrictEqual(Duration.Encounter);
            expect(goh.isReady()).toBeFalsy();

            // Can't use an ability while it's on cooldown
            tgf.nextTurn();
            expect(player.isReady()).toBeTruthy();
            expect(pc.attack(goh, npc.loc)).toBeFalsy();

            // cool down has cooled down, at least a little
            let expectedCooldown = Duration.Encounter - 1;
            expect(goh.currentCooldown).toStrictEqual(expectedCooldown);
        });
        test("Charge", () => {
            let charge = player.gainAbility("Charge");
            expect(charge.currentCooldown).toStrictEqual(0);
            gm.forceMoveCharacter(npc, {x: 3, y: 3});
            let originalLoc = Point.fromLoc(player.loc);

            gm.fixRolls(19, 1);
            expect(pc.attack(charge, npc.loc)).toBeTruthy();

            let expectedLoc = {x: originalLoc.x + 1, y: originalLoc.y + 1};
            expect(expectedLoc).toEqual(player.loc);
            expect(npc.hp).toStrictEqual(9999);
            expect(charge.currentCooldown).toStrictEqual(Duration.Encounter);
        });
        test("Deep Strike", () => {
            let ability = player.gainAbility("DeepStrike");
            expect(ability.currentCooldown).toStrictEqual(0);
            player.assignDefaultMeleeAbility(ability);

            gm.fixRolls(19, 1, 1);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();

            expect(npc.hp).toStrictEqual(9998);
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
            let ongoing = npc.hasConditionNamed("Ongoing");
            expect(ongoing).toBeTruthy();
            expect(ongoing.beginningOfTurnEffects[0]?.amount).toStrictEqual(3);
        });
        test("Lash Out", () => {
            let npc2 = tgf.testMonster({
                loc: {x: 1,  y: 2},  // Next to player and npc
                baseMaxHp: 5000,
            });
            let npc3 = tgf.testMonster({
                loc: {x: 0, y: 0}, // Next to player
                baseMaxHp: 4000,
            });
            gm.map.tileAt(npc3.loc).passable = true; // otherwise can't target
            let npc4 = tgf.testMonster({
                loc: {x: 2, y: 3}, // Next to npc but not player
                baseMaxHp: 2000,
            });

            let ability = player.gainAbility("LashOut");
            player.assignDefaultMeleeAbility(ability);

            gm.fixRolls(
                19, 19, 19, // npc, npc2, npc3
                1, 1, 1);
            expect(pc.attack(ability)).toBeTruthy();

            expect(npc.hp).toStrictEqual(9999);
            expect(npc2.hp).toStrictEqual(4999);
            expect(npc3.hp).toStrictEqual(3999);
            expect(npc4.hp).toStrictEqual(2000);
        });
    });

    describe("Boss", () => {
        test("Beatdown", () => {
            let ability = player.gainAbility("Beatdown");
            expect(ability.currentCooldown).toStrictEqual(0);
            player.assignDefaultMeleeAbility(ability);

            gm.fixRolls(19, 1, 1, 1);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();

            expect(npc.hp).toStrictEqual(9997);
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);

            let weakened = npc.hasConditionNamed("Weakened");
            expect(weakened).toBeTruthy();

            let prone = npc.hasConditionNamed("Prone");
            expect(prone).toBeTruthy();

            // Can't use ability on boss cooldown either
            tgf.nextTurn();
            expect(player.isReady()).toBeTruthy();
            expect(pc.attack(ability, npc.loc)).toBeFalsy();

            tgf.nextTurn();
            // Boss cooldowns don't cool down
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });

        test("Maiming Strike", () => {
            let ability = player.gainAbility("MaimingStrike");
            expect(ability.currentCooldown).toStrictEqual(0);
            player.assignDefaultMeleeAbility(ability);

            gm.fixRolls(19, 1, 1, 1);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();

            expect(npc.hp).toStrictEqual(9997);
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
            let ongoing = npc.hasConditionNamed("Ongoing");
            expect(ongoing).toBeTruthy();
            expect(ongoing.beginningOfTurnEffects[0]?.amount).toStrictEqual(5);
        });
    });
});
describe("T2", () => {
    describe("Instant", () => {
        test("Careful Strike", () => {
            player.stats.str = 12; // +1
            let ability = player.gainAbility("CarefulStrike");
            expect(ability.tier).toStrictEqual(2);
            player.assignDefaultMeleeAbility(ability);

            gm.fixRolls(7, 1); //normally miss, but +1 +2 = hit!
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();

            expect(npc.hp).toStrictEqual(9999); // only does dice damage
        });

        test("Snaring Strike", () => {
            let ability = player.gainAbility("SnaringStrike");
            player.assignDefaultMeleeAbility(ability);

            gm.fixRolls(19, 1);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();

            expect(npc.hp).toStrictEqual(9999);
            expect(npc.hasConditionNamed("Slow")).toBeTruthy();
        });

        test("Power Attack", () => {
            let ability = player.gainAbility("PowerAttack");
            player.assignDefaultMeleeAbility(ability);
            player.stats.str = 12; // +1

            gm.fixRolls(10);  // would ordinarily hit, but 10+1-2 = miss!
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(10000);

            tgf.nextTurn();
            gm.fixRolls(11, 1);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(9996); // 1, +1 (str), +2 (power)
        });
    });

    describe("Encounter", () => {
        test("Resurgent Strike", () => {
            let ability = player.gainAbility("ResurgentStrike");
            player.assignDefaultMeleeAbility(ability);
            expect(ability.currentCooldown).toStrictEqual(0);

            let expectedHealing = Math.floor(player.maxHp * 0.1);
            expect(expectedHealing).toBeGreaterThan(0);
            player.hp = 1;
            gm.fixRolls(19, 1);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(9999);
            expect(player.hp).toStrictEqual(1 + expectedHealing);

            // Also heals on a miss
            player.hp = 1;
            gm.fixRolls(1);
            tgf.nextTurn();
            ability.resetCooldown();
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(9999);
            expect(player.hp).toStrictEqual(1 + expectedHealing);
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        // https://gitlab.com/rostrander/tyranny-of-the-labyrinth/-/issues/50
        test("ResurgentStrike heal after death", () => {
            let ability = player.gainAbility("ResurgentStrike");
            player.assignDefaultMeleeAbility(ability);

            let expectedHealing = Math.floor(player.maxHp * 0.1);
            expect(expectedHealing).toBeGreaterThan(0);
            player.hp = 1;
            npc.hp = 1;
            gm.fixRolls(19, 1);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.alive).toBeFalsy();
            expect(player.hp).toStrictEqual(1 + expectedHealing);
        });

        test("Bowl Over", () => {
            let npc2 = tgf.testMonster({
                loc: {x: 1, y: 2},  // Next to player and npc
                baseMaxHp: 5000,
            });
            let npc3 = tgf.testMonster({
                loc: {x: 0, y: 0}, // Next to player
                baseMaxHp: 4000,
            });
            gm.map.tileAt(npc3.loc).passable = true; // otherwise can't target
            let npc4 = tgf.testMonster({
                loc: {x: 2, y: 3}, // Next to npc but not player
                baseMaxHp: 2000,
            });
            let ability = player.gainAbility("BowlOver");
            player.assignDefaultMeleeAbility(ability);
            player.stats.str = 12; // +1

            gm.fixRolls(
                1, // Missing npc3
                19, // npc2
                19,  // npc
            );
            expect(pc.attack(ability)).toBeTruthy();

            expect(npc.hp).toStrictEqual(9999);
            expect(npc.hasConditionNamed("Prone")).toBeTruthy();
            expect(npc2.hp).toStrictEqual(4999);
            expect(npc2.hasConditionNamed("Prone")).toBeTruthy();
            expect(npc3.hp).toStrictEqual(4000);
            expect(npc3.hasConditionNamed("Prone")).toBeFalsy();
            expect(npc4.hp).toStrictEqual(2000);
            expect(npc4.hasConditionNamed("Prone")).toBeFalsy();
        });

        test("Not So Fast", () => {
            let ability = player.gainAbility("NotSoFast");
            expect(ability.currentCooldown).toStrictEqual(0);

            gm.forceMoveCharacter(npc, {x: 3, y: 3});

            player.stats.str = 12; // +1, though we're ignoring it
            gm.fixRolls(19, 1, 1);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9998);
            expect(npc.hasConditionNamed("Immobilized")).toBeTruthy();
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

    });
    describe("Boss", () => {
        test("Put Your Back Into It", () => {
            let ability = player.gainAbility("PutYourBackIntoIt");
            expect(ability.currentCooldown).toStrictEqual(0);

            expect(pc.selfAbility(ability)).toBeTruthy();
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
            let dice = player.hasConditionNamed("ExtraDamageRoll");
            expect(dice).toBeTruthy();
            expect(dice.duration).toStrictEqual(Duration.Encounter - 1);
        });

        test("Comparative Armor", () => {
            let ability = player.gainAbility("ComparativeArmor");
            expect(ability.currentCooldown).toStrictEqual(0);
            player.defaultMeleeAbility = ability;
            expect(player.calcDefense(Chain.ac).value).toStrictEqual(10);
            expect(player.calcDefense(Chain.fort).value).toStrictEqual(10);
            expect(player.calcDefense(Chain.ref).value).toStrictEqual(10);
            expect(player.calcDefense(Chain.will).value).toStrictEqual(10);

            gm.fixRolls(19, 1, 1, 1);
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            expect(npc.hp).toStrictEqual(9997);

            expect(npc.hasConditionNamed("Impaired"));

            // Ability-specific condition
            expect(player.hasConditionNamed("ComparativeArmorBuff")).toBeTruthy();
            // +2 to all defenses
            expect(player.calcDefense(Chain.ac).value).toStrictEqual(12);
            expect(player.calcDefense(Chain.fort).value).toStrictEqual(12);
            expect(player.calcDefense(Chain.ref).value).toStrictEqual(12);
            expect(player.calcDefense(Chain.will).value).toStrictEqual(12);

            tgf.nextTurn();
            player.defaultMeleeAbility.resetCooldown();  // Otherwise we can't use it
            // +2 tohit, damage
            gm.fixRolls(6, 1, 1, 1);  // Normally miss, but +2 tohit, -2 enemy AC
            expect(pc.bump({x: 1, y: 1})).toBeTruthy();
            // Likewise +2 damage
            expect(npc.hp).toStrictEqual(9997-3-2);
        });
    });
});

describe("T3", () => {
    let ability: Ability;
    afterEach(() => {
        expect(ability.tier).toStrictEqual(3);
    });

    describe("Instant", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toBeLessThanOrEqual(1);
        });

        test("Terrifying Strike", () => {
            // Terrifying Strike - 1W+STR damage, enemy pushed 2
            ability = player.gainAbility("TerrifyingStrike");
            player.stats.str = 14; // +2
            gm.fixRolls(19, 2);
            let oldLoc = {x: npc.loc.x, y: npc.loc.y };
            expect(npc.loc).toEqual(oldLoc);

            expect(pc.attack(ability, oldLoc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9996); // rolled 2, +2 stat

            expect(npc.loc).not.toEqual(oldLoc);
        });

        test("Come At Me", () => {
            let ac = player.calcDefense(Chain.ac).value;
            ability = player.gainAbility("ComeAtMe");
            player.stats.str = 12; // +1
            gm.fixRolls(19, 5);

            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9994); // rolled 5, +1 stat
            expect(player.calcDefense(Chain.ac).value).toStrictEqual(ac+2);
            expect(player.hasConditionNamed("PlusDefense")).toBeTruthy();
        });
    }); // Instant

    describe("Encounter", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        test("Untouchable", () => {
            ability = player.gainAbility("Untouchable");
            player.stats.str = 16; // +3
            gm.fixRolls(19, 2, 2);

            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9993); // Rolled 4, +3 stat

            expect(player.hasConditionNamed("Resistant")).toBeTruthy();
        });

        test("I Am Not Messing Around", () => {
            ability = player.gainAbility("IAmNotMessingAround");
            player.stats.str = 9; // -1
            gm.fixRolls(19, 1, 2, 3);

            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9995); // Rolled 6, -1 stat
        });
    });

    describe("Boss", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });

        test("Second Wind", () => {
            player.hp = 1;
            let half = player.maxHp / 2;
            ability = player.gainAbility("SecondWind");

            expect(pc.selfAbility(ability)).toBeTruthy();
            expect(player.hp).toStrictEqual(half + 1);
        });
    })
});