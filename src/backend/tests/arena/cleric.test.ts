import {TestGameFactory} from "../testutils";
import {GameMaster} from "../../gamemaster";
import {PlayerController} from "../../control";
import {Character} from "../../character";
import {Chain, Duration} from "../../common";
import {Dice} from "../../dice";
import {Ability} from "../../abilities/ability";

let tgf: TestGameFactory;
let gm: GameMaster;
let pc: PlayerController;
let player: Character;
let npc: Character;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player, gm} = tgf.testGameData());
    pc = new PlayerController(player, gm);
    npc = tgf.testMonster();
})

describe("T1", () => {
    describe("Instant", () => {
        test("Holy Lance", () => {
            let ability = player.gainAbility("HolyLance");
            player.stats.wis = 14; // +2
            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9996);
            expect(player.hasConditionNamed("PlusToHit")).toBeTruthy();
        });

        test("Radiant Bulwark", () => {
            let ac = player.calcDefense(Chain.ac).value;
            let ability = player.gainAbility("RadiantBulwark");
            player.stats.wis = 9; // -1
            player.stats.cha = 16; // +3
            gm.fixRolls(19, 4);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9997);
            expect(player.calcDefense(Chain.ac).value).toStrictEqual(ac+3);
            expect(player.hasConditionNamed("PlusDefense")).toBeTruthy();
        });

        test("Shielding Siege", () => {
            let ability = player.gainAbility("ShieldingSiege")
            player.stats.str = 12; // +1
            player.stats.cha = 15; // +2
            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9997);
            expect(player.tempHp).toStrictEqual(2);
        })

        test("Redoubling Siege", () => {
            let ability = player.gainAbility("RedoublingSiege");
            player.stats.str = 12; // +1
            player.stats.con = 14; // +2
            let hit = player.addToHitBonuses(Dice.zero, player).value;

            gm.fixRolls(19, 4);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9995);
            let plus =  player.hasConditionNamed("PlusToHit")
            expect(plus).toBeTruthy();
            let newHit = player.addToHitBonuses(Dice.zero, player).value;
            expect(newHit - hit).toStrictEqual(2);
        });
    }); // Instant
    describe("Encounter", () => {
        test("Induce Fear", () => {
            let ability = player.gainAbility("InduceFear");

            let origLoc = {x: npc.loc.x, y: npc.loc.y};
            gm.fixRolls(19);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            expect(npc.hp).toStrictEqual(10000);  // no damage
            expect(npc.loc).not.toEqual(origLoc);
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        test("Divine Blast", () => {
            let ability = player.gainAbility("DivineBlast");
            player.stats.wis = 12; // +1
            // need another NPC
            let npc2 = tgf.testMonster({
                loc: {x: npc.loc.x, y: npc.loc.y + 1},
                baseMaxHp: 5000,
                name: 'npc2',
            });

            gm.fixRolls(19, 19, 2, 2);
            expect(pc.attack(ability)).toBeTruthy();

            expect(npc2.hp).toStrictEqual(4997);
            expect(npc.hp).toStrictEqual(9997);


            expect(player.hasConditionNamed("PlusToHit")).toBeTruthy();
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        test("Dazing Siege", () => {
            let ability = player.gainAbility("DazingSiege");
            player.stats.str = 14; // +2

            gm.fixRolls(19, 4);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            expect(npc.hp).toStrictEqual(9994);
            expect(npc.hasConditionNamed("Dazed")).toBeTruthy();
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        test("Recuperating Siege", () => {
            let ability = player.gainAbility("RecuperatingSiege");
            player.baseMaxHp = 20;
            player.hp = 1;
            // Recuperating Siege: 1W+str, Heal for (something? 15% puts it higher than fighter)
            player.stats.str = 9; // -1
            gm.fixRolls(19, 6);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            expect(npc.hp).toStrictEqual(9995);
            expect(player.hp).toStrictEqual(4); // 20*0.15 + 1 = 4
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        })
    }); // Encounter
    describe("Boss", () => {
        test("Divine Rebuke", () => {
            // Divine Rebuke: Ranged, wis vs will, 2d8 + wis, target gets vuln 5 (save ends)
            let ability = player.gainAbility("DivineRebuke");
            player.stats.wis = 18; // +4
            gm.fixRolls(19, 6);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            let cond = npc.hasConditionNamed('Vulnerable');
            expect(cond).toBeTruthy();
            expect(cond.passiveEffects[0].amount).toStrictEqual(5);
            expect(npc.hp).toStrictEqual(9985);  // 6+4 + vuln 5
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });

        test("Fiery Siege", () => {
            // Fiery Siege: 2W+str, ongoing 5 SE and -1 on saves
            let ability = player.gainAbility("FierySiege");
            player.stats.str = 12; // +1
            gm.fixRolls(19, 2, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            let cond = npc.hasConditionNamed("Ongoing");
            expect(cond).toBeTruthy();
            expect(cond.beginningOfTurnEffects[0].amount).toStrictEqual(5);

            expect(npc.hasConditionNamed("Saveless")).toBeTruthy();

            expect(npc.hp).toStrictEqual(9995);
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        })
    }); // Boss
}); // T1

describe("T2", () => {
    describe("Instant", () => {
        test("Light of Judgement", () => {
            let fort = npc.calcDefense(Chain.fort, player).value;
            let ability = player.gainAbility("LightOfJudgement");
            let npcTile = gm.map.tileAt(npc.loc);
            expect(npcTile.zone).toBeFalsy();

            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npcTile.zone).toBeTruthy();
            // Just 1x1 zone so check the player's not in it.
            expect(gm.map.tileAt(player.loc).zone).toBeFalsy();

            expect(npc.calcDefense(Chain.fort, player).value).toStrictEqual(fort-1);
        });

        test("Holy Mark", () => {
            let ability = player.gainAbility("HolyMark");
            expect(ability.tier).toStrictEqual(2);
            player.stats.wis = 9; // -1
            gm.fixRolls(19, 3);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            expect(npc.hasConditionNamed("Vulnerable")).toBeTruthy();
            expect(npc.hp).toStrictEqual(9997); // rolled 3, -1, +1 due to vuln
        });

        test("Scourging Siege", () => {
            // Scourging Siege: 1W+str, +1 damage EoNT
            let ability = player.gainAbility("ScourgingSiege");
            expect(ability.tier).toStrictEqual(2);
            player.stats.str = 19; // +4
            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            expect(player.hasConditionNamed("PlusDamage")).toBeTruthy();
            expect(npc.hp).toStrictEqual(9994);
        });
    }); // Instant
    describe("Encounter", () => {
        let ability: Ability;
        afterEach(() => {
            expect(ability.tier).toStrictEqual(2);
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        })
        test("Heal Wounds", () => {
            player.hp = 1;
            ability = player.gainAbility("HealWounds");
            expect(pc.selfAbility(ability)).toBeTruthy();
            expect(player.hp).toBeGreaterThan(1);
        });

        test("Expose Corruption", () => {
            ability = player.gainAbility("ExposeCorruption");
            player.stats.wis = 13; // +1
            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hasConditionNamed("Vulnerable")).toBeTruthy();
            expect(npc.hp).toStrictEqual(9994); // rolled 2, +1 stat, +3 vuln
        });

        test("Rebuking Siege", () => {
            //Rebuking Siege: 2W+str, knockback 2, prone
            let originalNpcLoc = {x: npc.loc.x, y: npc.loc.y};
            ability = player.gainAbility("RebukingSiege");
            player.stats.str = 9; // -1
            gm.fixRolls(19, 3, 1);
            expect(pc.attack(ability,  npc.loc)).toBeTruthy();
            expect(npc.loc).not.toEqual(originalNpcLoc);
            expect(npc.hasConditionNamed("Prone")).toBeTruthy();
            expect(npc.hp).toStrictEqual(9997); // rolled 4, -1 stat
        })
    }); // Encounter
    describe("Boss", () => {
        let ability: Ability;
        afterEach(() => {
            expect(ability.tier).toStrictEqual(2);
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });

        test("Enforced Pacifism", () => {
            // Enforced Pacifism: Wis vs Will, enemy cannot attack SE
            ability = player.gainAbility("EnforcedPacifism");
            player.stats.wis = 8; // -1
            gm.fixRolls(19, 8);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hasConditionNamed("Asleep")).toBeTruthy();
            expect(npc.hp).toStrictEqual(10000); // no damage
        });

        test("Holy Weapon", () => {
            let ac = npc.calcDefense(Chain.ac).value;
            ability = player.gainAbility("HolyWeapon");
            expect(pc.selfAbility(ability)).toBeTruthy();
            tgf.nextTurn();
            gm.fixRolls(19);
            expect(pc.attack(player.defaultMeleeAbility, npc.loc)).toBeTruthy();
            expect(npc.calcDefense(Chain.ac).value).toStrictEqual(ac-2);
        });

    }); // Boss

}); // T2

describe('T3', function () {
    let ability: Ability;
    afterEach(() => {
        expect(ability.tier).toStrictEqual(3);
    });

    describe('Instant', function () {
        afterEach(() => {
            expect(ability.currentCooldown).toBeLessThanOrEqual(1);
        });

        test("Rooting Siege", () => {
            // Rooting Siege:  1W+str, immob
            ability = player.gainAbility("RootingSiege");
            player.stats.str = 14; // +2
            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9996); // rolled 2, +2 stat
            expect(npc.hasConditionNamed("Immobilized")).toBeTruthy()
        });

        test("Unbalancing Light", () => {
            // Unbalancing Light: ranged, 1d8+wis, -1 all enemy defenses EoNT
            ability = player.gainAbility("UnbalancingLight");
            player.stats.wis = 12; // +1
            gm.fixRolls(19, 4);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9995); // Rolled 4, +1 stat
            expect(npc.hasConditionNamed("MinusDefense"));
            expect(npc.calcDefense(Chain.ac).value).toStrictEqual(9);
            expect(npc.calcDefense(Chain.will).value).toStrictEqual(9);
        });
    });

    describe("Encounter", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toBeLessThanOrEqual(Duration.Encounter);
            expect(ability.currentCooldown).toBeGreaterThanOrEqual(Duration.Encounter-1);
        });

        test("Knockout Siege", () => {
            // Knockout Siege: 1W+str, enemy sleep
            ability = player.gainAbility("KnockoutSiege");
            player.stats.str = 7; // -2
            gm.fixRolls(19, 6);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9996); // rolled 6, -2 stat
            expect(npc.hasConditionNamed("Asleep")).toBeTruthy();

            // Make sure subsequent damage wakes them up
            tgf.nextTurn();
            gm.fixRolls(19, 6);
            expect(pc.attack(player.defaultMeleeAbility, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9992);
            expect(npc.hasConditionNamed("Asleep")).toBeFalsy();
        });

        test("Repel Assault", () => {
            // Repel Assault: self, +5 all defenses until EoNT
            ability = player.gainAbility("RepelAssault");
            expect(pc.selfAbility(ability)).toBeTruthy();

            expect(player.calcDefense(Chain.ac).value).toStrictEqual(15);
            expect(player.calcDefense(Chain.fort).value).toStrictEqual(15);
            expect(player.calcDefense(Chain.ref).value).toStrictEqual(15);
            expect(player.calcDefense(Chain.will).value).toStrictEqual(15);
        });
    });

    describe("Boss", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });

        test("Bless", () => {
            // Bless: self, mode: +1 tohit and damage
            ability = player.gainAbility("Bless");
            expect(pc.selfAbility(ability)).toBeTruthy();

            let toHit = Dice.zero;
            expect(player.addToHitBonuses(toHit,npc).value).toStrictEqual(1);
            let toDmg = Dice.zero;
            expect(player.addDamageBonuses(toDmg, npc).value).toStrictEqual(1);

            let pth = player.hasConditionNamed("PlusToHit");
            expect(pth).toBeTruthy();
            expect(pth.duration).toStrictEqual(Duration.Encounter-1);

            let pd = player.hasConditionNamed("PlusDamage");
            expect(pd).toBeTruthy();
            expect(pd.duration).toStrictEqual(Duration.Encounter-1);
        });
    });
});