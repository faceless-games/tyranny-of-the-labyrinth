
import {Character} from "../../character";
import {GameMaster} from "../../gamemaster";
import {DefaultRangedBehavior, StandThereBehavior} from "../../behavior";
import {PlayerController} from "../../control";
import {Dice} from "../../dice";
import {Chain, Duration, Energy, Point} from "../../common";
import {Tile} from "../../world";
import {TestGameFactory} from "../testutils";
import {Ability} from "../../abilities/ability";

let tgf: TestGameFactory;
let gm: GameMaster;
let pc: PlayerController;
let player: Character;
let npc: Character;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player, gm} = tgf.testGameData());
    pc = new PlayerController(player, gm);
    npc = tgf.testMonster();
})

describe("T1", () => {
    describe("Instant", () => {
        describe("Unerring bolt", () => {
            test("Happy path", () => {
                player.stats.int = 14; // 2 bonus damage
                let bolt = player.gainAbility("UnerringBolt");
                let origEnergy = player.energy;

                // This attack shouldn't roll anything, but if it does (incorrectly),
                // I want it to miss
                gm.fixRolls(1);

                expect(pc.attack(bolt, npc.loc)).toBeTruthy();
                expect(player.energy).toBeLessThan(origEnergy);
                expect(npc.hp).toStrictEqual(9998);
            });

            test("Negative modifiers don't do negative damage", () => {
                player.stats.int = 7; // -2 bonus damage
                let bolt = player.gainAbility("UnerringBolt");

                expect(pc.attack(bolt, npc.loc)).toBeTruthy();
                expect(npc.hp).toStrictEqual(10000);
            });
        });

        test("Fright", () => {
            // need another npc!
            let npc2 = tgf.testMonster({
                loc: {x: player.loc.x, y: player.loc.y + 1},
                baseMaxHp: 5000,
                name: 'npc2',
            });
            // Put up a wall for them to bump into
            let wallLoc = {x: player.loc.x, y: player.loc.y + 3};
            gm.map.setTile(Tile.rotjsValueToTile(1, wallLoc));
            let npc3 = tgf.testMonster({
                loc: {x: player.loc.x + 1, y: player.loc.y},
                baseMaxHp: 4000,
                name: 'npc3',
            })
            // fourth out of range npc
            let npc4 = tgf.testMonster({
                loc: {x: player.loc.x + 3, y: npc.loc.y + 1},
                baseMaxHp: 2000,
                name: 'npc4',
            })
            const originalnpc4Loc = {x: npc4.loc.x, y: npc4.loc.y};

            let ability = player.gainAbility("Fright");
            player.stats.int = 14; // 2 dmg
            gm.fixRolls(19, 19, 19);
            expect(pc.attack(ability)).toBeTruthy();

            let npcs = [npc, npc2, npc3]; // Purposefully omitting 4
            for(let npc of npcs) {
                expect(npc.hp).toBeLessThan(npc.maxHp);
                expect(npc.maxHp-npc.hp).toBe(player.statBonus('int'));
            }
            expect(npc4.hp).toBe(2000);

            // npc 3 pushed back 2
            expect(npc3.loc.x).toBe(player.loc.x+3);
            expect(npc3.loc.y).toBe(player.loc.y);

            // npc 2 only partially pushed back (into wall
            expect(npc2.loc.x).toBe(player.loc.x);
            expect(npc2.loc.y).toBe(player.loc.y + 2);

            // npc 1 moved diagonally
            expect(npc.loc).toEqual({x: player.loc.x + 3, y: player.loc.y + 3});

            // npc 4 just sitting there
            expect(npc4.loc).toEqual(originalnpc4Loc);
        });

        test("Zone of Error", () => {
            let ability = player.gainAbility("ZoneOfError");
            let npcTile = gm.map.tileAt(npc.loc);
            let pcTile = gm.map.tileAt(player.loc);
            expect(npcTile.zone).toBeFalsy();
            expect(pcTile.zone).toBeFalsy();

            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npcTile.zone).toBeTruthy();
            expect(pcTile.zone).toBeTruthy();

            // npc should have a -2
            let bonus = npc.addToHitBonuses(Dice.zero, player);
            expect(bonus.value).toStrictEqual(-2);

            // Player unaffected
            bonus = player.addToHitBonuses(Dice.zero, npc);
            expect(bonus.value).toStrictEqual(0);
        });
        
        test("Arcane Bolt", () => {
            let ability = player.gainAbility("ArcaneBolt");
            player.stats.int = 14; // +2

            gm.fixRolls(19, 1);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9997);
        });
    });

    describe("Encounter", () => {
        test("Strobe", () => {
            let ability = player.gainAbility("Strobe");
            expect(ability.currentCooldown).toStrictEqual(0);
            player.stats.int = 14; // +2

            // need another NPC
            let npc2 = tgf.testMonster({
                loc: {x: npc.loc.x, y: npc.loc.y + 1},
                baseMaxHp: 5000,
                name: 'npc2',
            });

            gm.fixRolls(19, 19, 1, 1);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            expect(npc.hp).toStrictEqual(9997);
            expect(npc2.hp).toStrictEqual(4997);
            expect(npc.hasConditionNamed("Dazed")).toBeTruthy();
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        test("Arcane Blast", () => {
            let ability = player.gainAbility("ArcaneBlast");
            expect(ability.currentCooldown).toStrictEqual(0);

            player.stats.int = 14; // +2

            // need another NPC
            let npc2 = tgf.testMonster({
                loc: {x: npc.loc.x, y: npc.loc.y + 1},
                baseMaxHp: 5000,
                name: 'npc2',
            });

            gm.fixRolls(19, 19, 1, 1);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            expect(npc.hp).toStrictEqual(9997);
            expect(npc2.hp).toStrictEqual(4997);
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        test("Proximity Punishment", () => {
            let ability = player.gainAbility("ProximityPunishment");
            expect(ability.currentCooldown).toStrictEqual(0);

            player.stats.int = 14; // +2
            let originalHp = player.hp;

            // need another NPC
            let npc2 = tgf.testMonster({
                loc: {x: npc.loc.x, y: npc.loc.y + 1},
                baseMaxHp: 5000,
                name: 'npc2',
            });
            let originalNpcLoc = {x: npc.loc.x, y: npc.loc.y};
            let originalNpc2Loc = {x: npc2.loc.x, y: npc2.loc.y};

            gm.fixRolls(19, 19, 1, 1);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            expect(npc.loc).not.toEqual(originalNpcLoc);
            expect(npc2.loc).not.toEqual(originalNpc2Loc);

            expect(npc.hp).toStrictEqual(9997);
            expect(npc2.hp).toStrictEqual(4997);
            expect(player.hp).toStrictEqual(originalHp);
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        test("Echoing Blast", () => {
            let ability = player.gainAbility("EchoingBlast");
            expect(ability.currentCooldown).toStrictEqual(0);

            player.stats.int = 14;  // +2
            let originalHp = player.hp;

            // need another NPC
            let npc2 = tgf.testMonster({
                loc: {x: player.loc.x, y: npc.loc.y + 1},
                baseMaxHp: 5000,
                name: 'npc2',
            });
            // Apparently the on-hit effects happen first
            gm.fixRolls(19, 3, 2);
            expect(pc.attack(ability, npc2.loc)).toBeTruthy();

            expect(npc2.hp).toStrictEqual(4996); // 2 damage +2 int
            expect(npc.hp).toStrictEqual(9997); // Just the 3 rolled
            expect(player.hp).toStrictEqual(originalHp);

            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });
    });

    describe("Boss", () => {
        test("Entropic Bolt", () => {
            let ability = player.gainAbility("EntropicBolt");
            expect(ability.currentCooldown).toStrictEqual(0);

            player.stats.int = 14; // +2

            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();

            expect(npc.hp).toStrictEqual(9996); //2 dmg + 2 int
            expect(npc.hasConditionNamed("Weakened")).toBeTruthy();

            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });

        describe("Sleep", () => {
            let ability: Ability;

            beforeEach(() => {
                ability = player.gainAbility("Sleep");
                expect(ability.currentCooldown).toStrictEqual(0);
            });

            test("Hit", () => {
                let origEnergy = npc.energy;
                let origHp = npc.hp;
                gm.fixRolls(19);
                expect(pc.attack(ability, npc.loc)).toBeTruthy();
                expect(npc.hasConditionNamed("Asleep")).toBeTruthy();
                expect(npc.energy).toStrictEqual(origEnergy);
                expect(npc.hp).toStrictEqual(origHp);
            });

            test("Miss", () => {
                let origEnergy = npc.energy;
                gm.fixRolls(2);
                expect(pc.attack(ability, npc.loc)).toBeTruthy();
                expect(npc.hasConditionNamed("Asleep")).toBeFalsy();
                expect(npc.energy).toBeLessThan(origEnergy);
            });

            afterEach(() => {
                expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
            });
        });
    });
});

describe("T2", () => {
    describe("Instant", () => {
        test("Beam Of Impedance", () => {
            let ability = player.gainAbility("BeamOfImpedance");
            expect(ability.tier).toStrictEqual(2);

            gm.fixRolls(19, 5);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9995);
            expect(npc.hasConditionNamed("Slow")).toBeTruthy();
        });

        test("Twin Bolt", () => {
            let ability = player.gainAbility("TwinBolt");

            let npc2 = tgf.testMonster({
                loc: {x: 5,  y: 5},
                baseMaxHp: 5000,
            });

            gm.fixRolls(19, 2, 3);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9997);
            expect(npc2.hp).toStrictEqual(4998);
        });

        test("Zone Of Torpor", () => {
            let ability = player.gainAbility("ZoneOfTorpor");
            let npcTile = gm.map.tileAt(npc.loc);
            expect(npcTile.zone).toBeFalsy();

            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npcTile.zone).toBeTruthy();

            let origEnergy = npc.energy;
            gm.endTurn(npc, Energy.MOVE);
            let nowEnergy = npc.energy;
            expect(nowEnergy).toStrictEqual(origEnergy - Energy.MOVE*2);
        });
    });

    describe("Encounter", () => {
        test("Arcane Brain", () => {
            let ability = player.gainAbility("ArcaneBrain");
            expect(ability.currentCooldown).toStrictEqual(0);

            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9998);
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        })

        test("Contagion", () => {
            let ability = player.gainAbility("Contagion");
            expect(ability.currentCooldown).toStrictEqual(0);
            npc.hp = 1;
            tgf.bestowAndVerifyOn(npc, "Slow");

            let npc2 = tgf.testMonster({
                loc: {x: 1,  y: 2},
                baseMaxHp: 5000,
            });

            gm.fixRolls(19, 2);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.alive).toBeFalsy();
            expect(npc2.alive).toBeTruthy();
            expect(npc2.hasConditionNamed("Contagious")).toBeTruthy();
            expect(npc2.hasConditionNamed("Ongoing")).toBeTruthy();
            expect(npc2.hasConditionNamed("Slow")).toBeTruthy();
            expect(player.hasConditionNamed("Slow")).toBeFalsy();
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        test("Source of Pain", () => {
            let ability = player.gainAbility("SourceOfPain");
            let origHp = player.hp;
            expect(ability.currentCooldown).toStrictEqual(0);
            npc.hp = 1;

            let npc2 = tgf.testMonster({
                loc: {x: 1,  y: 2},
                baseMaxHp: 5000,
            });

            gm.fixRolls(19);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.alive).toBeTruthy();  // no damage for this attack
            expect(npc.hasConditionNamed("PainAura")).toBeTruthy();
            expect(npc2.hasConditionNamed("PainAura")).toBeFalsy();

            expect(player.isReady()).toBeFalsy();
            expect(player.hp).toStrictEqual(origHp);

            gm.fixRolls(6);
            tgf.nextTurn(npc);
            gm.endTurn(npc, Energy.STANDARD);
            expect(npc.hp).toStrictEqual(1);  // doesn't effect host
            expect(npc2.hp).toStrictEqual(4994); // but those nearby
            expect(player.hp).toStrictEqual(origHp);  // player immune

            tgf.nextTurn(npc2);
            gm.endTurn(npc, Energy.STANDARD);
            // Shouldn't have done anything
            expect(npc.hp).toStrictEqual(1);
            expect(npc2.hp).toStrictEqual(4994);
            expect(player.hp).toStrictEqual(origHp);
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });
    });

    describe("Boss", () => {
        test("Summon Imp", () => {
            // Move the npc so imp doesn't spawn next to it and run away
            gm.forceMoveCharacter(npc, {x: 5, y: 5});

            let ability = player.gainAbility("SummonImp");
            expect(ability.currentCooldown).toStrictEqual(0);
            expect(gm.characters).toHaveLength(2);
            let startHp = player.hp

            expect(pc.selfAbility(ability)).toBeTruthy();
            let msgLength = player.messages.length;
            expect(gm.characters).toHaveLength(3);
            let imp = gm.characters[gm.characters.length-1];
            expect(imp.name).toStrictEqual("Imp");
            expect(imp.faction).toStrictEqual(player.faction);
            expect(imp.behavior).toBeInstanceOf(DefaultRangedBehavior);
            let impLoc = {x: imp.loc.x, y: imp.loc.y};

            // imp should attack npc, not player
            tgf.nextTurn(imp);
            gm.tick();
            expect(player.hp).toStrictEqual(startHp);
            expect({x: imp.loc.x, y: imp.loc.y}).toEqual(impLoc);
            expect(npc.hp).toStrictEqual(9999);

            // PC should have gotten a message about this
            expect(player.messages.length).toBeGreaterThan(msgLength);

            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });

        test("Haste", () => {
            let ability = player.gainAbility("Haste");
            expect(ability.currentCooldown).toStrictEqual(0);

            expect(pc.selfAbility(ability)).toBeTruthy();
            expect(player.hasConditionNamed("Hasted")).toBeTruthy();

            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });
    });
});

describe("T3", () => {
    let ability: Ability;
    afterEach(() => {
        expect(ability.tier).toStrictEqual(3);
    });

    describe("Instant", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toBeLessThanOrEqual(1);
        });

        test("Arcane Link", () => {
            // Arcane Link - 1d10+INT damage. If you target the last creature you hit with this power, it automatically hits
            ability = player.gainAbility("ArcaneLink");
            player.stats.int = 12; // +1
            gm.fixRolls(19, 4);

            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9995); // rolled 4, +1 stat
            expect(npc.calcDefense(Chain.will).value).toBeLessThanOrEqual(0);

            tgf.nextTurn();
            gm.fixRolls(2, 1);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9993); // rolled 1, +1 stat
        });

        test("Zone of Disfavor", () => {
            // Zone of Disfavor - Ranged burst 1, INT damage, Zone: enemies in zone are disadvantaged (all)
            ability = player.gainAbility("ZoneOfDisfavor");
            player.stats.int = 12; // +1
            gm.fixRolls(19);
            let npcTile = gm.map.tileAt(npc.loc);

            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(9999);  // stat
            expect(npc.advantageStatus()).toBeLessThan(0);
            expect(player.advantageStatus()).toStrictEqual(0);
            expect(npcTile.zone).toBeTruthy();
        });
    });

    describe("Encounter", () => {
        afterEach(() => {
            expect(ability.currentCooldown).toStrictEqual(Duration.Encounter);
        });

        test("Hypnosis", () => {
            // Hypnosis - burst 1, sleep
            ability = player.gainAbility("Hypnosis");
            gm.fixRolls(19);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(10000);
            expect(npc.hasConditionNamed("Asleep")).toBeTruthy();
        });

        test("Misfortune", () => {
            // Misfortune - Slow, disadvantaged (all), debilitated (all), dazed, weakened, ongoing INT
            ability = player.gainAbility("Misfortune");
            gm.fixRolls(19);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(10000);
            expect(npc.calcDefense(Chain.ac).value).toStrictEqual(5);
            expect(npc.calcDefense(Chain.ref).value).toStrictEqual(5);
            expect(npc.hasConditionNamed("Slow")).toBeTruthy();
            expect(npc.hasConditionNamed("Disadvantaged")).toBeTruthy();
            expect(npc.hasConditionNamed("Debilitated")).toBeTruthy();
            expect(npc.hasConditionNamed("Dazed")).toBeTruthy();
            expect(npc.hasConditionNamed("Weakened")).toBeTruthy();
            expect(npc.hasConditionNamed("Ongoing")).toBeTruthy();
        });
    }); // Encounter

    describe('Boss', () => {
        afterEach(() => {
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });

        test("Begone", () => {
            // Begone - teleport target to a random spot in the dungeon
            let origLoc = {x: npc.loc.x, y: npc.loc.y};
            ability = player.gainAbility("Begone");
            gm.fixRolls(19);
            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(10000);
            expect(npc.loc).not.toEqual(origLoc);
        });
    });
})
