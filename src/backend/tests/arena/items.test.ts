import {GameMaster} from "../../gamemaster";
import {PlayerController} from "../../control";
import {Character} from "../../character";
import {Chain} from "../../common";
import {ARMOR} from "../../items/armors";
import {Armor} from "../../items/item";
import {TestGameFactory} from "../testutils";

let tgf: TestGameFactory;
let gm: GameMaster;
let pc: PlayerController;
let player: Character;
let npc: Character;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player, gm} = tgf.testGameData());
    pc = new PlayerController(player, gm);
    npc = tgf.testMonster();
});

describe("Shields", () => {
    test("Happy path", () => {
        expect(player.equipment.offHand.isNothing()).toBeTruthy();
        let originalAc = player.calcDefense(Chain.ac).value;

        let shield = new Armor(ARMOR.Shield);
        player.receiveItem(shield);
        expect(pc.equip(shield)).toBeTruthy();

        let newAc = player.calcDefense(Chain.ac).value;
        expect(newAc).toBeGreaterThan(originalAc);
        expect(player.equipment.offHand).toBe(shield);
    });
});