import {Armor, Item, NothingEquipment, UsableItem, Weapon} from "../items/item";
import {jsonRoundTrip, simpleRoundTripTest, TestGameFactory} from "./testutils";
import {POTIONS} from "../items/potions";
import {ARMOR} from "../items/armors";
import {WEAPONS} from "../items/weapons";
import {Chain, Energy, templateOverride} from "../common";
import {DefenseEffect} from "../effects/passive";
import {Character} from "../character";
import {GameMaster} from "../gamemaster";

let tgf: TestGameFactory;
let gm: GameMaster;
let player: Character;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player} = tgf.testGameData());
})

describe("Serialization", () => {
    test("Basic roundtrip", () => {
        let junk = new Item({
            char: '?',
            name: 'Junk',
            desc: 'junk',
        });
        let saved = jsonRoundTrip(junk);
        let newJunk = Item.fromTemplate(saved);
        expect(newJunk).toEqual(junk);
    });

    test("Usable Roundtrip", () => {
        let healie = new UsableItem(POTIONS.MinorHealingPotion);
        simpleRoundTripTest(healie);
    });

    test("Nothing Roundtrip", () => {
        let nothing = new NothingEquipment();
        let restored = simpleRoundTripTest(nothing);
        expect(restored.isNothing()).toBeTruthy();
    });

    test("Armor Roundtrip", () => {
        let shield = new Armor(ARMOR.Shield);
        simpleRoundTripTest(shield);
    });

    test("Item With Passives Roundtrip", () => {
        let template = templateOverride(ARMOR.PaddedArmor, {
            passiveEffectTemplates: [
                {
                    name: 'DefenseEffect',
                    amount: 1,
                    defense: Chain.ac,
                }
            ]
        });
        let plusone = new Armor(template);
        let newly = simpleRoundTripTest(plusone);
        expect(newly.passiveEffectTemplates).toHaveLength(1);
        expect(newly.passiveEffects).toHaveLength(1);
        let pe = newly.passiveEffects[0];
        expect(pe).toBeInstanceOf(DefenseEffect);
    });

    test("Weapon roundtrip", () => {
        let mace = new Weapon(WEAPONS.Mace);
        simpleRoundTripTest(mace);
    });

    test("IID survives roundtrip", () => {
        let junk = new Item({
            char: '?',
            name: 'Junk',
            desc: 'junk',
        });
        expect(junk.iid).toBeTruthy();
        let saved = jsonRoundTrip(junk);
        let newJunk = Item.fromTemplate(saved);
        expect(newJunk.iid).toStrictEqual(junk.iid);
    });

    test("onBehalfOf doesn't result in loops", () => {
        let shield = new Armor(ARMOR.Shield);
        tgf.equip(shield);
        shield.onBehalfOf = player;
        let exported = jsonRoundTrip(shield);
        expect(exported).toBeTruthy();
        expect(exported.onBehalfOf).toBeFalsy();
    });
});

test("displayName defaults to name", () => {
    let template = ARMOR.Shield;
    let armor = new Armor(template);
    expect(armor.displayName).toStrictEqual(template.name);
    armor.displayName = "Test";
    expect(armor.displayName).toStrictEqual("Test");
    armor.displayName = undefined;
    expect(armor.displayName).toStrictEqual(template.name);

});

describe("Armor", () => {
    test("Heavy armor applies a movement energy penalty", () => {
        let tgf = new TestGameFactory();
        let char = tgf.basicTestMonster({energy: 0});
        expect(char.energy).toStrictEqual(0);

        let armor = new Armor(ARMOR.Chainmail);
        char.receiveItem(armor);
        char.equipment['body'] = armor;

        char.expend(Energy.MOVE);

        expect(char.energy).toStrictEqual(-Energy.MOVE - 1);
    });
});

describe("Item ID", () => {
    test("Items are generated with an item ID", () => {
        let junkTemplate = {
            char: '?',
            name: 'Junk',
            desc: 'junk',
        };
        let junk = new Item(junkTemplate);
        expect(junk.iid).toBeTruthy();

        let junk2 = new Item(junkTemplate);
        expect(junk2.iid).toBeTruthy();
        expect(junk.iid).not.toEqual(junk2.iid);

        // different IID makes them not equal
        expect(junk2).not.toEqual(junk);
    });

    test("Item ID can be assigned at creation", () => {
        let junkTemplate = {
            char: '?',
            name: 'Junk',
            desc: 'junk',
            iid: "abcde",
        };
        let junk = new Item(junkTemplate);
        expect(junk.iid).toBeTruthy();

        let junk2 = new Item(junkTemplate);
        expect(junk2.iid).toBeTruthy();
        expect(junk.iid).toEqual(junk2.iid);

        // same IID makes them equal
        expect(junk2).toEqual(junk);
    });

});