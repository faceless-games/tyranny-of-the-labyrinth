import {GameMaster} from "../gamemaster";
import {Character} from "../character";
import {Duration, Energy} from "../common";
import {BossBehavior, DefaultRangedBehavior, MeleeCombatBehavior, StandThereBehavior} from "../behavior";
import {TestGameFactory} from "./testutils";
import {MONSTERS} from "../monsters/nonboss";
import {Faction} from "../interfaces";
import {PlayerController} from "../control";

let tgf: TestGameFactory;
let gm: GameMaster;
let pc: PlayerController;
let player: Character;
let npc: Character;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player, gm} = tgf.testGameData());
    gm.endTurn(player, Energy.TURN);
    npc = tgf.testMonster({
        loc: {x: 3, y: 3},
        energy: 0,  // So it's ready immediately
    });
    pc = new PlayerController(player, gm);
});

describe("StandThereBehavior", () => {
    beforeEach(() => {
        npc.setBehavior(new StandThereBehavior(), gm);
    });

    test("It just... stands there", () => {
        let {x,y} = npc.loc;
        expect(npc.isReady()).toBeTruthy();
        gm.tick();
        // NPC went
        expect(npc.isReady()).toBeFalsy();
        // They didn't move anywhere
        expect(x).toBe(npc.loc.x);
        expect(y).toBe(npc.loc.y);
    });
});

describe("MeleeCombatBehavior", () => {
    let behavior: MeleeCombatBehavior;
    beforeEach(() => {
        behavior = npc.setBehavior(new MeleeCombatBehavior(), gm) as MeleeCombatBehavior;
    });

    test("Doesn't move toward player if it's too far away", () => {
        npc.sightDistance = 1;
        let {x,y} = npc.loc;
        expect(npc.isReady()).toBeTruthy();
        gm.tick();
        // NPC went
        expect(npc.isReady()).toBeFalsy();
        // They didn't move anywhere
        expect(npc.loc).toEqual({x, y});
    });

    test("Moving toward the player", () => {
        let {x,y} = npc.loc;
        expect(x).toStrictEqual(3);
        expect(y).toStrictEqual(3);
        let {x:px, y:py} = player.loc;
        expect(px).toStrictEqual(1);
        expect(py).toStrictEqual(1);

        expect(npc.isReady()).toBeTruthy();
        gm.tick();
        // npc went
        expect(npc.isReady()).toBeFalsy();

        expect(npc.loc.x).toStrictEqual(x - 1);
        expect(npc.loc.y).toStrictEqual(y - 1);
    });

    test("NPC hits player in melee", () => {
        player.hp = 10000;

        // Put the npc right next to the player
        gm.forceMoveCharacter(npc, {x: 2, y: 1});
        expect(npc.loc.x).toStrictEqual(2);
        expect(npc.loc.y).toStrictEqual(1);

        expect(npc.isReady()).toBeTruthy();
        // Fight, fight, fight!
        gm.fixRolls(19, 1);
        gm.tick();
        expect(npc.isReady()).toBeFalsy();

        // Didn't move because it was too busy fighting
        expect(npc.loc).toEqual({x: 2, y: 1});

        expect(player.hp).toEqual(9999);
    })
});

describe("DefaultRangedBehavior", () => {
    let behavior: DefaultRangedBehavior;
    beforeEach(() => {
        behavior = npc.setBehavior(new DefaultRangedBehavior(), gm) as DefaultRangedBehavior;
    });

    test("Does not move toward player", () => {
        let {x,y} = npc.loc;
        expect(x).toStrictEqual(3);
        expect(y).toStrictEqual(3);

        expect(npc.isReady()).toBeTruthy();
        gm.tick();
        // npc went
        expect(npc.isReady()).toBeFalsy();

        expect(npc.loc.x).toStrictEqual(x);
        expect(npc.loc.y).toStrictEqual(y);
    });

    test("Runs away if in melee range", () => {
        gm.forceMoveCharacter(npc, {x: 2, y: 1});
        expect(npc.isReady()).toBeTruthy();
        gm.tick();

        // Should have run directly away
        expect(npc.loc.x).toStrictEqual(3);
        expect(npc.loc.y).toStrictEqual(1);
    });

    test("Attacks rangedly", () => {
        player.hp = 10000;

        expect(npc.isReady()).toBeTruthy();
        // Fight, fight, fight!
        gm.fixRolls(19, 3);
        gm.tick();
        expect(npc.isReady()).toBeFalsy();

        expect(npc.loc).toEqual({x: 3, y: 3});

        expect(player.hp).toEqual(9997);
    });

    test("Imps' ranged attack override works", () => {
        player.hp = 10000;
        gm.characterDied(npc);  // kill off the old one
        npc = tgf.testMonster(MONSTERS.Imp, {loc: {x: 3, y: 3}});
        behavior = npc.setBehavior(new DefaultRangedBehavior(), gm) as DefaultRangedBehavior;
        npc.energy = 0;
        expect(npc.loc).toEqual({x: 3, y: 3});

        // Should just do its ranged attack that always does 1,
        // so if it does otherwise, let's make sure we notice:
        gm.fixRolls(19, 6);
        gm.tick();
        expect(npc.isReady()).toBeFalsy();

        expect(player.hp).toStrictEqual(9999);
        expect(npc.loc).toEqual({x: 3, y: 3});
    });
});

describe("BossCombatBehavior", () => {
    let behavior: BossBehavior;
    let boss: Character;

    beforeEach(() => {
        const testBoss = "Fighter Moxie";
        boss = gm.spawner.createBoss(testBoss);
        boss.loc = {x: 3, y: 3};
        boss.energy = 0;
        behavior = boss.behavior as BossBehavior;
        gm.characterDied(npc);
        gm.addCharacter(boss);
    });

    test("Boss was assigned a boss behavior", () => {
        expect(behavior).toBeInstanceOf(BossBehavior);
        expect(behavior.gm).toBeTruthy();
    });

    test("Basic attacks are not on weight list", () => {
        expect(behavior.weightList).toBeTruthy();
        let keys = Object.keys(behavior.weightList);
        expect(keys.length).toBeGreaterThan(0);
        expect(boss.abilityNames).toContain("BasicMeleeAttack");
        expect(Object.keys(behavior.weightList)).not.toContain('BasicMeleeAttack')
    });

    test("ChoosePower happy path", () => {
        let power = behavior.choosePower();
        expect(power).toBeTruthy();
    });

    test("Randomly choosing a power on cooldown does default", () => {
        let chosen = boss.abilities.find((a) => {
            return a.cooldown !== Duration.Instant &&
                a.isMelee();
        });
        expect(chosen).toBeTruthy();
        chosen.startCooldown();
        expect(chosen.isReady()).toBeFalsy();

        behavior.weightList = {
            [chosen.name]: 100,
        };
        let power = behavior.choosePower();
        expect(power.name).not.toStrictEqual(chosen.name);
        expect(power.isReady()).toBeTruthy();
        expect(power).toBe(boss.defaultMeleeAbility);
    });

    test("Randomly choosing cooldown does basic if nothing else available", () => {
        let chosen = boss.abilities.find((a) => {
            return a.cooldown !== Duration.Instant &&
                a.isRanged();
        });
        expect(chosen).toBeTruthy();
        chosen.startCooldown();

        behavior.weightList = {
            [chosen.name]: 100,
        };
        let power = behavior.choosePower();
        expect(power.name).not.toStrictEqual(chosen.name);
        expect(power.isReady()).toBeTruthy();
        expect(power.basic).toBeTruthy();
        expect(power).toBe(boss.defaultRangedAbility);
    });

    test("Randomly choosing melee if not in range moves", () => {
        let chosen = boss.abilities.find((a) => {
            return a.cooldown !== Duration.Instant &&
                a.isMelee();
        });
        expect(chosen).toBeTruthy();
        behavior.weightList = {
            [chosen.name]: 100,
        };

        expect(boss.isReady()).toBeTruthy();
        let {x,y} = npc.loc;
        expect(x).toStrictEqual(3);
        expect(y).toStrictEqual(3);

        gm.tick();
        // npc went
        expect(boss.isReady()).toBeFalsy();

        // Moved toward player and didn't use ability
        expect(boss.loc.x).toStrictEqual(x - 1);
        expect(boss.loc.y).toStrictEqual(y - 1);
        expect(chosen.isReady()).toBeTruthy();
    });
});

describe("BaseBehavior", () => {
    test("Doesn't crash if it can't find a new target", () => {
        // Brainwash our NPC
        npc.faction = Faction.Player;
        npc.setBehavior(new MeleeCombatBehavior({gm, character: npc}));
        pc.wait();

        expect(npc.isReady()).toBeTruthy();
        // Their energy should already be 0, per initialization above, so here we go:
        gm.tick();
        // Should have just waited
        expect(npc.isReady()).toBeFalsy();
    });
});