// This file is for more generic ability tests.
// Specific ability tests can be found:
// * In the tests for the class that uses that ability (e.g. fighter.test.ts)
// * In gamemaster.test.ts

import {
    ABILITIES,
    Ability
} from "../abilities/ability";
import {GameMaster} from "../gamemaster";
import {PlayerController} from "../control";
import {Character} from "../character";
import {Dice} from "../dice";
import {Chain, Duration} from "../common";
import {Condition, CONDITIONS} from "../effects/condition";
import {
    DamageSpecParser,
    DefensePortion, DicePortion, NumberPortion,
    SpecPortion,
    StatPortion,
    TohitSpecParser,
    WeaponDamagePortion
} from "../parsers";
import {jsonRoundTrip, TestGameFactory} from "./testutils";

let tgf: TestGameFactory;
let gm: GameMaster;
let pc: PlayerController;
let player: Character;
let npc: Character;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player, gm} = tgf.testGameData());
    pc = new PlayerController(player, gm);
    npc = tgf.testMonster();
});

describe("Ability", () => {
    let placeholder = new Ability(ABILITIES.newFromName('BasicMeleeAttack'));

    describe("Tohit Parsing", () => {
        let parser: TohitSpecParser

        beforeEach(() => {
            parser = new TohitSpecParser();
        });

        test("Explodes on unrecognized portion", () => {
            expect(() => parser.parseOne('barf')).toThrow(/barf/);
        });

        describe("Stats", () => {

            test("Recognized by SpecParser", () => {
                let portion = parser.parseOne('cha');
                expect((portion as StatPortion).stat).toBeTruthy();
            });
            // Stat and bonus extraction done in damage tests below
        });

        describe("Raw numbers", () => {
            test("Recognized by SpecParser", () => {
                let portion = parser.parseOne("800");
                expect((portion as NumberPortion).value).toBeTruthy();
            });

            test("Extracts the number", () => {
                let portionStr = "456";
                let portion = new NumberPortion(portionStr);
                expect(portion.value).toStrictEqual(456);
            });

        })

        describe("Defenses", () => {
            test("Recognized by SpecParser", () => {
                let portion = parser.parseOne('will');
                expect((portion as DefensePortion).defense).toBeTruthy();
            });

            test("Extracts defense", () => {
                let portionStr = 'fort';
                let portion = new DefensePortion(portionStr);
                expect(portion.defense).toStrictEqual(Chain.fort);
            });

            test("Gets defense from opponent", () => {
                let cond = new Condition(CONDITIONS.Barkskin);
                npc.receiveCondition(cond);

                let portion = new DefensePortion('ac');
                let result = portion.act(player, placeholder, npc, gm, Dice.zero);
                expect(result.value).toStrictEqual(12);

            });
        });

        describe("Complex interactions", () => {
            test("Most straightforward case", () => {
                let spec = "str vs ac";
                let left = parser.parse(spec);
                expect(left).toBe(parser.leftPortions);
                expect(left).toHaveLength(1);
                expect((left[0] as StatPortion).stat).toStrictEqual('str');
                let right = parser.rightPortions;
                expect(right).toHaveLength(1);
                expect((right[0] as DefensePortion).defense).toStrictEqual(Chain.ac);
            });

            test("Segment addition", () => {
                let spec = "con+wis vs fort+will";
                let left = parser.parse(spec);
                expect(left).toBe(parser.leftPortions);
                expect(left).toHaveLength(2);
                expect((left[0] as StatPortion).stat).toStrictEqual('con');
                expect((left[1] as StatPortion).stat).toStrictEqual('wis');
                let right = parser.rightPortions;
                expect(right).toHaveLength(2);
                expect((right[0] as DefensePortion).defense).toStrictEqual(Chain.fort);
                expect((right[1] as DefensePortion).defense).toStrictEqual(Chain.will);
            });

            test("Understands subtraction", () => {
                let spec = "dex-1 vs ac";
                let left = parser.parse(spec);
                expect(left).toHaveLength(2);
                let numPortion = left[1] as NumberPortion;
                expect(numPortion.value).toStrictEqual(1);  // value is the raw number
                expect(numPortion.negative).toBeTruthy();  // but it's marked as negative,

                let result = numPortion.act(player, placeholder, npc, gm, Dice.zero);
                expect(result.value).toStrictEqual(-1);  // so it ends up negative
            });

            test("Acting on a full expression", () => {
                let spec = "dex+1 vs ac";
                player.stats.dex = 16; // +3
                let cond = new Condition(CONDITIONS.Barkskin);
                npc.receiveCondition(cond);  // +2
                gm.fixRolls(10);

                let result = parser.act(spec, player, placeholder, npc, gm);
                expect(result).toBe(parser.roll);
                expect(result.value).toStrictEqual(14);
                expect(parser.target.value).toStrictEqual(12);
            });
        });
    });

    describe("Damage Parsing", () => {
        let parser: DamageSpecParser;

        beforeEach(() => {
            parser = new DamageSpecParser();
        });

        test("Explodes on unrecognized portion", () => {
            expect(() => parser.parseOne('blarf')).toThrow(/blarf/);
        });

        describe("Dice", () => {
            test("Recognized by SpecParser", () => {
                let portion = parser.parseOne("2d6");
                let dice = (portion as DicePortion).dice;
                expect(dice).toBeTruthy();
            });

            test("Extracts dieRolls", () => {
                let portionStr = "2d8";
                let portion = new DicePortion(portionStr);

                expect(portion.dice.n).toStrictEqual(2);
                expect(portion.dice.d).toStrictEqual(8);
            });

            test("Rolls that die", () => {
                let start = Dice.zero;
                let portionStr = "2d4";
                let portion = new DicePortion(portionStr);

                // Only results in the sum because I fix the result
                // of `roll`, not the two rolls themselves.
                gm.fixRolls(8);
                let result = portion.act(player, placeholder, npc, gm, start);
                expect(result.value).toStrictEqual(8);
            });
        });

        describe("Weapons", () => {
            test("Recognized by SpecParser", () => {
                let portion = parser.parseOne("2W");
                expect((portion as WeaponDamagePortion).dieRolls).toBeTruthy();
            })

            test("Extracts dieRolls", () => {
                let portionStr = "3W";
                let portion = new WeaponDamagePortion(portionStr);

                expect(portion.dieRolls).toStrictEqual(3);
            });

            test("Rolls weapon die", () => {
                let wpnDice = jest.fn().mockImplementation(() => {
                    return new Dice({d: 6});
                })
                player.weaponDice = wpnDice;

                let start = Dice.zero;
                let portionStr = "1W";
                let portion = new WeaponDamagePortion(portionStr);

                gm.fixRolls(6);
                let result = portion.act(player, placeholder, npc, gm, start);
                expect(result.value).toStrictEqual(6);
                expect(wpnDice).toHaveBeenCalled();
            });
        });

        describe("Stats", () => {
            test("Recognized by SpecParser", () => {
                let portion = parser.parseOne('str');
                expect((portion as StatPortion).stat).toBeTruthy();
            });

            test("Extracts stat", () => {
                let portionStr = "dex";
                let portion = new StatPortion(portionStr);

                expect(portion.stat).toStrictEqual("dex");
            });

            test("Gets bonus from character", () => {
                player.stats.wis = 16;
                let portion = new StatPortion('wis');
                let result = portion.act(player, placeholder, npc, gm, Dice.zero);
                expect(result.value).toStrictEqual(3);
            });
        });

        describe('Raw Numbers', function () {
            test("Recognized by SpecParser", () => {
                let portion = parser.parseOne('56');
                expect((portion as NumberPortion).value).toBeTruthy();
            });
        });

        describe("Complex interactions", () => {
            test("Actually not that complex: just one segment", () => {
                let spec = "cha";
                let portions = parser.parse(spec);
                expect(portions).toHaveLength(1);
                let portion = portions[0];
                expect((portion as StatPortion).stat).toStrictEqual('cha');
            });

            test("Adding two segments", () => {
                let spec = "2W+ con";
                let portions = parser.parse(spec);
                expect(portions).toHaveLength(2);
                expect((portions[0] as WeaponDamagePortion).dieRolls).toStrictEqual(2);
                expect((portions[1] as StatPortion).stat).toStrictEqual('con');
            });

            test("Acting on a full expression", () => {
                let spec = "1W +dex + 2-1 + 1d12"
                player.stats.dex = 8;
                gm.fixRolls(6, 6);

                let result = parser.act(spec, player, placeholder, npc, gm);
                expect(result.value).toStrictEqual(12);
            });

        });
    });

    test("isReady", () => {
        // Ready by default
        expect(placeholder.isReady()).toBeTruthy();
    });

    describe("Cooldown", () => {
        test("Boss doesn't expend on miss", () => {
            let ability = player.gainAbility("HitEvenHarder");
            gm.fixRolls(2);

            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(10000);
            expect(ability.currentCooldown).toStrictEqual(0);
        });

        test("Boss expends on miss if it has 'miss' clause", () => {
            let ability = player.gainAbility("Sleep");
            gm.fixRolls(2);

            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hasConditionNamed("Asleep")).toBeFalsy();
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });

        test("Boss expends if even one enemy is hit", () => {
            let ability = player.gainAbility("PocketSand");
            gm.fixRolls(2, 19);

            // need another NPC
            let npc2 = tgf.testMonster({
                loc: {x: 3, y: 2},
                baseMaxHp: 5000,
                name: 'npc2',
            });

            expect(pc.attack(ability, npc.loc)).toBeTruthy();
            expect(npc.hp).toStrictEqual(10000);
            expect(npc2.hp).toBeLessThan(5000);
            expect(ability.currentCooldown).toStrictEqual(Duration.Boss);
        });
    });

    describe('Serialization', function () {
        test("Ability excludes realized items", () => {
            let exported = jsonRoundTrip(placeholder);
            expect(exported['preRoll']).toBeFalsy();
            expect(exported['onHit']).toBeFalsy();
            expect(exported['invokers']).toBeFalsy();
        });

        test("Ability roundtrip", () => {
            let exported = jsonRoundTrip(placeholder);
            let ability = new Ability(exported);
            expect(ability).toEqual(placeholder);
        });

        test("Ability with effects roundtrip", () => {
            let heal = player.gainAbility("BasicHeal");
            let exported = jsonRoundTrip(heal);
            let ability = new Ability(exported);
            expect(ability).toEqual(heal);
        });
    });
});