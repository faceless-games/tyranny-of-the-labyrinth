import {AddableResult, Dice} from "../dice";

describe("AddableResult", () => {
    test("Additions do what I expect", () => {
        let result = new AddableResult(5);
        expect(result.value).toStrictEqual(5);

        result.add(2, "testing");
        expect(result.value).toStrictEqual(7);

        result.add(-10, "more testing");
        expect(result.value).toStrictEqual(-3);
    });

    describe("Minmax", () => {
        test("Lower bounds", () => {
            let result = new AddableResult(5);
            result.minimum = 10;
            expect(result.value).toStrictEqual(10);
        });

        test("Upper bounds", () => {
            let result = new AddableResult(5);
            result.maximum = 2;
            expect(result.value).toStrictEqual(2);
        });
    });

    test("Multipliers work how I expect", () => {
        let result = new AddableResult(10);
        expect(result.value).toStrictEqual(10);

        result.addMultiplier(1, "testing");
        expect(result.value).toStrictEqual(20);

        result.add(2, "testing flat number + multiplier");
        // 10+2 => 12 * (1+1) => 24
        expect(result.value).toStrictEqual(24);

        result.addMultiplier(-0.5, "testing negative multipliers");
        // 10+2 => 12 * (1+1-0.5) => 18
        expect(result.value).toStrictEqual(18);
    });

    test("Multipliers can't flip the number", () => {
        let result = new AddableResult(10);
        result.addMultiplier(-1, "testing");
        expect(result.value).toStrictEqual(0);

        result.addMultiplier(-1, "super negative");
        expect(result.value).toStrictEqual(0);

        // But those negative multipliers do have to be individually canceled out
        result.addMultiplier(1, "testing");
        expect(result.value).toStrictEqual(0);

        result.addMultiplier(1, "testing");
        expect(result.value).toStrictEqual(10);
    });

    test("Booleans work how I expect", () => {
        let result = new AddableResult();
        expect(result.passes).toBeTruthy();

        result.addBoolean(true, 'cuz');
        expect(result.passes).toBeTruthy();

        result.fail('epic');
        expect(result.passes).toBeFalsy();

        result.addBoolean(true, 'Redemption arc?');
        expect(result.passes).toBeFalsy();  // one fail = everything fails
    });
});

describe("Dice", () => {
    test("Rolling 1d6 seems okay", () => {
        for(let i=0; i < 1000; i++) {
            let result = Dice.roll(1, 6);
            expect(result.value).toBeLessThan(7);
            expect(result.value).toBeGreaterThan(0);
        }
    });

    test("Dice additions do what I expect", () => {
        let die = Dice.fixed(10);
        expect(die.value).toStrictEqual(10);

        die.add(3, "testing");
        expect(die.value).toStrictEqual(13);

        die.add(-12, "more testing");
        expect(die.value).toStrictEqual(1);

    });

    test("Description works", () => {
        let die1 = new Dice();
        expect(die1.description()).toBe("1d20");

        let die2 = new Dice({n:2, d: 6, plus: 1});
        expect(die2.description()).toBe("2d6+1");

        let die3 = new Dice({d: 8, plus: -1});
        expect(die3.description()).toBe("1d8-1");
    });
})
