import {Character} from "../character";
import {TileMap} from "../world";
import {GameMaster} from "../gamemaster";
import {StandThereBehavior} from "../behavior";
import {EquippableItem, IEquippableItemTemplate, Item} from "../items/item";
import {AbstractGameFactory} from "../newgame";
import {ICharacterTemplate, IItemTemplate} from "../interfaces";
import {IEffectTemplate} from "../effects/effect";
import {Condition, CONDITIONS} from "../effects/condition";
import {BestowConditionEffect} from "../effects/active";
import {PASSIVES} from "../effects/passive_powers";

interface PlayerMapGm {
    player: Character,
    map: TileMap,
    gm: GameMaster,
}

export class TestGameFactory extends AbstractGameFactory {
    get player(): Character {
        return this.gm.player;
    }

    newGame(): GameMaster {
        this.easySetup(null, true);
        // Most tests will not be expecting new spawns to appear
        this.gm.spawner.spawnLimit = 0;
        return this.gm;
    }

    public testGameData(): PlayerMapGm {
        if(this.gm === undefined) {
            this.gm = this.newGame();
        }
        return {
            player: this.gm.player,
            map: this.gm.map,
            gm: this.gm
        };
    }

    junkItem(template?: Partial<IItemTemplate>): Item {
        let defaults = new Item({
            char: '?',
            name: 'Junk',
            desc: 'junk',
        });
        if(template) Object.assign(defaults, template);
        return defaults;
    }

    basicTestMonster(
        template?: Partial<ICharacterTemplate>,
        overrideTemplate?: Partial<ICharacterTemplate>): Character {
        let defaults: ICharacterTemplate = {
            char: 'k',
            name: 'Kobold',
            baseMaxHp: 10000,
            hpPerLevel: 0,
            loc: {x: 2, y: 2},
            energy: -5000,
            description: "test",
        };
        if(template && overrideTemplate) Object.assign(template, overrideTemplate);
        if(template) Object.assign(defaults, template);
        return new Character(defaults);
    }

    testMonster(
        template?: Partial<ICharacterTemplate>,
        overrideTemplate?: Partial<ICharacterTemplate>): Character
    {
        let gm = this.gm;
        let monster = this.basicTestMonster(template, overrideTemplate);
        monster.setBehavior(new StandThereBehavior(), gm);
        gm.addCharacter(monster);
        return monster;
    }

    nextTurn(player: Character = null) {
        player = player || this.gm.player;
        player.energy = -1;
        this.gm.tick();
    }

    bestowAndVerify(conditionName: string, effectOverrides?: Partial<IEffectTemplate>): Condition {
        return this.bestowAndVerifyOn(this.player, conditionName, effectOverrides);
    }

    bestowAndVerifyOn(who: Character, conditionName: string, effectOverrides?: Partial<IEffectTemplate>): Condition {
        let conditionTemplate = CONDITIONS[conditionName];
        expect(conditionTemplate).toBeTruthy();
        let effectTemplate = {
            name: "bestowAndVerify",
            effectOverrides,
            condition: conditionTemplate,
        }
        let bce = new BestowConditionEffect(effectTemplate);
        bce.invoke(this.player, who, this.gm);
        let condition = who.hasConditionNamed(conditionTemplate.name);
        expect(condition).toBeTruthy();
        return condition;
    }

    bestowAndVerifyPassive(passiveName: string): Condition {
        let template = PASSIVES[passiveName];
        expect(template).toBeTruthy();

        let passive = new Condition(template);
        this.gm.player.gainPassive(passive);
        expect(this.gm.player.hasPassiveNamed(passiveName)).toBeTruthy();

        return passive;
    }

    equip(item: EquippableItem, test=true): EquippableItem {
        this.player.receiveItem(item);
        let equipped = this.gm.equipItem(this.player, item);
        if(test) {
            expect(equipped).toBeTruthy();
        }
        this.nextTurn();
        if(equipped) return item;
        return null;
    }

}

export function jsonRoundTrip(obj: any): any {
    let strung = JSON.stringify(obj);
    return JSON.parse(strung);
}

export function simpleRoundTripTest(original: any): any {
    let expectedName = original.constructor.name;
    let saved = jsonRoundTrip(original);
    expect(saved['itemType']).toStrictEqual(expectedName);
    let newItem = original.constructor.fromTemplate(saved);
    expect(newItem).toEqual(original);
    expect(newItem.constructor.name).toStrictEqual(original.constructor.name);
    return newItem;
}