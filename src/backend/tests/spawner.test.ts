import {TestGameFactory} from "./testutils";
import {PlayerController} from "../control";
import {GameMaster} from "../gamemaster";
import {Character} from "../character";
import {ALL_MONSTER_SPAWNS, Spawner, SpawnPoolByTier} from "../monsters/spawner";
import {DefaultRangedBehavior, StandThereBehavior} from "../behavior";
import {Faction, ICharacterTemplate, Weightable} from "../interfaces";
import {BOSSES} from "../classes";
import {Chain, Duration} from "../common";
import {MONSTERS} from "../monsters/nonboss";
import {applyVariant, IVariantTemplate, VARIANTS} from "../monsters/variants";

let tgf: TestGameFactory;
let gm: GameMaster;
let pc: PlayerController;
let player: Character;
let spawner: Spawner;
let testSpawnPool: Weightable;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player, gm} = tgf.testGameData());
    pc = new PlayerController(player, gm);
    spawner = gm.spawner;
    spawner.spawnLimit = 30;
    spawner.variantChance = 0;
    testSpawnPool = {
        "Kobold": 10,
    }
    spawner.spawnPool = testSpawnPool;
});

test("Spawner automatically created", () => {
    expect(spawner).toBeTruthy();
});

test("createMonster", () => {
    player.level = 2;
    let monster = spawner.createMonster();
    expect(monster.name).toStrictEqual("Kobold");
    // created monsters should have some amount of negative energy
    // so they can't act immediately
    expect(monster.energy).toBeLessThan(0);
    // newly spawned monsters match the player's level
    expect(monster.level).toStrictEqual(2);
});

describe("tick", () => {
    test("Happy Path", () => {
        expect(gm.spawnCount).toStrictEqual(0);

        // First tick won't spawn a monster (as tickNum is incremented
        // before modulo check)
        spawner.tick();
        expect(gm.spawnCount).toStrictEqual(0);
        // Skip up to next period
        while(spawner.tickNum < spawner.spawnPeriod-1) {
            spawner.tick();
            expect(gm.spawnCount).toStrictEqual(0);
        }

        // But now it will
        spawner.tick();
        expect(gm.spawnCount).toStrictEqual(1);

        // Done again for a while
        spawner.tick();
        expect(gm.spawnCount).toStrictEqual(1);
    });

    test("Respects spawnLimit", () => {
        spawner.spawnLimit = 2;
        spawner.spawnPeriod = 1;
        expect(gm.spawnCount).toStrictEqual(0);
        spawner.tick();
        expect(gm.spawnCount).toStrictEqual(1);
        spawner.tick();
        expect(gm.spawnCount).toStrictEqual(2);
        spawner.tick();
        expect(gm.spawnCount).toStrictEqual(2);
    });

});

describe("createAndSpawnMonster", () => {
    test("Happy path", () => {
        expect(gm.characters).toHaveLength(1);  // player

        let monster = spawner.createAndSpawnMonster();
        expect(monster).toBeTruthy();
        expect(gm.characters).toHaveLength(2);
        let loc = monster.loc;
        let tile = gm.map.tileAt(loc);
        expect(tile.lit).toBeFalsy();
        expect(tile.character).toBe(monster);
    });

    test("Levels add to stats", () => {
        // Default is no bonuses
        let monster = spawner.createAndSpawnMonster();
        expect(monster).toBeTruthy();
        expect(monster.stats.str).toStrictEqual(10);

        // +1 doesn't really do much, but it's there
        player.level = 2;
        monster = spawner.createAndSpawnMonster();
        expect(monster).toBeTruthy();
        expect(monster.level).toStrictEqual(2);
        expect(monster.stats.dex).toStrictEqual(11);

        // +2 overall should add to lots of things
        let origAc = monster.calcDefense(Chain.ac).value;
        let origWill = monster.calcDefense(Chain.will).value;

        player.level = 3;
        monster = spawner.createAndSpawnMonster();
        expect(monster).toBeTruthy();
        expect(monster.level).toStrictEqual(3);
        expect(monster.stats.dex).toStrictEqual(12);

        expect(monster.calcDefense(Chain.ac).value).toStrictEqual(origAc+1);
        expect(monster.calcDefense(Chain.will).value).toStrictEqual(origWill+1);

        // Skip ahead to +7
        player.level = 8;
        monster = spawner.createAndSpawnMonster();
        expect(monster).toBeTruthy();
        expect(monster.level).toStrictEqual(8);
        expect(monster.stats.dex).toStrictEqual(17);

        expect(monster.calcDefense(Chain.ac).value).toStrictEqual(origAc+3);
        expect(monster.calcDefense(Chain.will).value).toStrictEqual(origWill+3);

    });

    test("Spawn limit", () => {
        spawner.spawnLimit = 1;
        let monster = spawner.createAndSpawnMonster();
        expect(monster).toBeTruthy();
        expect(gm.spawnCount).toStrictEqual(1);

        // Won't spawn another
        let newMonster = spawner.createAndSpawnMonster();
        expect(newMonster).toBeFalsy();

        // Can kill and re-spawn okay though
        gm.characterDied(monster);
        monster = spawner.createAndSpawnMonster();
        expect(monster).toBeTruthy();

        // Can force spawn
        newMonster = spawner.createAndSpawnMonster(true);
        expect(newMonster).toBeTruthy();
        expect(gm.spawnCount).toStrictEqual(2);

        // Definitely can't spawn a new one now
        let lastMonster = spawner.createAndSpawnMonster();
        expect(lastMonster).toBeFalsy();

        // Gotta kill some off
        gm.characterDied(newMonster);

        // Still not enough
        newMonster = spawner.createAndSpawnMonster();
        expect(newMonster).toBeFalsy();

        // Kill last one and spawn a new one just because we can
        gm.characterDied(monster);
        monster = spawner.createAndSpawnMonster(true);
        expect(monster).toBeTruthy();
    });

    test("Correctly assign named behavior", () => {
        spawner.spawnPool = {
            "Elf": 10,
        };
        let monster = spawner.createAndSpawnMonster();
        expect(monster).toBeTruthy();
        expect(monster.behaviorName).toStrictEqual("DefaultRangedBehavior");
        expect(monster.behavior).toBeInstanceOf(DefaultRangedBehavior);
    });

    test("Correctly assign default behavior", () => {
        let monster = spawner.createAndSpawnMonster();
        expect(monster).toBeTruthy();
        expect(monster.behaviorName).toBeTruthy();
        expect(monster.behavior).toBeTruthy();
        expect(monster.behavior).not.toBeInstanceOf(StandThereBehavior);
    });

    test("Assigns non-boss defaults correctly", () => {
        spawner.spawnPool = {"DwarfWizard": 10};
        let monster = spawner.createAndSpawnMonster();
        expect(monster).toBeTruthy();
        expect(monster.behaviorName).toStrictEqual("BossBehavior");
        expect(monster.defaultMeleeAbility).toBeTruthy();
        expect(monster.defaultRangedAbility).toBeTruthy();
    });

    test("Troll regen", () => {
        spawner.spawnPool = {"Troll": 10};
        let monster = spawner.createAndSpawnMonster();
        expect(monster).toBeTruthy();
        monster.hp = 1;
        pc.wait();
        tgf.nextTurn(monster);
        expect(monster.hp).toBeGreaterThan(1);
        tgf.nextTurn(monster);
        expect(monster.hp).toBeGreaterThan(2);
    })
});

describe("Bosses", () => {
    const testBoss = Object.keys(BOSSES)[0];

    test("Can create a boss", () => {
        player.level = 2;
        let boss = spawner.createBoss(testBoss);
        expect(boss).toBeTruthy();
        expect(boss.faction).toStrictEqual(Faction.Enemy);
        expect(boss.name).toStrictEqual(testBoss);

        // Should have more than just the two default abilities
        expect(boss.abilities.length).toBeGreaterThan(2);

        // Should match the player's level
        expect(boss.level).toStrictEqual(2);
    });

    test("All boss names map to a class", () => {
        let keys = Object.keys(BOSSES);
        for(let key of keys) {
            expect(spawner.createBoss(key)).toBeTruthy();
        }
    });

    test("Nonbasic, instant defaults", () => {
        const bossName = "Fighter Moxie";
        let boss = spawner.createBoss(bossName);

        expect(boss.defaultMeleeAbility.basic).toBeFalsy();
        // But there are no instant non-basic ranged, so:
        expect(boss.defaultRangedAbility.cooldown).toStrictEqual(Duration.Instant);
        expect(boss.defaultRangedAbility.basic).toBeTruthy();
    });

    test("Attempting to create nonexistent boss throws", () => {
        expect(() => {spawner.createBoss("Zorbzorb")}).toThrow();
    });

    test("Boss abilities should match current tier", () => {
        let boss = spawner.createBoss(testBoss);
        expect(boss).toBeTruthy();

        // Should have basic T1 ability
        expect(boss.abilityByName("ShieldPush")).toBeTruthy();
        // Should not have T2 abilities
        expect(boss.abilityByName("CarefulStrike")).toBeFalsy();
        for(let ability of boss.abilities) {
            expect(ability.tier).toBeLessThanOrEqual(1);
        }

        spawner.adoptNextTier();
        boss = spawner.createBoss(testBoss);
        expect(boss).toBeTruthy();
        expect(boss.abilityByName("ShieldPush")).toBeTruthy();
        expect(boss.abilityByName("CarefulStrike")).toBeTruthy();
        for(let ability of boss.abilities) {
            expect(ability.tier).toBeLessThanOrEqual(2);
        }
    });
});

describe("Sanity", () => {
    test("All non-boss monsters can spawn", () => {
        for(const [k, v] of Object.entries(MONSTERS)) {
            expect(ALL_MONSTER_SPAWNS).toHaveProperty(k);
        }
    });

    test("All spawnable monsters are real", () => {
        for(const k of Object.keys(ALL_MONSTER_SPAWNS)) {
            expect(MONSTERS).toHaveProperty(k);
        }
    });

    const monsterTable = Object.values(MONSTERS).map(m => [m.name, m]);
    test.each(monsterTable)
    ('Has unique character or color: %s', (name: string, monster: ICharacterTemplate) => {
        for(const m of Object.values(MONSTERS)) {
            if(m !== monster) {
                let charequal = m.char == monster.char;
                let colorEqual = m.fgcolor == monster.fgcolor;
                expect(charequal && colorEqual).toBeFalsy();
            }
        }
    });
});

describe("tiering up", () => {
    beforeEach(() => {
        expect(spawner.adoptNextTier()).toBeTruthy();
    });

    test("Actually tiers up", () => {
        expect(spawner.tierNum).toStrictEqual(2);
    })

    test("Adds next tier's spawn table", () => {
        expect(spawner.spawnPool).toHaveProperty("Hobgoblin");
        // But preserves the previous tier, unlike treasurer
        expect(spawner.spawnPool).toHaveProperty("Kobold");
        expect(spawner.adoptNextTier()).toBeTruthy();
        expect(spawner.spawnPool).toHaveProperty("Troll");
    });

    test("Doesn't mutate original spawn table", () => {
        expect(testSpawnPool).not.toHaveProperty("Hobgoblin");
        expect(spawner.adoptNextTier()).toBeTruthy();
        expect(testSpawnPool).not.toHaveProperty("Troll");
    });
});

describe("Specific Variants", () => {
    test("Apply zombie", () => {
        let koboldTemplate = MONSTERS.Kobold;
        let zombie = VARIANTS.Zombie;
        let koboldZombie = applyVariant(koboldTemplate, zombie);

        expect(koboldZombie).not.toBe(koboldTemplate);
        expect(koboldZombie).not.toBe(zombie);

        // Straight-up assignment works:
        expect(koboldZombie.baseMovementSpeed).toStrictEqual(3);
        // Renaming works:
        expect(koboldZombie.name).toStrictEqual("Zombie Kobold");
        // HP Multiplier works:
        expect(koboldZombie.baseMaxHp).toStrictEqual(6);
    });

    test("Apply zombie to complex monster", () => {
        let impZombieTemplate = applyVariant(MONSTERS.Imp, VARIANTS.Zombie);

        expect(impZombieTemplate.behaviorName).toStrictEqual("MeleeCombatBehavior");
        expect(impZombieTemplate.abilityNames).toHaveLength(1);

        // Realizing it so I can check to see that it's melee-flavored now
        let impZombie = new Character(impZombieTemplate);
        expect(impZombie.abilities).toHaveLength(1);
        expect(impZombie.abilities[0].name).toStrictEqual("BasicMeleeAttack");
    });

    test("Feral", () => {
        let feralKobold = applyVariant(
            MONSTERS.Kobold,
            VARIANTS.Feral,
        );
        expect(feralKobold.name).toStrictEqual("Feral Kobold");
        expect(feralKobold.baseMaxHp).toStrictEqual(1);
        expect(feralKobold.hpPerLevel).toStrictEqual(0);
        expect(feralKobold.baseMovementSpeed).toStrictEqual(8);
    });

    describe("Bosses", () => {
        test("Fighter Moxie", () => {
            let stalwartKobold = applyVariant(
                MONSTERS.Kobold,
                VARIANTS.BossFighter,
            );
            expect(stalwartKobold.name).toStrictEqual("Stalwart Kobold");

            //  Base AC bonus:
            expect(stalwartKobold.baseAc).toStrictEqual(12);
        });

        test("Wizard Zankar", () => {
            let arcaneKobold = applyVariant(
                MONSTERS.Kobold,
                VARIANTS.BossWizard,
            );
            expect(arcaneKobold.name).toStrictEqual("Arcane Kobold");

            // Increased int
            expect(arcaneKobold.stats).toBeTruthy();
            expect(arcaneKobold.stats.int).toStrictEqual(12);
            // Increased will (realizing to use calcDefense)
            let kobold = new Character(arcaneKobold);
            expect(kobold.calcDefense(Chain.will).value).toBeGreaterThan(10);
            // But that didn't change wis
            expect(kobold.stats.wis).toStrictEqual(10);
        });

        test("Cleric Llynmir", () => {
            let piousKobold = applyVariant(
                MONSTERS.Kobold,
                VARIANTS.BossCleric,
            );
            expect(piousKobold.name).toStrictEqual("Pious Kobold");

            // Increased wis
            expect(piousKobold.stats).toBeTruthy();
            expect(piousKobold.stats.wis).toStrictEqual(12);
            // Increased fort
            let kobold = new Character(piousKobold);
            expect(kobold.calcDefense(Chain.fort).value).toBeGreaterThan(10);
            expect(kobold.stats.con).toStrictEqual(10);
        });

        test("Rogue Jayne", () => {
            let agileKobold = applyVariant(
                MONSTERS.Kobold,
                VARIANTS.BossRogue,
            );
            expect(agileKobold.baseMovementSpeed).toStrictEqual(6);
            expect(agileKobold.name).toStrictEqual("Agile Kobold");

            // Stat upgrades
            expect(agileKobold.stats).toBeTruthy();
            expect(agileKobold.stats.dex).toStrictEqual(12);
            // Didn't mess with other stats
            expect(agileKobold.stats.str).toStrictEqual(10);
            // realizing preserves these stats
            expect(new Character(agileKobold).stats.dex).toStrictEqual(12);

            // Base HP Multiplier undefined doesn't cause problems
            expect(agileKobold.baseMaxHp).not.toBeNaN();
            expect(agileKobold.baseMaxHp).toBeGreaterThan(0);
        });
    });
});

describe("Variant spawning support", () => {
    const BOSS_VARIANT_POOL = {
        "BossFighter": 50,
        "BossWizard": 50,
        "BossCleric": 50,
        "BossRogue": 50,
    }
    test("Correctly spawns variant", () => {
        spawner.variantPool = {
            "Zombie": 100,
        };
        spawner.variantChance = 1;
        let monster = spawner.createAndSpawnMonster();

        expect(monster).toHaveProperty("variantPrefix");  // variant applied
        expect(monster.name.substr(0, 6)).toStrictEqual("Zombie");
    });

    test("Correctly spawns boss variant", () => {
        spawner.variantPool = BOSS_VARIANT_POOL;
        spawner.variantChance = 1;
        let monster = spawner.createAndSpawnMonster();

        expect(monster).toHaveProperty("variantPrefix");  // variant applied
    });

    test("Can't spawn defeated boss variant", () => {
        let bossNames = Object.keys(BOSSES);
        let undefeated = bossNames[bossNames.length - 1];
        for(let i=0; i < bossNames.length - 1; i++) {
            let name = bossNames[i];
            expect(gm.canChallengeBoss(name)).toBeTruthy();
            gm.defeatedBosses.push(name);
            expect(gm.canChallengeBoss(name)).toBeFalsy();
        }
        expect(gm.canChallengeBoss(undefeated)).toBeTruthy();

        spawner.variantPool = BOSS_VARIANT_POOL;
        spawner.variantChance = 1;
        // spawn a buncha them to make sure they're either the last remaining or nothing
        let variantSpawned = 0;
        for(let i=0; i < 100; i++) {
            let newbie = spawner.createAndSpawnMonster(true);
            let vbn = (newbie as any).variantBossName;
            if(vbn) {
                expect(vbn).toStrictEqual(undefeated);
                variantSpawned += 1;
            }
        }
        expect(variantSpawned).toBeGreaterThan(0);  // 0.00015% chance for this to be wrong (with 4 bosses)

        // Defeat that boss:
        gm.defeatedBosses.push(undefeated);
        expect(gm.canChallengeBoss(undefeated)).toBeFalsy();

        // Now we can't spawn variants so they're all normal
        for(let i=0; i < 100; i++) {
            let newbie = spawner.createAndSpawnMonster(true);
            expect(newbie).not.toHaveProperty("variantPrefix");
        }
    });
});
