import {TestGameFactory} from "./testutils";
import {GameMaster, Tier} from "../gamemaster";
import {ALL_DROPS, masterLootTableByTier, Treasurer} from "../items/treasurer";
import {Armor, EquippableItem, UsableItem, Weapon} from "../items/item";
import {TileMap} from "../world";
import {Character} from "../character";
import {Chain} from "../common";
import {PlayerController} from "../control";
import {WEAPONS} from "../items/weapons";
import {ARMOR} from "../items/armors";

let tgf: TestGameFactory;
let gm: GameMaster;
let pc: PlayerController;
let player: Character;
let npc: Character;
let map: TileMap;
let treasurer: Treasurer;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player, gm, map} = tgf.testGameData());
    treasurer = gm.treasurer;
    npc = tgf.testMonster();
    pc = new PlayerController(player, gm);
});

test("Treasurer automatically created", () => {
    expect(treasurer).toBeTruthy();
});

describe("Loot table", () => {
    test("createLoot", () => {
        treasurer.lootTable = {
            "test": 1,
        }
        let item = treasurer.createLoot();
        expect(item).toBeTruthy();
    });

    test("createLootFromTable", () => {
        let item = treasurer.createLootFromTable({
            "test": 1,
        });
        expect(item).toBeTruthy();
    });

    test("'nothing' always generates null", () => {
        let item = treasurer.createLootFromTable({
            "nothing": 1,
        });
        expect(item).toBeNull();
    });

    test("Nonexistent loot categories panic", () => {
        expect( () => {
            treasurer.createLootFromTable({
                "barf": 1000,
            });
        }).toThrow(/barf/);
    });
});

describe("Loot categories", () => {
    test("Potion", () => {
        let item = treasurer.createLootFromTable({
            "potion": 1000,
        });
        expect(item).toBeTruthy();
        expect(item).toBeInstanceOf(UsableItem);
    });

    test("Armor", () => {
        let item = treasurer.createLootFromTable({
            "armor": 1000,
        });
        expect(item).toBeTruthy();
        expect(item).toBeInstanceOf(Armor);
    });

    test("Weapon", () => {
        let item = treasurer.createLootFromTable({
            "weapon": 1000,
        });
        expect(item).toBeTruthy();
        expect(item).toBeInstanceOf(Weapon);
    });

    test("PlusArmor", () => {
        let item = treasurer.createLootFromTable({
            "plusarmor": 1000,
        });
        expect(item).toBeTruthy();
        expect(item).toBeInstanceOf(Armor);

        let originalAc = player.calcDefense(Chain.ac).value;
        player.receiveItem(item);
        expect(gm.equipItem(player, item)).toBeTruthy();
        let expectedAc = originalAc + (item as Armor).ac + 1
        expect(player.calcDefense(Chain.ac).value).toStrictEqual(expectedAc);

        expect(item.displayName).toMatch(/\+1 /);
    });

    test("PlusWeapon", () => {
        let item = treasurer.createLootFromTable({
            "plusweapon": 1000,
        });
        expect(item).toBeTruthy();
        expect(item).toBeInstanceOf(Weapon);
        player.receiveItem(item);
        expect(gm.equipItem(player, item)).toBeTruthy();
        tgf.nextTurn();

        gm.fixRolls(9, 1);  // would normally miss, but weapon is +1
        expect(pc.bump({x: 1, y: 1})).toBeTruthy();
        expect(npc.hp).toStrictEqual(9998);  // rolled 1, weapon +1

        expect(item.displayName).toMatch(/\+1 /);
    });

    test("PlusFRW", () => {
        let item = treasurer.createLootFromTable({
            "plus_frw": 1000,
        });
        expect(item).toBeTruthy();
        expect(item).toBeInstanceOf(Armor);

        player.receiveItem(item);
        expect(gm.equipItem(player, item)).toBeTruthy();

        let fort = player.calcDefense(Chain.fort).value;
        let ref = player.calcDefense(Chain.ref).value;
        let will = player.calcDefense(Chain.will).value;

        let most = Math.max(fort, ref, will);

        expect(most).toStrictEqual(11);

        expect(item.displayName).toMatch(/\+1 /);
    });

    test("Gloves", () => {
        let item = treasurer.createLootFromTable({
            "gloves": 1000,
        });
        expect(item).toBeTruthy();
        expect(item).toBeInstanceOf(Armor);
        player.receiveItem(item);
        expect(gm.equipItem(player, item)).toBeTruthy();

        let sum = 0;
        for(let stat of ['str', 'dex', 'con', 'int', 'wis', 'cha']) {
            sum += player.statBonus(stat);
        }
        expect(sum).toBeGreaterThan(0);
        expect(item.displayName).toMatch(/\+\d /);
    });

    test("Rings", () => {
        let item = treasurer.createLootFromTable({
            "ring": 1000,
        }) as EquippableItem;
        expect(item).toBeTruthy();
        expect(item).toBeInstanceOf(Armor);
        player.receiveItem(item);
        expect(gm.equipItem(player, item)).toBeTruthy();

        expect(item.slots).toHaveLength(2);
        expect(item.slots).toContain("ring1");
        expect(item.slots).toContain("ring2");
        expect(item.displayName).toMatch(/\+\d /);
        expect(item.passiveEffects.length).toBeGreaterThan(0);
    })
});

describe("Dropping items", () => {

    test("Happy Path", () => {
        let loc = {x: 7, y: 3}
        let item = treasurer.dropLootFromTable(loc, {
            "potion": 1000,
        });
        expect(item).toBeTruthy();
        let tile = map.tileAt(loc);
        expect(tile.contents).toHaveLength(1);
        expect(tile.contents[0]).toBe(item);
    });

    test("Nothing doesn't drop anything", () => {
        let loc = {x: 7, y: 3}
        let item = treasurer.dropLootFromTable(loc, {
            "nothing": 1000,
        });
        expect(item).toBeFalsy();
        let tile = map.tileAt(loc);
        expect(tile.contents).toHaveLength(0);
    });

    test("On death", () => {
        treasurer.lootTable = {
            "test": 1,
        };
        let npc = tgf.basicTestMonster();
        let tile = map.tileAt(npc.loc);
        expect(tile.contents).toHaveLength(0);

        treasurer.creatureDied(npc);
        tile = map.tileAt(npc.loc);
        expect(tile.contents).toHaveLength(1);
    });
});

describe("Sanity: Droppability", () => {
    test("All weapons can drop", () => {
        for (const [k, v] of Object.entries(WEAPONS)) {
            if(k === "ImprovisedWeapon") continue;
            expect(ALL_DROPS.weapons).toHaveProperty(k);
        }
    });

    test("All dropped weapons are real", () => {
        for(const [k, v] of Object.entries(ALL_DROPS.weapons)) {
            expect(WEAPONS).toHaveProperty(k);
        }
    });

    test("All armors can drop", () => {
        for (const [k, v] of Object.entries(ARMOR)) {
            expect(ALL_DROPS.armors).toHaveProperty(k);
        }
    });

    test("All dropped armors are real", () => {
        for(const [k, v] of Object.entries(ALL_DROPS.armors)) {
            expect(ARMOR).toHaveProperty(k);
        }
    });

});

test("Correct rangedness", () => {
    for (const [k, v] of Object.entries(WEAPONS)) {
        if(k === "ImprovisedWeapon") continue;
        if(k === "Sling" || k.indexOf("bow") >= 0) {
            expect(v.isRanged).toBeTruthy();
        } else {
            expect(v.isRanged).toBeFalsy();
        }
    }
});

describe("Tiering up", () => {
    beforeEach(() => {
        expect(treasurer.adoptNextTier()).toBeTruthy();
    });

    test("Adopts next tier's loot table", () => {
        expect(treasurer.lootTable).toBe(masterLootTableByTier[1]);
    });

    test("Make and wear 1000 magic items", () => {
        let attempts = 1000;
        while(attempts > 0) {
            let item = treasurer.createLootFromTable({
                "plusarmor": 1000,
                "plusweapon": 1000,
                "plus_frw": 1000,
                "gloves": 1000,
                "ring": 1000,
            }) as EquippableItem;
            attempts -= 1;
            expect(item).toBeTruthy();
            expect(item.displayName).toMatch(/\+\d /);
            expect(item.passiveEffects.length).toBeGreaterThan(0);
            player.receiveItem(item);
            expect(gm.equipItem(player, item)).toBeTruthy();
            gm.unequipItem(player, item.slots[0]);
        }
    });

    test("Next tier can give +2 items", () => {
        let attempts = 1000;
        let item = null;
        while(attempts > 0) {
            item = treasurer.createLootFromTable({
                "plusarmor": 1000,
                "plusweapon": 1000,
                "plus_frw": 1000,
                "gloves": 1000,
                "ring": 1000,
            });
            attempts -= 1;

            if(item.displayName.match(/\+2 /)) {
                break;
            }
        }
        if(attempts > 0) {
            let originalAc = player.calcDefense(Chain.ac).value;
            expect(item.displayName).toMatch(/\+2 /);
            player.receiveItem(item);
            expect(gm.equipItem(player, item)).toBeTruthy();
            if((item as EquippableItem).slots.includes("body")) {
                let expectedAc = originalAc + (item as Armor).ac + 2
                expect(player.calcDefense(Chain.ac).value).toStrictEqual(expectedAc);
            }
        } else {
            fail("Over 1000 loot drops did not result in +2");
        }
    });

    test("Tiering up beyond existing tables supported", () => {
        expect(treasurer.adoptNextTier()).toBeTruthy();
        expect(treasurer.tierNum).toStrictEqual(3);
        expect(treasurer.lootTable).toBe(masterLootTableByTier[1]);
        expect(treasurer.adoptNextTier()).toBeTruthy();
        expect(treasurer.tierNum).toStrictEqual(4);
        expect(treasurer.lootTable).toBe(masterLootTableByTier[1]);
    });

    test("Setting tier to nonexistent table supported", () => {
        treasurer = new Treasurer(gm, 7000);
        expect(treasurer.tierNum).toStrictEqual(7000);
        expect(treasurer.lootTable).toBe(masterLootTableByTier[1]);
        expect(treasurer.adoptNextTier()).toBeTruthy();
        expect(treasurer.tierNum).toStrictEqual(7001);
        expect(treasurer.lootTable).toBe(masterLootTableByTier[1]);
    });
});