import * as ROT from 'rot-js';
import {ICharacterTemplate, Stats} from "../interfaces";
import {IConditionTemplate} from "../effects/condition";
import {Chain, Duration} from "../common";

export interface IVariantTemplate extends Partial<ICharacterTemplate> {
    variantPrefix: string;

    baseMaxHpMultiplier?: number;
    baseAcBonus?: number;
    variantBossName?: string;
    variantStatBonuses?: Partial<Stats>;
    variantAddConditions?: IConditionTemplate[];
}

export const VARIANTS: {[key:string]: IVariantTemplate } = {
    Zombie: {
        variantPrefix: 'Zombie',
        baseMaxHpMultiplier: 1.25,

        fgcolor: ROT.Color.fromString("#6ba353"),  // off green
        baseMovementSpeed: 3,
        behaviorName: "MeleeCombatBehavior",
        abilityNames: ["BasicMeleeAttack"],
        defaultMeleeAbilityName: "BasicMeleeAttack",
    },
    Feral: {
        variantPrefix: 'Feral',
        baseMovementSpeed: 8,
        baseMaxHp: 1,
        hpPerLevel: 0,
        fgcolor: ROT.Color.fromString("#fe2f4a"),  // lightish red
    },
    BossFighter: {
        variantPrefix: 'Stalwart',
        variantBossName: 'Fighter Moxie',
        baseAcBonus: 2,
    },
    BossWizard: {
        variantPrefix: 'Arcane',
        variantBossName: 'Wizard Zankar',
        variantStatBonuses: {int: 2},
        variantAddConditions: [{
            name: "ArcaneBossBlessing",
            passiveEffectTemplates: [{
                name: 'DefenseEffect',
                defense: Chain.will,
                amount: 1,
            }],
            duration: Duration.AlwaysOn,
        }],
    },
    BossCleric: {
        variantPrefix: 'Pious',
        variantBossName: 'Cleric Llynmir',
        variantStatBonuses: {wis: 2},
        variantAddConditions: [{
            name: "PiousBossBlessing",
            passiveEffectTemplates: [{
                name: 'DefenseEffect',
                defense: Chain.fort,
                amount: 1,
            }],
            duration: Duration.AlwaysOn,
        }],
    },
    BossRogue: {
        variantPrefix: 'Agile',
        variantBossName: 'Rogue Jayne',
        variantStatBonuses: {dex: 2},

        baseMovementSpeed: 6,
    },
}

export function applyVariant(srcTemplate: ICharacterTemplate, variant: IVariantTemplate): ICharacterTemplate {
    let result = JSON.parse(JSON.stringify(srcTemplate));
    let copyVar = JSON.parse(JSON.stringify(variant));

    Object.assign(result, copyVar);

    // Apply prefixes:
    let name = result.displayName || result.name;
    result.name = `${copyVar.variantPrefix} ${name}`;

    // Apply multipliers:
    if(copyVar.baseMaxHpMultiplier) {
        result.baseMaxHp = Math.floor(result.baseMaxHp * copyVar.baseMaxHpMultiplier);
    }

    // Apply flat bonuses:
    if(copyVar.baseAcBonus) {
        if(!result.baseAc) result.baseAc = 10;
        result.baseAc += copyVar.baseAcBonus;
    }

    // Apply stat bonuses:
    if(copyVar.variantStatBonuses) {
        if(!result.stats) {
            result.stats = {
                str: 10,
                dex: 10,
                con: 10,
                int: 10,
                wis: 10,
                cha: 10,
            }
        }
        for(let statName of Object.keys(copyVar.variantStatBonuses)) {
            result.stats[statName] += copyVar.variantStatBonuses[statName];
        }
    }

    // Add to existing lists
    if(copyVar.variantAddConditions) {
        if(!result.conditions) result.conditions = [];
        result.conditions.push(...copyVar.variantAddConditions);
    }

    return result;
}
