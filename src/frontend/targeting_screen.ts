import * as ROT from 'rot-js';

import {State} from "./statemachine";
import {Point, Targets} from "../backend/common";
import {PlayerController} from "../backend/control";
import {Ability} from "../backend/abilities/ability";
import {Character} from "../backend/character";
import {Action, actionToDirection} from "./keybinds";
import {makeRibbonInfo, RibbonComponent} from "./components";
import {MapDrawer} from "./common";
import {XY} from "../backend/interfaces";
import {GameMaster} from "../backend/gamemaster";

export class TargetingState extends State {
    public targetLoc: XY;

    protected player: Character;
    protected ribbon: RibbonComponent;
    protected gm: GameMaster;

    protected lastTarget: Character;

    constructor(protected pc: PlayerController,
                protected ability: Ability,
                protected mapDraw: MapDrawer,
    ) {
        super();
        this.player = pc.player;
        this.gm = mapDraw.gm;
        this.lastTarget = null;
        this.targetLoc = this.determineDefaultTargetLoc();
        this.ribbon = new RibbonComponent(this, [
            makeRibbonInfo(Action.Cancel, 'quit'),
            makeRibbonInfo(Action.Confirm, 'select'),
        ]);
        this.addComponent(this.ribbon);
    }

    public activate(prev: State) {
        super.activate(prev);
        this.ribbon.move({x: 0, y: this.driver.height - 1});
        this.targetLoc = this.determineDefaultTargetLoc();
    }

    public setAbility(what: Ability) {
        this.ability = what;
        this.targetLoc = this.determineDefaultTargetLoc();
    }

    protected determineDefaultTargetLoc(): XY {
        let result;

        if(this.lastTarget && this.lastTarget.alive) {
            return this.lastTarget.loc;
        }

        let closest = this.gm.closestEnemyTo(this.player);
        if(!closest || this.ability?.targeting === Targets.Move) {
            result = Point.fromLoc(this.player.loc)
        } else {
            result = closest.loc;
        }

        return result;
    }

    public handleInput(action: Action, ev: KeyboardEvent): void {
        super.handleInput(action, ev);
        let dir = actionToDirection(action);
        let mult = ev.shiftKey ? 5 : 1;
        if(dir) {
            let newTarget = {
                x: this.targetLoc.x + dir.x * mult,
                y: this.targetLoc.y + dir.y * mult
            };
            this.targetLoc = newTarget;
            return;
        }

        switch(action) {
            case Action.Cancel:
                this.targetLoc = null;
                this.driver.pop();
                break;
            case Action.Confirm:
            case Action.DefaultRangedScreen:
                let who = this.gm.map.tileAt(this.targetLoc);
                this.lastTarget = who.character;
                this.driver.pop();
                break;
        }
    }

    public mouseMove(loc: XY) {
        super.mouseMove(loc);
        if(loc.x < 0 || loc.y < 0) {
            this.targetLoc = this.player.loc;
        } else {
            this.targetLoc = this.mapDraw.screenCoordsToMap(loc, this.player.loc);
        }
    }

    public mouseUp(loc: XY) {
        this.driver.pop();
    }

    public render(display: ROT.Display) {
        this.mapDraw.drawEntireMap(display, this.player.loc);
        let verb = "On this spot is: ";
        if(this.ability) {
            verb = "Targeting: "
            let dispAbility = this.ability.displayName || this.ability.name;
            display.drawText(0, 0, `Select a target for ${dispAbility}`);

            // Cursor
            let targetable = this.pc.canCenterAbility(this.ability, this.targetLoc);
            let cursorColor = targetable ? "#888" : "#f00";
            if(this.ability.radius) {
                let radius = this.ability.radius;
                let where = this.targetLoc;
                for(let dx=-radius; dx <= radius; dx++) {
                    for(let dy=-radius; dy <= radius; dy++) {
                        let there = {x: where.x + dx, y: where.y + dy};
                        this.mapDraw.drawTileAt(display, there, this.player.loc,
                            cursorColor);
                    }
                }
            } else {
                this.mapDraw.drawTileAt(display, this.targetLoc, this.player.loc,
                    cursorColor);
            }
        } else {
            this.mapDraw.drawTileAt(display, this.targetLoc, this.player.loc, "#888");
        }
        display.drawText(0, 1, `${verb} ${this.tileDescription(this.targetLoc)}`);
        super.render(display);
    }

    protected tileDescription(loc: XY): string {
        let tile = this.mapDraw.tileAt(loc);

        if(!tile.lit) {
            return "Darkness";
        }

        if(tile.character) {
            let display = tile.character.displayName || tile.character.name;
            return `A ${display}`;
        }

        let headline = tile.contents[0];
        if(!headline) {
            switch(tile.char) {
                case '.':
                    return "A stone floor";
                case '#':
                    return "A stone wall";
                case "+":
                    return "The door to a challenge room";
                case ">":
                    return "Stairs down";
                default:
                    return "A bug!";
            }
        }
        let more = tile.contents.length - 1;
        let moreStr = more > 0 ? `(+${more} more)` : '';
        return `${headline.displayName} ${moreStr}`;
    }
}