import * as ROT from 'rot-js';
import {State} from "./statemachine";
import {ICharacterTemplate} from "../backend/interfaces";
import {ListComponent, makeRibbonInfo, RibbonComponent, SimpleWrapper, TextWrapper} from "./components";
import {BOSSES} from "../backend/classes";
import {Action} from "./keybinds";
import {PlayerController} from "../backend/control";
import {Campaign} from "../backend/campaign";

const BOSS_DESCRIPTIONS: {[key:string]: string} = {
    "Fighter Moxie": "A deranged dwarf whose mental state has only " +
        "deteriorated from his time in the Labyrinth, Moxie will fight "+
        "to the death to keep his place.",
    "Wizard Zankar": "His actions a mystery even to himself, Zankar "+
        "has dedicated himself to solving the mysteries of the Labyrinth. "+
        "By force, if necessary.",
    "Cleric Llynmir": "Saint Llynmir has is a vessel of her God, one "+
        "which seeks to claim the Labyrinth's power for itself.  She "+
        "will brook no tresspass.",
    "Rogue Jayne": "A shadowy thief who snuck into the Labyrinth to "+
        "steal its power, she is now as trapped as those she sought to "+
        "steal from.  This won't stop her from trying to eliminate "+
        "the competition, however.",
}

export class ChallengeScreen extends State {
    public ready: boolean;
    public chosen: string;

    protected bossList: ListComponent<TextWrapper>;
    protected ribbon: RibbonComponent;

    constructor(protected pc: PlayerController, protected campaign: Campaign) {
        super();

        this.ready = false;
        this.chosen = null;
        let wrapped = TextWrapper.
            map(Object.keys(BOSSES)).
            filter(b => pc.canChallengeBoss(b.displayName));
        this.bossList = new ListComponent<TextWrapper>(this, wrapped, {x: 2, y: 2});
        this.addComponent(this.bossList);

        this.ribbon = new RibbonComponent(this, [
            makeRibbonInfo(Action.Confirm, 'choose'),
            makeRibbonInfo(Action.Cancel, 'cancel'),
            makeRibbonInfo(Action.North, 'up'),
            makeRibbonInfo(Action.South, 'down'),
        ]);
        this.addComponent(this.ribbon);
    }

    public activate(prev: State) {
        super.activate(prev);
        this.ribbon.move({x: 0, y: this.driver.height - 1});
    }

    public handleInput(action: Action, ev: KeyboardEvent) {
        super.handleInput(action, ev);
        switch(action) {
            case Action.Confirm:
                this.ready = true;
                this.chosen = this.bossList.selectedItem.displayName;
                this.driver.pop();
                break;
            case Action.Cancel:
                this.chosen = null;
                this.driver.pop();
                break;
        }
    }

    protected endgameDesc(who: string) {
        let lastBoss = this.pc.gm.tier.oneBossLeft();
        let clsName = BOSSES[who];
        let defeated = this.campaign.isUnlocked(clsName);

        if(lastBoss) {
            if(defeated) {
                return "(You have already unlocked this Tyrant's class)";
            }  else {
                return "(Defeating this Tyrant now will unlock their "+
                    "class for future playthroughs)";
            }
        } else {
            if(defeated) {
                return "(You have already unlocked this Tyrant's "+
                    "class; it is safe to fight them now)";
            } else {
                return "(You have not unlocked this Tyrant's class; "+
                    "fighting them now will prevent you from doing so "+
                    "on this playthrough)";
            }
        }
    }

    public render(display: ROT.Display) {
        super.render(display);
        let chosen = this.bossList.selectedItem.displayName
        let half = this.driver.width / 2;
        this.drawCenter(display, "Challenge a Tyrant", 0);

        let desc = BOSS_DESCRIPTIONS[chosen];
        desc = desc || `No description for ${chosen}, this is a bug`;
        display.drawText(half, 2, desc, half);

        let endgame = this.endgameDesc(chosen);
        endgame = endgame || `No endgame desc for ${chosen}, this is a bug`;
        display.drawText(half, 9, endgame, half);
    }
}
