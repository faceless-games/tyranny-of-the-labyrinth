import * as ROT from 'rot-js';

import {Component, State} from "./statemachine";
import {Action} from "./keybinds";
import {XY} from "../backend/interfaces";

export interface Listable {
    displayName: string;
    fgcolor?: string;
    disabled?: boolean;
}

type ToStringFn = (item:any) => string;

export class SimpleWrapper implements Listable {
    protected displayFunc: ToStringFn;

    public static map(collection: any[], displayFunc: ToStringFn): SimpleWrapper[] {
        return collection.map(c => new SimpleWrapper(c, displayFunc));
    }

    constructor(public wrapped: any,displayFunc: ToStringFn) {
        this.displayFunc = displayFunc;
    }

    public get displayName():string {
        return this.displayFunc(this.wrapped);
    }
}

export class TextWrapper implements Listable {
    public disabled: boolean;

    public static map(collection: string[]): TextWrapper[] {
        return collection.map(c => new TextWrapper(c));
    }
    constructor(public displayName: string) {
        this.disabled = false;
    }
}

export class ListComponent<T extends Listable> extends Component {
    public selectedIndex: number;
    public maxDisplayedItems: number;

    protected topOffset: number;

    constructor(parent: State, protected items: Array<T>, where: XY) {
        super(parent, where);
        this.selectedIndex = 0;
        this.maxDisplayedItems = 20;
        this.topOffset = 0;
    }

    public handleInput(action: Action, ev: KeyboardEvent): void {
        if (action == Action.North) {
            this.selectIndex(this.selectedIndex-1);
        } else if (action == Action.South) {
            this.selectIndex(this.selectedIndex + 1);
        }
    }

    public render(display: ROT.Display) {
        let displayed = 0;
        for(let offset=0; offset < this.items.length && displayed < this.maxDisplayedItems; offset++) {
            let item = this.items[offset + this.topOffset];
            let textPrefix = "";
            let preferredColor = item.fgcolor || "#FFF";
            let fg = item.disabled ? "#888" : preferredColor;
            let bg = "";
            if(offset + this.topOffset === this.selectedIndex) {
                bg = fg;
                fg = "#000";
            }
            textPrefix = `%b{${bg}}%c{${fg}}`;
            display.drawText(
                this.loc.x,
                this.loc.y+offset,
                textPrefix+item.displayName
            );
            displayed+=1;
        }
    }

    public get selectedItem(): T {
        return this.items[this.selectedIndex];
    }

    public refresh() {
        this.selectIndex(this.selectedIndex);
    }

    protected selectIndex(newIndex?: number) {
        let len = this.items.length;
        if(newIndex === undefined) newIndex = 0;
        let sel = ROT.Util.mod(newIndex, len);
        this.selectedIndex = sel;

        if(len <= this.maxDisplayedItems) return;
        let center = Math.floor(this.maxDisplayedItems / 2);
        if(sel <= center) {
            this.topOffset = 0;
            return;
        } else if(sel >= len - center) {
            this.topOffset = len - this.maxDisplayedItems;
        } else {
            this.topOffset = sel - center;
        }
    }
}

export interface RibbonInfo {
    action: Action;
    text: string;
    disabled?: boolean;
}

export function makeRibbonInfo(action: Action, text: string, disabled?: boolean): RibbonInfo {
    return {
        action, text, disabled
    }
}

export class RibbonComponent extends Component {
    protected infoByAction: {[index:number] : RibbonInfo}

    constructor(parent: State, protected cmds: RibbonInfo[], where= {x:0, y: 0}) {
        super(parent, where);
        this.infoByAction = {};
        for(let cmd of cmds) {
            this.infoByAction[cmd.action] = cmd;
        }
    }

    public handleInput(action: Action, ev: KeyboardEvent): void {
        return;
    }

    protected getKey(action: Action) {
        return this.parent.driver.keybinds.actionToFirstKey(action);
    }

    public setEnabled(action: Action, enable = true) {
        this.infoByAction[action].disabled = !enable;
    }


    public render(display: ROT.Display) {
        let resultList = this.cmds.map((item) => {
            let prefix = item.disabled ? "%c{#888}" : "";
            let suffix = item.disabled ? "%c{}": "";
            return `${prefix}${this.getKey(item.action)}: ${item.text}${suffix}`;
        });
        let ribbon = resultList.join(" ");
        display.drawText(this.loc.x, this.loc.y, ribbon);
    }
}
