import * as ROT from 'rot-js';
import {State} from "./statemachine";
import {ListComponent, makeRibbonInfo, RibbonComponent, TextWrapper} from "./components";
import {Action} from "./keybinds";
import {PowerSelectScreen} from "./power_select_screen";
import {BOSSES, CLASSES} from "../backend/classes";
import {ABILITIES, IAbilityTemplate} from "../backend/abilities/ability";
import {Character} from "../backend/character";
import {PASSIVES} from "../backend/effects/passive_powers";
import {PassiveSelectScreen} from "./passive_select_screen";
import {Condition} from "../backend/effects/condition";
import {StatIncreaseState} from "./stat_increase_screen";

export class BossKillScreen extends State {
    protected menu: TextWrapper[];
    protected menuList: ListComponent<TextWrapper>;
    protected ribbon: RibbonComponent;

    constructor(protected character: Character, protected bossName: string, protected tier: number) {
        super();
        this.menu = [
            `Gain one of ${bossName}'s powers`,
            "Gain a passive power",
            "Increase a stat (str, dex, con, int, wis, or cha)",
            "Nothing: Forfeit this chance, knowing the choice is irreversible"
        ].map(s => new TextWrapper(s));
        this.menuList = new ListComponent<TextWrapper>(this, this.menu, {x: 1, y: 8});
        this.addComponent(this.menuList);

        this.ribbon = new RibbonComponent(this, [
            makeRibbonInfo(Action.Confirm, 'choose'),
            makeRibbonInfo(Action.North, 'up'),
            makeRibbonInfo(Action.South, 'down'),
        ]);
        this.addComponent(this.ribbon);
    }

    public activate(prev: State) {
        super.activate(prev);
        this.ribbon.move({x: 0, y: this.driver.height - 1});
    }

    render(display: ROT.Display) {
        super.render(display);

        let txt = `You have defeated ${this.bossName}!`
        display.drawText(2, 1, txt);

        txt = "Their power will soon be dispersed throughout the Labyrinth, " +
            "rendering the other tyrants and their minions that much more "+
            "difficult.  However, you too can gain a portion:";
        display.drawText(2, 3, txt, this.driver.width - 4);

    }

    public handleInput(action: Action, ev: KeyboardEvent) {
        super.handleInput(action, ev);
        if(action === Action.Confirm) {
            switch (this.menuList.selectedIndex) {
                case 0:
                    return this.chooseBossPower();
                case 1:
                    return this.choosePassivePower();
                case 2:
                    return this.chooseStatIncrease();
                case 3:
                    this.driver.pop();
            }
        }
    }

    public async chooseBossPower() {
        let bossTemplateName = BOSSES[this.bossName];
        let bossTemplate = CLASSES[bossTemplateName];
        let abilityNames = bossTemplate.classAbilityNames;
        let templates = abilityNames.map((name) => {
            return ABILITIES.templateHolderFromName(name).template as IAbilityTemplate;
        });
        templates = templates.filter((t) => {
            let alreadyHave = this.character.abilityByName(t.name);
            return (
                t.tier <= this.tier &&
                !alreadyHave
            )
        });
        let powerScreen = new PowerSelectScreen(templates, "Choose a power to steal");
        await this.driver.push(powerScreen);

        if(powerScreen.selectedPower) {
            this.character.gainAbilityFromTemplate(powerScreen.selectedPower);
        }
        if(powerScreen.ready) {
            this.driver.pop();
        }
    }

    public async choosePassivePower() {
        let templates = Object.values(PASSIVES).filter((p) => {
            return !this.character.hasPassiveNamed(p.name);
        })
        let passiveScreen = new PassiveSelectScreen(templates);
        await this.driver.push(passiveScreen);

        if(passiveScreen.selectedPassive) {
            this.character.gainPassive(new Condition(passiveScreen.selectedPassive));
            this.driver.pop();
        }
    }

    public async chooseStatIncrease() {
        let statScreen = new StatIncreaseState(this.character);
        await this.driver.push(statScreen);
        if(statScreen.ready) {
            this.character.stats[statScreen.chosenStat] += 1;
            this.driver.pop();
        }
    }
}