import * as ROT from 'rot-js';

import {Character} from "../backend/character";
import {Listable, ListComponent, makeRibbonInfo, RibbonComponent} from "./components";
import {State} from "./statemachine";
import {Action} from "./keybinds";
import {Stats} from "../backend/interfaces";
import {CLASSES} from "../backend/classes";


export const statToDesc: {[key: string]: string} = {
    'str': "Strength determines your ability to hit and do damage " +
        "in melee",
    'dex': "Dexterity determines your ability to hit and do " +
        "damage with ranged attacks, as well as improving " +
        "your ability to dodge.",
    'con': "Constitution improves your health and defense against "+
        "fortitude attacks.",
    'int': "Intelligence generally improves magical ability",
    'wis': "Wisdom assists divine powers and improves your "+
        "defense against some magical attacks.",
    'cha': "Charisma is a 'catchall' stat, which can improve "+
        "specific abilities and defenses.",
};

export const statToClasses: {[key: string]: string} = {
    'str': "Strength is of use to any melee class, but especially:",
    'dex': "Dexterity is of use to ranged attacks:",
    'con': "Constitution is useful for all classes, but especially:",
    'int': "Intelligence is useful for magic classes:",
    'wis': "Wisdom is useful for divine classes:",
    'cha': "Charisma is useful for:",
}

export function classesForStat(stat: string): string {
    let result = [];
    for(let className of Object.keys(CLASSES)) {
        let cls = CLASSES[className];
        if(cls.classStatFocuses.includes(stat)) {
            result.push(className);
        }
    }
    return result.join("\n");
}

function canIncreaseOrDecrease(who: Character, statName: string) {
    let increase = who.canBuyStat(statName, 1);
    let decrease = who.canBuyStat(statName, -1);

    return {increase, decrease}
}

class StatWrapper implements Listable {
    constructor(public statName: string, public char: Character) {
    }

    public get displayName(): string {
        let statName = this.statName;
        let stat = this.char.stats[statName];
        let {increase, decrease} = canIncreaseOrDecrease(this.char, this.statName);
        let incr = increase ? ">": " ";
        let decr = decrease ? "<": " ";
        let statStr = stat < 10 ? ` ${stat}` : `${stat}`;
        let bonus = this.char.statBonus(statName);
        let bonusStr = bonus >= 0 ? `+${bonus}` : `${bonus}`;
        let cost = this.char.pointCostAt(stat);
        return `${statName}: ${decr}${statStr}${incr}  ${bonusStr}     ${cost}`;
    }
}

export class PointBuyState extends State {
    protected statList: ListComponent<StatWrapper>;
    protected ribbon: RibbonComponent;

    public ready: boolean;

    constructor(protected char: Character) {
        super();

        this.ready = false;
        let wrapped = [];
        for(let statName in this.char.stats) {
            wrapped.push(new StatWrapper(statName, this.char));
        }
        this.statList = new ListComponent<StatWrapper>(this, wrapped, {x: 2, y: 3});
        this.addComponent(this.statList);

        this.ribbon = new RibbonComponent(this, [
            makeRibbonInfo(Action.Confirm, 'done'),
            makeRibbonInfo(Action.Cancel, 'cancel'),
            makeRibbonInfo(Action.North, 'up'),
            makeRibbonInfo(Action.South, 'down'),
            makeRibbonInfo(Action.East, 'increase'),
            makeRibbonInfo(Action.West, 'decrease'),
        ]);
        this.addComponent(this.ribbon);
    }

    public activate(prev: State) {
        super.activate(prev);
        this.ribbon.move({x: 0, y: this.driver.height - 1});
    }

    public handleInput(action: Action, ev: KeyboardEvent) {
        super.handleInput(action, ev);
        switch(action) {
            case Action.Confirm:
                this.ready = true;
                this.driver.pop();
                break;
            case Action.Cancel:
                this.driver.pop();
                break;
            case Action.East:
                this.buyStat(1);
                break;
            case Action.West:
                this.buyStat(-1);
                break;
        }
    }

    public buyStat(direction = 1) {
        let wrapped = this.statList.selectedItem;
        if(this.char.canBuyStat(wrapped.statName, direction)) {
            this.char.buyStat(wrapped.statName, direction);
        }
    }

    public render(display: ROT.Display) {
        let statName = this.statList.selectedItem.statName;
        let {increase, decrease} = canIncreaseOrDecrease(this.char, statName);
        this.ribbon.setEnabled(Action.East, increase);
        this.ribbon.setEnabled(Action.West, decrease);

        super.render(display);

        display.drawText(6, 2, "Score Bonus Cost to increase");

        let pointBuyDesc = `You have ${this.char.points} point(s) ` +
            "to distribute between your stats.  Higher stat numbers will cost " +
            "more points, but you can also gain points by lowering stats.";
        display.drawText(2, 11, pointBuyDesc, 38);

        let wrapped = this.statList.selectedItem;
        let desc = statToDesc[wrapped.statName];
        display.drawText(2, 17, desc, 38);

        let prologue = statToClasses[wrapped.statName];
        display.drawText(41, 2, prologue, 38);

        display.drawText(41, 5, classesForStat(wrapped.statName));
    }
}