import * as ROT from 'rot-js';
import {State} from "./statemachine";
import {Action} from "./keybinds";

export class GameOverState extends State {

    render(display: ROT.Display): void {
        let halfWidth = Math.floor(this.driver.width / 2);
        let halfHeight = Math.floor(this.driver.height / 2);
        let gameover = "G A M E    O V E R";

        display.drawText(
            halfWidth - gameover.length / 2,
            halfHeight,
            gameover
        );

        let confirmKey = this.driver.keybinds.actionToFirstKey(Action.Confirm);
        let txt = `Press ${confirmKey} to return to the title`;
        display.drawText(1, this.driver.height - 1, txt);
    }

    public handleInput(action: Action, ev: KeyboardEvent) {
        super.handleInput(action, ev);
        if(action == Action.Confirm || action == Action.Cancel) {
            this.driver.pop();
        }
    }

}