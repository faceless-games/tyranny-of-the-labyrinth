import * as ROT from 'rot-js';

import {State} from "./statemachine";
import {Ability} from "../backend/abilities/ability";
import {ListComponent, makeRibbonInfo, RibbonComponent} from "./components";
import {Character} from "../backend/character";
import {GameMaster} from "../backend/gamemaster";
import {Action} from "./keybinds";
import {Targets} from "../backend/common";


export class AbilitiesState extends State {
    public allAbilities: Array<Ability>;

    protected abilityList: ListComponent<Ability>;
    protected ribbon: RibbonComponent;

    constructor(protected char: Character, protected gm: GameMaster) {
        super();
        this.allAbilities = char.abilities;

        this.abilityList = new ListComponent<Ability>(this, this.allAbilities, {x: 1, y: 3});
        this.addComponent(this.abilityList);
        this.ribbon = new RibbonComponent(this, [
            makeRibbonInfo(Action.Cancel, 'quit'),
            makeRibbonInfo(Action.AssignDefaultMelee, 'assign melee'),
            makeRibbonInfo(Action.AssignDefaultRanged, 'assign ranged'),
        ]);
        this.addComponent(this.ribbon);
    }

    public activate(prev: State) {
        super.activate(prev);
        this.ribbon.move({x: 0, y: this.driver.height - 1});
    }

    protected canBeAssignedMelee(what: Ability): boolean {
        return what.targeting == Targets.Melee;
    }

    protected canBeAssignedRanged(what: Ability): boolean {
        return what.needsExplicitTarget();
    }

    public handleInput(action: Action, ev: KeyboardEvent): void {
        super.handleInput(action, ev);
        let selectedAbility = this.abilityList.selectedItem;
        switch(action) {
            case Action.Cancel:
                this.driver.pop();
                break;
            case Action.AssignDefaultMelee:
                if(this.canBeAssignedMelee(selectedAbility)) {
                    this.char.assignDefaultMeleeAbility(selectedAbility);
                }
                break;
            case Action.AssignDefaultRanged:
                if(this.canBeAssignedRanged(selectedAbility)) {
                    this.char.assignDefaultRangedAbility(selectedAbility);
                }
                break;
            case Action.Hotkey0:
            case Action.Hotkey1:
            case Action.Hotkey2:
            case Action.Hotkey3:
            case Action.Hotkey4:
            case Action.Hotkey5:
            case Action.Hotkey6:
            case Action.Hotkey7:
            case Action.Hotkey8:
            case Action.Hotkey9:
                if(action >= Action.Hotkey0 && action <= Action.Hotkey9) {
                    let actionIndex = <number>action - Action.Hotkey0;
                    this.char.hotkeys[actionIndex] = selectedAbility;
                }
        }
    }

    public render(display: ROT.Display) {
        display.drawText(0, 0, "Your abilities:");

        let selectedAbility = this.abilityList.selectedItem;
        if(selectedAbility) {
            this.ribbon.setEnabled(Action.AssignDefaultMelee, this.canBeAssignedMelee(selectedAbility));
            this.ribbon.setEnabled(Action.AssignDefaultRanged, this.canBeAssignedRanged(selectedAbility));
        }

        super.render(display);

        let defaultMelee = this.char.defaultMeleeAbility;
        let defaultRanged = this.char.defaultRangedAbility;
        let half = this.driver.width / 2;
        let afterHotkeys = 0;
        let dispMelee = defaultMelee.displayName || defaultMelee.name;
        display.drawText(half, 1, `Default Melee:  ${dispMelee}`);
        let dispRanged = defaultRanged.displayName || defaultRanged.name;
        display.drawText(half, 2, `Default Ranged: ${dispRanged}`);

        for(let i=0; i < 10; i++) {
            let action = Action.Hotkey0 + i;
            let actionKey = this.driver.keybinds.actionToFirstKey(action);
            if(!actionKey) actionKey = ' ';
            let hotkeyAbility = this.char.hotkeys[i];
            let hotkeyName = hotkeyAbility ? (hotkeyAbility.displayName || hotkeyAbility.name) : "<nothing>";
            display.drawText(half, 4+i, `${actionKey} Hotkey ${i+1}: ${hotkeyName}`);
            afterHotkeys = 6+i;
        }

        if(!selectedAbility) return;
        display.drawText(half, afterHotkeys, selectedAbility.description);
        display.drawText(half, afterHotkeys+3, selectedAbility.techDescription());
    }
}