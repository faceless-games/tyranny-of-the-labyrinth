import * as ROT from 'rot-js';
import {State} from "./statemachine";
import {Listable, ListComponent, makeRibbonInfo, RibbonComponent} from "./components";
import {Character} from "../backend/character";
import {CLASSES} from "../backend/classes";
import {Action} from "./keybinds";
import {ICharacterTemplate} from "../backend/interfaces";
import {Campaign} from "../backend/campaign";
import {displayEquipmentTag, EquipmentTag} from "../backend/common";

const CLASS_TO_COMPLEXITY: {[key: string]: string} = {
    "Adventurer": "*",
    "Fighter": "**",
    "Wizard": "****",
    "Cleric": "***",
    "Rogue": "***",
}

class CharTemplateWrapper implements Listable {
    public static wrap(templates: {[key:string]:ICharacterTemplate}): CharTemplateWrapper[] {
        let wrapped = [];
        for(let className in templates) {
            wrapped.push(new CharTemplateWrapper(templates[className]));
        }
        return wrapped;
    }
    public disabled: boolean;

    constructor(public template:ICharacterTemplate) {
        this.disabled = false;
    }

    public get displayName(): string {
        let display = this.template.displayName || this.template.className;
        return display;
    }

}

export class ClassSelectionScreen extends State {

    protected classList: ListComponent<CharTemplateWrapper>;
    protected ribbon: RibbonComponent;

    public ready: boolean;
    public character: Character;

    constructor(protected campaign: Campaign) {
        super();

        this.ready = false;
        this.character = null;
        this.classList = this.prepareClassList();
        this.addComponent(this.classList);

        this.ribbon = new RibbonComponent(this, [
            makeRibbonInfo(Action.Confirm, 'choose'),
            makeRibbonInfo(Action.Cancel, 'cancel'),
            makeRibbonInfo(Action.North, 'up'),
            makeRibbonInfo(Action.South, 'down'),
        ]);
        this.addComponent(this.ribbon);
    }

    protected prepareClassList() {
        let wrapped = CharTemplateWrapper.wrap(CLASSES);
        for(let w of wrapped) {
            if(!this.campaign.isUnlocked(w.displayName)) {
                w.disabled = w.displayName !== "Adventurer";
            }
        }
        return new ListComponent<CharTemplateWrapper>(this, wrapped, {x: 2, y: 2});
    }

    public activate(prev: State) {
        super.activate(prev);
        this.ribbon.move({x: 0, y: this.driver.height - 1});
    }

    public handleInput(action: Action, ev: KeyboardEvent) {
        super.handleInput(action, ev);
        switch(action) {
            case Action.Confirm:
                if(!this.makeCharacter()) return;
                this.driver.pop();
                break;
            case Action.Cancel:
                this.driver.pop();
                break;
        }
    }

    public render(display: ROT.Display) {
        super.render(display);
        let wrapped = this.classList.selectedItem;
        let x = this.driver.width / 3;
        let t = wrapped.template;

        display.drawText(x, 2, t.description, this.driver.width - x);

        let y = 10;
        display.drawText(x, y, `Hp Per Level: ${t.hpPerLevel}`);

        let statstr = t.classStatFocuses.join(", ");
        if(t.classStatFocuses.length === 6) {
            statstr = "All"
        }
        display.drawText(x, y+2, "Stats important to this class:");
        display.drawText(x+2, y+3, statstr);

        let complexity = CLASS_TO_COMPLEXITY[t.name];
        display.drawText(x, y+5, `Complexity: ${complexity}`);

        let proficiencies = t.proficiencies.map(t => displayEquipmentTag(t)).join(", ");
        display.drawText(x, y+7, `Proficient with: ${proficiencies}`);
    }

    protected makeCharacter(): boolean {
        let item = this.classList.selectedItem;
        if(item.disabled) return false;
        let template = item.template;
        this.character = new Character(template);
        this.ready = true;
        return true;
    }
}