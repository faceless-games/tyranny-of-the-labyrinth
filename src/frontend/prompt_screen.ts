import * as ROT from 'rot-js';
import {State} from "./statemachine";

export class PromptScreen extends State {
    public result: string

    constructor(public prompt: string, public maxLen = 20) {
        super();
        this.result = '';
    }

    public rawKey(ev: KeyboardEvent) {
        super.rawKey(ev);
        let c = ev.key;
        if (c.length === 1 && this.result.length <= this.maxLen) {
            this.result += c;
        }
        if (c === 'Backspace' || c === 'Delete') {
            this.result = this.result.slice(0, -1);
        }

        if (c === 'Escape' || c === 'Esc' || c === 'Cancel') {
            this.result = null;
            this.driver.pop();
            return;
        }
        if (c === 'Enter' || c === 'Accept') {
            this.driver.pop();
            return;
        }
    }

    render(display: ROT.Display) {
        super.render(display);

        display.drawText(5, 1, this.prompt);

        let resultX = 5
        if(this.result) {
            display.drawText(resultX, 3, this.result);
            resultX += this.result.length;
        }
        display.draw(resultX, 3, ' ', "#000", "#FFF");
    }
}