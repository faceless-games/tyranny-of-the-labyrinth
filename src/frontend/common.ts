import * as ROT from 'rot-js';
import {XY} from "../backend/interfaces";
import {Tile} from "../backend/world";
import {GameMaster} from "../backend/gamemaster";

export interface MapDrawer {
    gm: GameMaster;

    drawEntireMap(display: ROT.Display, center: XY): void;
    tileAt(loc: XY): Tile;
    drawTileAt(display: ROT.Display, tileLoc: XY, center: XY, forceBg?: string): boolean;
    screenCoordsToMap(screenLoc: XY, center: XY): XY;
}