import * as ROT from 'rot-js';

import {Listable, ListComponent, makeRibbonInfo, RibbonComponent} from "./components";
import {EquippableItem, Weapon} from "../backend/items/item";
import {Character, EquipmentSlotName} from "../backend/character";
import {State} from "./statemachine";
import {GameMaster} from "../backend/gamemaster";
import {Action} from "./keybinds";
import {Chain} from "../backend/common";
import {PlayerController} from "../backend/control";

class SlottedWrapper implements Listable {
    public padding = 10;

    constructor(public slotName: EquipmentSlotName, public item: EquippableItem) {

    }

    public get displayName(): string {
        let paddedSlot = `${this.slotName}:`.padEnd(this.padding);
        return `${paddedSlot}${this.item.displayName}`;
    }
}

export class CharacterState extends State {
    public slots: Array<SlottedWrapper>;

    protected equippedList: ListComponent<SlottedWrapper>;
    protected ribbon: RibbonComponent;
    protected char: Character;

    constructor(protected pc: PlayerController) {
        super();
        this.char = pc.player;
        this.slots = [];
        for (let key in this.char.equipment) {
            this.slots.push(new SlottedWrapper(key, this.char.equipment[key]));
        }
        this.equippedList = new ListComponent<SlottedWrapper>(this, this.slots, {x: 1, y: 5});
        this.addComponent(this.equippedList);
        this.ribbon = new RibbonComponent(this, [
            makeRibbonInfo(Action.Cancel, 'quit'),
            makeRibbonInfo(Action.Unequip, 'unequip'),
            makeRibbonInfo(Action.North, 'up'),
            makeRibbonInfo(Action.South, 'down'),
        ]);
        this.addComponent(this.ribbon);
    }

    public activate(prev: State) {
        super.activate(prev);
        this.ribbon.move({x: 0, y: this.driver.height - 1});
    }

    public handleInput(action: Action, ev: KeyboardEvent): void {
        super.handleInput(action, ev);
        switch (action) {
            case Action.Unequip:
                this.unequipSelectedItem();
                break;
            case Action.Cancel:
                this.driver.pop();
        }
    }

    public render(display: ROT.Display) {
        display.drawText(0, 1, `${this.char.name} the ${this.char.className}`);

        // Equipment
        display.drawText(1, 3, "== Equipment ==");

        let wrapper = this.equippedList.selectedItem;
        this.ribbon.setEnabled(Action.Unequip, wrapper.item.isUnequppable());
        super.render(display);

        // stats
        let y = this.slots.length + 7;
        display.drawText(1, y-1, "== Stats ==");
        for(let statName of ['str', 'dex', 'con', 'int', 'wis', 'cha']) {
            let stat = this.char.stats[statName];
            let statStr = stat < 10 ? ` ${stat}` : `${stat}`;
            let bonus = this.char.statBonus(statName);
            let bonusStr = bonus >= 0 ? `+${bonus}` : `${bonus}`;
            let desc = `${statName}: ${statStr}  ${bonusStr}`;
            display.drawText(1, y, desc);
            y += 1;
        }

        // Defenses
        let x = this.driver.width / 2;
        display.drawText(x + 1, 3, "== Defenses ==");

        let char = this.char;
        display.drawText(x, 5, `HP: ${char.hp}/${char.maxHp}`);
        display.drawText(x+13, 5, `AC: ${char.calcDefense(Chain.ac).value}`);
        let fort = char.calcDefense(Chain.fort).value;
        let ref = char.calcDefense(Chain.ref).value;
        let will = char.calcDefense(Chain.will).value;
        let statStr = `Fortitude: ${fort}  Reflex: ${ref}  Will: ${will}`;
        display.drawText(x, 6, statStr)

        // Offenses
        display.drawText(x + 1, 9, "== Offense ==");

        let wpn = char.equipment.mainHand as Weapon;
        display.drawText(x, 10, `Default Melee: ${wpn.damage.description()}`);

    }

    protected unequipSelectedItem() {
        let wrapper = this.equippedList.selectedItem;
        if(this.pc.unequip(wrapper.slotName)) {
            this.regenerateSlots();
        }
    }

    protected regenerateSlots(): void {
        let offset = 0;
        for(let key in this.char.equipment) {
            let equipment = this.char.equipment[key];
            this.slots[offset].item = equipment;
            offset += 1;
        }
    }
}

