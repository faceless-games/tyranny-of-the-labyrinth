import {LitElement, html} from 'lit-element';
import {Character} from "../backend/character";
import {Chain} from "../backend/common";

class StatusElement extends LitElement {
    static get properties() {
        return {
            character: {type: Object}
        };
    }

    constructor() {
        super();
        this.character = null;
    }

    tempHpDisplay() {
        let thp = this.character.tempHp;

        if(thp <= 0) return '';
        return ` (+${thp})`;
    }

    render() {
        if(this.character === null) {
            return html`(Awaiting gameplay)`;
        }

        return html`
<link href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.1/cyborg/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-iMvB1cubutqnCw/Xlf3A1lEXHojPMC7dETFR9CYfYENKn8yw6QlyL+BaYmPXEjmo" crossorigin="anonymous">

<div class="container">
    <div class="row">
        <div class="col-4">
            ${this.character.name}, the level ${this.character.level}
            ${this.character.className}
        </div>
        <div class="col-2">
            HP: ${this.character.hp}${this.tempHpDisplay()}/${this.character.maxHp}
        </div>
        <div class="col-1">
            AC: ${this.character.ac}
        </div>
        <div class="col-1">
            Fort: ${this.character.fort}
        </div>
        <div class="col-1">
            Ref: ${this.character.ref}
        </div>
        <div class="col-1">
            Will: ${this.character.will}
        </div>
    </div>
    <div class="row">
        <div class="col-10">
            ${this.character.conditionString}
        </div>
    </div>
</div>
`;
    }

}

customElements.define('status-element', StatusElement);
