import {StateDriver} from "./frontend/statemachine";
import {PlayingState} from "./frontend/playing";
import './lit-elements/status-element.js';
import './lit-elements/messages-element.js';
import './lit-elements/hotkeys-element.js';
import './lit-elements/version-element.js';
import {TitleState} from "./frontend/title";

window.onload = () => {
    let driver = new StateDriver();
    let title = new TitleState();
    driver.push(title);

    // For debugging
    (window as any)['driver'] = driver;
};
