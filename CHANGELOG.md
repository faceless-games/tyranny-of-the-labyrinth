# Changelog

* **0.8.1**
  * Fixed an issue where using 'enter' to confirm adventurer name would
    skip past class selection in Firefox

* **0.8.0**: Initial release