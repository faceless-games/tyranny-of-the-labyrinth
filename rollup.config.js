import typescript from '@rollup/plugin-typescript';
import resolve from '@rollup/plugin-node-resolve';
import copy from 'rollup-plugin-copy'

let revision = process.env['CI_COMMIT_SHA'];
if(!revision) {
    revision = require('child_process')
        .execSync('git rev-parse HEAD')
        .toString().trim()
}

let branch = process.env['CI_COMMIT_REF_NAME'];
if(!branch) {
    branch = require('child_process')
        .execSync('git symbolic-ref --short HEAD')
        .toString().trim()
}

let dev_out = {
    file: './build/dev/lib/build.js',
    name: 'totl',
    globals: {
        'rot-js': 'ROT',
        'lz-string': 'LZString',
    },
    sourcemap: true,
    format: 'iife',
    banner: `
    window['totl_build'] = 'dev';
    window['totl_git_hash'] = '${revision}';
    window['totl_branch'] = '${branch}';
    `
};

let unstable_out = {
    ...dev_out,
    file: './build/unstable/lib/build.js',
    banner: `
    window['totl_build'] = 'unstable';
    window['totl_git_hash'] = '${revision}';
    window['totl_branch'] = '${branch}';
    `
}

let stable_out = {
    ...dev_out,
    file: './build/stable/lib/build.js',
    banner: `
    window['totl_build'] = 'stable';
    window['totl_git_hash'] = '${revision}';
    window['totl_branch'] = '${branch}';
    `
}

export default {
    input: './src/main.ts',
    output: [dev_out, unstable_out, stable_out],
    plugins: [
        resolve(),
        typescript(),
        copy({
            targets: [
                {src: 'pages-src/**/*', dest: ['build/dev/', 'build/unstable/', 'build/stable']},
            ],
            flatten: false,
        })
    ],
    external: ['rot-js', 'lz-string'],
    watch: {
        chokidar: false,
        exclude: "node_modules/**"
    },
}
